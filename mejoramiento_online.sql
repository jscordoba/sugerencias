-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-06-2016 a las 22:08:05
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mejoramiento_online`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aprobacion`
--

CREATE TABLE `aprobacion` (
  `idaprobacion` int(11) NOT NULL,
  `pre_mejoramiento_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `tipo_aprobacion_id` int(11) NOT NULL,
  `aprobacion_date` date DEFAULT NULL,
  `usuario_evaluador_id` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `aprobacion`
--

INSERT INTO `aprobacion` (`idaprobacion`, `pre_mejoramiento_id`, `usuario_id`, `tipo_aprobacion_id`, `aprobacion_date`, `usuario_evaluador_id`) VALUES
(54, 66, 31429238, 2, '2016-06-11', 18517426),
(55, 67, 1088004425, 5, '2016-06-11', 18517426),
(56, 68, 1088004425, 6, '2016-06-11', NULL),
(57, 69, 1088004425, 6, '2016-06-11', NULL),
(58, 70, 18518496, 1, '2016-06-11', 51791651),
(59, 71, 10129505, 1, '2016-06-11', 51791651),
(60, 72, 1088006945, 1, '2016-06-11', 51791651),
(61, 73, 1088004425, 6, '2016-06-11', NULL),
(62, 74, 1088317457, 1, '2016-06-11', 51791651);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clase_mejoramiento`
--

CREATE TABLE `clase_mejoramiento` (
  `idclase_mejoramiento` int(11) NOT NULL,
  `clase_mejoramiento_descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clase_mejoramiento`
--

INSERT INTO `clase_mejoramiento` (`idclase_mejoramiento`, `clase_mejoramiento_descripcion`) VALUES
(1, 'SUGERENCIA'),
(2, 'GRUPO KAIZEN'),
(3, 'PLAN ESTRATEGICO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `criterio_evaluacion`
--

CREATE TABLE `criterio_evaluacion` (
  `idcriterio_evaluacion` int(11) NOT NULL,
  `criterio_evaluacion_descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `criterio_evaluacion`
--

INSERT INTO `criterio_evaluacion` (`idcriterio_evaluacion`, `criterio_evaluacion_descripcion`) VALUES
(1, 'CONCEPCION'),
(2, 'METODO'),
(3, 'ESTANDARIZACION'),
(4, 'ESFUERZO'),
(5, 'EFECTO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_mejoramiento`
--

CREATE TABLE `estado_mejoramiento` (
  `idestado_mejoramiento` int(11) NOT NULL,
  `estado_mejoramiento_descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `estado_mejoramiento`
--

INSERT INTO `estado_mejoramiento` (`idestado_mejoramiento`, `estado_mejoramiento_descripcion`) VALUES
(1, 'APROBADO'),
(2, 'NO APROBADO'),
(3, 'PENDIENTE APROBACION'),
(4, 'FINALIZADO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estandarizacion`
--

CREATE TABLE `estandarizacion` (
  `idestandarizacion` int(11) NOT NULL,
  `estandarizacion_descripcion` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `estandarizacion`
--

INSERT INTO `estandarizacion` (`idestandarizacion`, `estandarizacion_descripcion`) VALUES
(2, 'dzfxgbchngfbdsadzfxgbchvjbkkjmhnbdv');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evaluacion`
--

CREATE TABLE `evaluacion` (
  `idevaluacion` int(11) NOT NULL,
  `pre_mejoramiento_id` int(11) NOT NULL,
  `usuario_evaluador_id` int(11) NOT NULL,
  `evaluacion_concepcion` int(2) NOT NULL,
  `evaluacion_metodo` int(2) NOT NULL,
  `evaluacion_estandarizacion` int(2) NOT NULL,
  `evaluacion_esfuerzo` int(2) NOT NULL,
  `evaluacion_efecto` int(2) NOT NULL,
  `evaluacion_date` date DEFAULT NULL,
  `novedad_id` int(12) NOT NULL,
  `evaluacion_total` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jefe_area`
--

CREATE TABLE `jefe_area` (
  `idjefe_area` int(15) NOT NULL,
  `usuario_id` int(15) NOT NULL,
  `seccion_usuario_id` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `jefe_area`
--

INSERT INTO `jefe_area` (`idjefe_area`, `usuario_id`, `seccion_usuario_id`) VALUES
(1, 51791651, 1),
(2, 51791651, 2),
(3, 1088253045, 3),
(4, 42145509, 4),
(5, 79600258, 5),
(6, 51791651, 6),
(7, 9867216, 7),
(8, 10135119, 8),
(9, 10138298, 9),
(10, 18521371, 10),
(11, 10013622, 11),
(12, 79600258, 12),
(13, 75099412, 13),
(14, 1053767608, 14),
(15, 76325863, 15),
(16, 10008060, 16),
(17, 10131272, 17),
(18, 1094937521, 19),
(19, 79959915, 20),
(20, 10003512, 21),
(21, 1088282444, 22),
(22, 10007728, 23),
(23, 1088013767, 24),
(24, 1088249038, 25),
(25, 9861869, 26),
(26, 1087552035, 27),
(27, 9860110, 28),
(28, 10014693, 29),
(29, 1088237115, 30),
(30, 75051806, 31),
(31, 10004100, 32);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `novedad`
--

CREATE TABLE `novedad` (
  `idnovedad` int(30) NOT NULL,
  `pre_mejoramiento_id` int(30) NOT NULL,
  `novedad_descripcion` varchar(500) DEFAULT NULL,
  `novedad_date` date DEFAULT NULL,
  `usuario_id` int(11) NOT NULL,
  `tipo_aprobacion_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `novedad`
--

INSERT INTO `novedad` (`idnovedad`, `pre_mejoramiento_id`, `novedad_descripcion`, `novedad_date`, `usuario_id`, `tipo_aprobacion_id`) VALUES
(40, 67, 'comenzar fase 2, adm', '2016-06-11', 1088248262, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `participante`
--

CREATE TABLE `participante` (
  `idparticipante` int(11) NOT NULL,
  `pre_mejoramiento_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `participante`
--

INSERT INTO `participante` (`idparticipante`, `pre_mejoramiento_id`, `usuario_id`) VALUES
(61, 66, 31429238),
(62, 67, 1088004425),
(63, 68, 1088004425),
(64, 69, 1088004425),
(65, 70, 18518496),
(66, 71, 10129505),
(67, 72, 1088006945),
(68, 73, 1088004425),
(69, 73, 9866473),
(70, 74, 1088317457),
(71, 74, 10001486);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `post_mejoramiento`
--

CREATE TABLE `post_mejoramiento` (
  `idpost_mejoramiento` int(11) NOT NULL,
  `pre_mejoramiento_id` int(11) NOT NULL,
  `post_mejoramiento_despues` varchar(500) NOT NULL,
  `post_mejoramiento_evidencia` varchar(100) DEFAULT NULL,
  `estandarizacion_id` int(11) NOT NULL,
  `post_mejoramiento_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='		';

--
-- Volcado de datos para la tabla `post_mejoramiento`
--

INSERT INTO `post_mejoramiento` (`idpost_mejoramiento`, `pre_mejoramiento_id`, `post_mejoramiento_despues`, `post_mejoramiento_evidencia`, `estandarizacion_id`, `post_mejoramiento_date`) VALUES
(2, 67, 'zfstghgfdsw<dfzgtxhyjuhtgrfdes', 'post_evidencias_2016-06-11_ID677.xlsx', 2, '2016-06-11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pre_mejoramiento`
--

CREATE TABLE `pre_mejoramiento` (
  `id_pre_mejoramiento` int(11) NOT NULL,
  `pre_mejoramiento_titulo` varchar(45) DEFAULT NULL,
  `tipo_mejoramiento_id` int(11) NOT NULL,
  `pre_mejoramiento_date` date DEFAULT NULL,
  `clase_mejoramiento_id` int(11) DEFAULT NULL,
  `seccion_usuario_id` int(11) DEFAULT NULL,
  `pre_mejoramiento_consecutivo` int(11) DEFAULT NULL,
  `tipo_aprobacion_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='		';

--
-- Volcado de datos para la tabla `pre_mejoramiento`
--

INSERT INTO `pre_mejoramiento` (`id_pre_mejoramiento`, `pre_mejoramiento_titulo`, `tipo_mejoramiento_id`, `pre_mejoramiento_date`, `clase_mejoramiento_id`, `seccion_usuario_id`, `pre_mejoramiento_consecutivo`, `tipo_aprobacion_id`) VALUES
(66, 'asdasbdaksdvka', 2, '2016-06-11', 1, 1, NULL, 2),
(67, 'prueba desde adm', 1, '2016-06-11', 1, 1, NULL, 5),
(68, 'negado', 1, '2016-06-11', 1, 1, NULL, 6),
(69, 'finalizado', 2, '2016-06-11', 1, 1, NULL, 6),
(70, 'finalizado planta', 1, '2016-06-11', 1, 1, NULL, 1),
(71, 'negado planta', 1, '2016-06-11', 1, 1, NULL, 1),
(72, 'activo planta', 2, '2016-06-11', 1, 1, NULL, 1),
(73, 'sugerencia de 2 usuarios adm', 1, '2016-06-11', 1, 1, NULL, 6),
(74, 'sugerencia de 2 usuarios PLANTA', 3, '2016-06-11', 1, 1, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seccion_usuario`
--

CREATE TABLE `seccion_usuario` (
  `idseccion_usuario` int(11) NOT NULL,
  `seccion_usuario_descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `seccion_usuario`
--

INSERT INTO `seccion_usuario` (`idseccion_usuario`, `seccion_usuario_descripcion`) VALUES
(1, 'ADMINISTRACION Y FINANZAS'),
(2, 'COMERCIO EXTERIOR'),
(3, 'CONTROL DE CALIDAD'),
(4, 'DESARROLLO HUMANO'),
(5, 'EFACEC'),
(6, 'GRUPO EPICOR'),
(7, 'INGENIERIA'),
(8, 'LABORATORIO'),
(9, 'LOGISTICA'),
(10, 'MANTENIMIENTO'),
(11, 'MEDIO AMBIENTE PLANTA'),
(12, 'MERCADEO Y VENTAS'),
(13, 'PROYECTOS PLANTA 1'),
(14, 'PROYECTOS PLANTA 2'),
(15, 'SEGURIDAD'),
(16, 'SERVICIO TECNICO'),
(17, 'SST'),
(18, 'VENTAS ABQ'),
(19, 'ZONA FRANCA ADMON'),
(20, 'OPERACIONES'),
(21, 'JEFE PLANTA 1'),
(22, 'ENSAMBLE Y CONEXIONES'),
(23, 'BOBINAS'),
(24, 'TERMINADO'),
(25, 'METIDA A CAJA'),
(26, 'JEFE PLANTA 2'),
(27, 'ARMADO'),
(28, 'MAQUINADO'),
(29, 'NUCLEOS'),
(30, 'PINTURA'),
(31, 'SKID'),
(32, 'ZONA FRANCA PDC');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sugerencia`
--

CREATE TABLE `sugerencia` (
  `idsugerencia` int(11) NOT NULL,
  `pre_mejoremiento_id` int(11) NOT NULL,
  `sugerencia_antes_mejora` varchar(500) DEFAULT NULL,
  `sugerencia_problema` varchar(500) DEFAULT NULL,
  `sugerencia_esperado` varchar(500) DEFAULT NULL,
  `sugerencia_ahorro` tinyint(1) DEFAULT NULL,
  `sugerencia_fahorro` varchar(100) DEFAULT NULL,
  `sugerencia_propuesta` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sugerencia`
--

INSERT INTO `sugerencia` (`idsugerencia`, `pre_mejoremiento_id`, `sugerencia_antes_mejora`, `sugerencia_problema`, `sugerencia_esperado`, `sugerencia_ahorro`, `sugerencia_fahorro`, `sugerencia_propuesta`) VALUES
(53, 66, 'sfdsadfasofnalisbaf', 'lbsflb', 'lbljfbskgdvfdslicbl', 0, '', 'pre_evidencias_2016-06-11_ID66.xlsx'),
(54, 67, 'klscdbdvhncdsvdhbcklk', 'sdvhbnlddvbdnslcjk', 'asbldnckbjkdadlcnkdjkbscxln', 0, '', 'pre_evidencias_2016-06-11_ID67.xlsx'),
(55, 68, 'avioubnkbsjzhdvic<cbsñ', 'vhdsbjldkjohvaidvjsbknñsao', 'dcsbjlkncdñdbjoccddnas', 0, '', 'pre_evidencias_2016-06-11_ID68.xlsx'),
(56, 69, 'adoiujbkjssnkslddviha ln', 'bjnkwlsdbhkjdlnbdschd kjlndab', 'isaschbjdsnbidvhskjsnldbbd', 0, '', 'pre_evidencias_2016-06-11_ID69.docx'),
(57, 70, 'csikhcdbjnñkalsivhcsjbklb', 'cvbliyctxytfcgvbklkuifydtyjxfcgh', 'kiuyjgch bjnkliyutcyfghvjbkn', 1, '', 'pre_evidencias_2016-06-11_ID70.docx'),
(58, 71, 'fawkebcawebvwi wv bawv wecv wlvuiwwbevkuyawv ', 'weiucub wiub w    eyvuevca  wbdckudsv c', 'ecyvlibjhsvcvhgsvlsd   qcbaecjvwlcuyv', 0, '', 'pre_evidencias_2016-06-11_ID71.xlsx'),
(59, 72, '<vkjjlhlhfaskfh blbkjcb vb<bev<en.<l dvbzs vv e bug <c<acjv', '<sdcvsucuvkjdbcv<efvserfseurogleauovryfarlvbuahv la c', 'wliuawbrfbreyucebvliueawhmfviwegu', 1, '', 'pre_evidencias_2016-06-11_ID72.doc'),
(60, 73, 'fani fifkb fb', 'li beliaubefl uhfewvf vkhv oiv', ' wb kb webve ve ñwoeihveñwoi eñfb', 1, '', 'pre_evidencias_2016-06-11_ID73.xlsx'),
(61, 74, 'wwcbaikbawñoebiaweb ceib lew', 'iwubdvwbv ñwenvw ev ewv iwebivlevlkn', 'wvñauinvabiuven  weivjawivlbw av ', 1, '', 'pre_evidencias_2016-06-11_ID74.xlsx');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_aprobacion`
--

CREATE TABLE `tipo_aprobacion` (
  `idtipo_aprobacion` int(11) NOT NULL,
  `tipo_aprobacion_descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_aprobacion`
--

INSERT INTO `tipo_aprobacion` (`idtipo_aprobacion`, `tipo_aprobacion_descripcion`) VALUES
(1, 'PJ'),
(2, 'PL'),
(3, 'F2'),
(4, 'IJ'),
(5, 'IL'),
(6, 'PDT'),
(7, 'FN'),
(8, 'AL'),
(9, 'NG');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_mejoramiento`
--

CREATE TABLE `tipo_mejoramiento` (
  `idtipo_mejoramiento` int(11) NOT NULL,
  `tipo_mejoramiento_descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_mejoramiento`
--

INSERT INTO `tipo_mejoramiento` (`idtipo_mejoramiento`, `tipo_mejoramiento_descripcion`) VALUES
(1, 'PREVENTIVO'),
(2, 'MEJORA'),
(3, 'CORRECTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_usuario`
--

CREATE TABLE `tipo_usuario` (
  `idtipo_usuario` int(11) NOT NULL,
  `tipo_usuario_descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`idtipo_usuario`, `tipo_usuario_descripcion`) VALUES
(1, 'PROPONENTE'),
(2, 'EVALUADOR'),
(3, 'ADMINISTRADOR'),
(4, 'PLANTA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` bigint(30) NOT NULL,
  `usuario_usuario` varchar(30) DEFAULT NULL,
  `usuario_clave` varchar(30) DEFAULT NULL,
  `usuario_nombre` varchar(45) DEFAULT NULL,
  `usuario_apellido` varchar(45) DEFAULT NULL,
  `seccion_usuario_id` int(30) NOT NULL,
  `tipo_usuario_id` int(30) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `usuario_planta` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `usuario_usuario`, `usuario_clave`, `usuario_nombre`, `usuario_apellido`, `seccion_usuario_id`, `tipo_usuario_id`, `email`, `usuario_planta`) VALUES
(1, 'planta1', 'planta123', 'PLANTA', '1', 0, 4, '', 'PLANTA 1'),
(2, 'planta2', 'planta123', 'PLANTA', '2', 0, 4, '', 'PLANTA 2'),
(3, 'admin', '321', 'Administrador', 'Mejoramiento Magnetron', 0, 3, '', ''),
(4236451, NULL, NULL, 'JOSE ROSEMBERG', 'BAUTISTA PARDO', 29, 4, '', 'ZF'),
(4512430, NULL, NULL, 'MARLON ARIEL', 'GARCIA HENAO', 5, 4, '', 'PLANTA 2'),
(4512490, NULL, NULL, 'ANDRES FELIPE', 'GARCIA SANCHEZ', 19, 4, '', 'PLANTA 1'),
(4512529, NULL, NULL, 'ALEXANDER ', 'PEREZ GRISALES', 2, 4, '', 'PLANTA 2'),
(4512595, NULL, NULL, 'ANDRES JULIAN', 'RODRIGUEZ MOLINA', 3, 4, '', 'PLANTA 1'),
(4512780, NULL, NULL, 'DANIEL ANDRES', 'CORTES POLOCHE', 19, 4, '', 'PLANTA 1'),
(4513309, NULL, NULL, 'CRISTIAN XAVIER', 'AMAYA JIMENEZ', 8, 4, '', 'PLANTA 1'),
(4514091, NULL, NULL, 'JUAN PABLO', 'RAMIREZ OCAMPO', 18, 4, '', 'PLANTA 2'),
(4514241, NULL, NULL, 'CHRISTIAN MARINO', 'LOPEZ SALAZAR', 11, 4, '', 'PLANTA 1'),
(4514295, NULL, NULL, 'JORGE EDUAR', 'MONTOYA NARVAEZ', 5, 4, '', 'PLANTA 1'),
(4515539, NULL, NULL, 'RUSVEDT ', 'HERNANDEZ PELAEZ', 2, 4, '', 'PLANTA 2'),
(4517470, NULL, NULL, 'LUIS FERNANDO', 'SOTO TABARES', 3, 4, '', 'PLANTA 1'),
(4517630, NULL, NULL, 'GONZALO ENRIQUE', 'GONZALEZ FERNANDEZ', 3, 4, '', 'PLANTA 1'),
(4517734, NULL, NULL, 'HECTOR AUGUSTO', 'HIDALGO DIAZ', 3, 4, '', 'PLANTA 1'),
(4517935, NULL, NULL, 'JOSE LUIS', 'DORIA MEDINA', 12, 4, '', 'PLANTA 1'),
(4517937, NULL, NULL, 'OSCAR ALBERTO', 'GOMEZ GIRALDO', 12, 4, '', 'PLANTA 1'),
(4517961, NULL, NULL, 'FABIO DE JESUS', 'ALVAREZ BERMUDEZ', 1, 4, '', 'PLANTA 1'),
(4518747, NULL, NULL, 'JUAN CARLOS', 'NARVAEZ ALVAREZ', 12, 4, '', 'PLANTA 1'),
(4518917, NULL, NULL, 'ROBERTO ', 'CORTES VALENCIA', 2, 4, '', 'PLANTA 2'),
(4519732, NULL, NULL, 'JOSE DAVID', 'QUITIAN ESCUDERO', 3, 4, '', 'PLANTA 1'),
(4519870, NULL, NULL, 'CHRISTIAN ALEJANDRO', 'GRISALES SANCHEZ', 5, 4, '', 'PLANTA 1'),
(4520271, NULL, NULL, 'JULIAN ANDRES', 'ZAPATA BEDOYA', 11, 4, '', 'PLANTA 1'),
(4520643, NULL, NULL, 'JHON ALBERTO', 'CORREA GOMEZ', 3, 4, '', 'PLANTA 1'),
(4520825, NULL, NULL, 'GERMAN DARIO', 'HERNANDEZ IDARRAGA', 19, 4, '', 'PLANTA 1'),
(4540207, NULL, NULL, 'JUVENAL ', 'PESCADOR LADINO', 17, 4, '', 'PLANTA 1'),
(4585418, NULL, NULL, 'CRISTIAN EDUARDO', 'ACUNA NEIRA', 14, 4, '', 'PLANTA 2'),
(4586969, NULL, NULL, 'ADALBERTO DE JESUS', 'MORALES BEDOYA', 8, 4, '', 'PLANTA 1'),
(6112284, NULL, NULL, 'LUIS FERNANDO', 'ARRUBLA CARMONA', 18, 4, '', 'PLANTA 2'),
(6358610, NULL, NULL, 'FERNANDO ', 'LONDONO MONTES', 17, 4, '', 'PLANTA 1'),
(6372589, NULL, NULL, 'RAUL ENRIQUE', 'NAVARRO BAZURTO', 27, 4, '', 'PLANTA 1'),
(6479355, NULL, NULL, 'DAIRO ', 'CARDONA BENAVIDES', 12, 4, '', 'PLANTA 1'),
(6519894, NULL, NULL, 'VICTOR HUGO', 'VALENCIA MUNOZ', 26, 4, '', 'PLANTA 1'),
(6529844, NULL, NULL, 'LUIS CARLOS', 'LOAIZA CASTANO', 26, 4, '', 'PLANTA 1'),
(7548049, NULL, NULL, 'JOSE ELMER', 'QUITIAN GUERRA', 3, 4, '', 'PLANTA 1'),
(9698328, NULL, NULL, 'CESAR AUGUSTO', 'ALARCON SERNA', 8, 4, '', 'PLANTA 1'),
(9739010, NULL, NULL, 'MAURICIO ', 'GARCIA ZAPATA', 29, 4, '', 'ZF'),
(9790773, NULL, NULL, 'LUIS FERNANDO', 'OSPINA GRISALES', 2, 4, '', 'PLANTA 2'),
(9805486, NULL, NULL, 'HERNAN NICOLAS', 'BOLIVAR CARDENAS', 5, 4, '', 'PLANTA 2'),
(9806779, NULL, NULL, 'JORGE ALBERTO', 'PARRA PALTA', 29, 4, '', 'ZF'),
(9806790, NULL, NULL, 'LUIS FERNANDO', 'BEDOYA CARDENAS', 29, 4, '', 'ZF'),
(9807754, NULL, NULL, 'HECTOR FABIO', 'CAICEDO ALVAREZ', 29, 4, '', 'ZF'),
(9809349, NULL, NULL, 'JHONNY FERNANDO', 'OLIVERO ARIZA', 29, 4, '', 'ZF'),
(9816025, NULL, NULL, 'HUGO ARLEY', 'NARVAEZ LOAIZA', 8, 4, '', 'PLANTA 1'),
(9818349, NULL, NULL, 'DIEGO ARMANDO', 'BEDOYA MONSALVE', 24, 4, '', 'PLANTA 2'),
(9818877, NULL, NULL, 'CARLOS ALBERTO', 'VALENCIA ACEVEDO', 12, 4, '', 'PLANTA 1'),
(9860031, NULL, NULL, 'HECTOR FABIO', 'MORALES CORREA', 14, 4, '', 'PLANTA 2'),
(9860110, '9860110', '123', 'DIEGO ALEJANDRO', 'VIANA BOLANOS', 28, 1, 'digoviana@magnetron.com.co', 'PLANTA 2'),
(9860390, NULL, NULL, 'JOSE VICENTE', 'BEDOYA BEDOYA', 3, 4, '', 'PLANTA 1'),
(9860414, NULL, NULL, 'DIEGO ANDRES', 'GUEVARA PINZON', 12, 4, '', 'PLANTA 1'),
(9860620, NULL, NULL, 'ROGER ', 'ARIAS RIOS', 5, 4, '', 'PLANTA 2'),
(9860874, NULL, NULL, 'FABIO ', 'VASQUEZ HECTOR', 13, 4, '', 'PLANTA 2'),
(9861801, NULL, NULL, 'CHRISTIAN GEOVANNY', 'OSORNO CANAS', 1, 4, '', 'PLANTA 1'),
(9861851, NULL, NULL, 'YEISON ANDRES', 'SALAZAR OROZCO', 5, 4, '', 'PLANTA 1'),
(9861869, '9861869', '123', 'JOHAN ANDRES', 'LOAIZA RAMIREZ', 26, 1, 'johanloaiza@magnetron.com.co', 'PLANTA 2'),
(9861898, NULL, NULL, 'CRISTIAN CAMILO', 'MONTOYA VALENCIA', 16, 4, '', 'PLANTA 1'),
(9865570, NULL, NULL, 'NESTOR ADOLFO', 'VALENCIA CALVO', 18, 4, '', 'PLANTA 2'),
(9865769, NULL, NULL, 'JHONAR ', 'PINO ARIAS', 18, 4, '', 'PLANTA 2'),
(9865793, NULL, NULL, 'JUAN PABLO', 'BAQUERO GUTIERREZ', 12, 4, '', 'PLANTA 1'),
(9866473, NULL, NULL, 'DIEGO FERNANDO', 'MOYA VERGARA', 9, 4, '', 'PLANTA 2'),
(9866638, NULL, NULL, 'OMAR ORESTES', 'BOTERO GALLEGO', 19, 4, '', 'PLANTA 1'),
(9867055, NULL, NULL, 'OMAR ANDRES', 'SARAY GALVIS', 19, 4, '', 'PLANTA 1'),
(9867216, '9867216', '123', 'JASON ', 'RESTREPO MUNOZ', 7, 1, 'jasonrestrepo@magnetron.com.co', 'PLANTA 1'),
(9867853, NULL, NULL, 'JORGE MARIO', 'LOPEZ ANDICA', 14, 4, '', 'PLANTA 2'),
(9868678, NULL, NULL, 'MILTON FABIAN', 'COBO NERY', 19, 4, '', 'PLANTA 1'),
(9868825, NULL, NULL, 'DARWIN ', 'HOYOS HURTADO', 2, 4, '', 'PLANTA 2'),
(9868851, NULL, NULL, 'LUIS ALBERTO', 'MEJIA VELASCO', 3, 4, '', 'PLANTA 1'),
(9868933, NULL, NULL, 'MAURICIO ', 'OSORIO VALENCIA', 12, 4, '', 'PLANTA 1'),
(9869688, NULL, NULL, 'JUAN CARLOS', 'VALENCIA GUERRA', 3, 4, '', 'PLANTA 1'),
(9870024, NULL, NULL, 'JOHN JADER', 'TABARES CHAVES', 8, 4, '', 'PLANTA 1'),
(9870520, NULL, NULL, 'DIEGO FERNANDO', 'CARRENO HINESTROZA', 23, 4, '', 'PLANTA 1'),
(9871652, NULL, NULL, 'JHON WILLIAM', 'LOPEZ MOTATO', 3, 4, '', 'PLANTA 1'),
(9872227, NULL, NULL, 'ANTONIO ', 'HIDALGO UBER', 3, 4, '', 'PLANTA 1'),
(9873311, NULL, NULL, 'JHON FREDY', 'VALLEJO GIRALDO', 5, 4, '', 'PLANTA 1'),
(9873522, NULL, NULL, 'JOSE ELMER', 'JIMENEZ GIRALDO', 3, 4, '', 'PLANTA 1'),
(9873744, NULL, NULL, 'MAURICIO ', 'BANOL BECERRA', 8, 4, '', 'PLANTA 1'),
(9894844, NULL, NULL, 'LUIS ORLANDO', 'BEDOYA RAMIREZ', 19, 4, '', 'PLANTA 2'),
(9910962, NULL, NULL, 'ALEXANDER ', 'ROJAS VINASCO', 19, 4, '', 'PLANTA 1'),
(9915295, NULL, NULL, 'JHON EDISON', 'HENAO GALLEGO', 2, 4, '', 'PLANTA 2'),
(9993968, NULL, NULL, ' DABIER', 'CARDONA', 18, 4, '', 'PLANTA 2'),
(10000616, NULL, NULL, 'DIEGO ALEXANDER', 'MARIN ROSERO', 13, 4, '', 'PLANTA 2'),
(10001022, NULL, NULL, 'MAURICIO ', 'GONZALEZ CASTILLO', 16, 4, '', 'PLANTA 1'),
(10001338, NULL, NULL, 'JHON FREDY', 'AGUIRRE HENAO', 8, 4, '', 'PLANTA 1'),
(10001404, NULL, NULL, 'RICARDO ANDRES', 'GALVIS BEDOYA', 3, 4, '', 'PLANTA 1'),
(10001486, NULL, NULL, 'NORDEY FERNANDO', 'ARBOLEDA GOMEZ', 19, 4, '', 'PLANTA 2'),
(10002268, NULL, NULL, 'JAIR ', 'OSORIO MARSIALEZ', 12, 4, '', 'PLANTA 1'),
(10002849, NULL, NULL, 'NELSON ', 'QUINTERO QUINTERO', 13, 4, '', 'PLANTA 1'),
(10003512, '10003512', '123', 'JORGE ANCIR', 'ARANZAZU AGUDELO', 21, 1, 'jaranzazu@magnetron.com.co', 'PLANTA 1'),
(10004100, '10004100', '123', 'HECTOR FABIO', 'LUGO ATEHORTUA', 32, 1, 'lugo@magnetron.com.co', 'ZF'),
(10004958, NULL, NULL, 'DIEGO ALONSO', 'ARANGO VEGA', 8, 4, '', 'PLANTA 1'),
(10005206, NULL, NULL, 'EDISON ', 'CASTANO CIFUENTES', 8, 4, '', 'PLANTA 1'),
(10005405, NULL, NULL, 'CARLOS MARIO', 'VERGARA MORALES', 12, 4, '', 'PLANTA 1'),
(10006481, NULL, NULL, 'ALBEIRO DE JESUS', 'ARCILA GIRALDO', 3, 4, '', 'PLANTA 1'),
(10006993, NULL, NULL, 'DIEGO ALEJANDRO', 'DUQUE ALVAREZ', 26, 4, '', 'PLANTA 1'),
(10007019, NULL, NULL, 'ALEXANDER ANDRES', 'GONZALEZ AGUDELO', 3, 4, '', 'PLANTA 1'),
(10007147, NULL, NULL, 'DIEGO GERMAN', 'CLAVIJO LOPEZ', 5, 4, '', 'PLANTA 1'),
(10007240, NULL, NULL, 'MAURICIO DE JESUS', 'LOPEZ GRANADA', 21, 4, '', 'PLANTA 1'),
(10007367, NULL, NULL, 'HENRY ', 'MARIN ZAPATA', 2, 4, '', 'PLANTA 2'),
(10007432, NULL, NULL, 'CARLOS AUGUSTO', 'MARTINEZ ESCOBAR', 8, 4, '', 'PLANTA 1'),
(10007578, NULL, NULL, 'ALEJANDRO ', 'VANEGAS JOSE', 18, 4, '', 'PLANTA 2'),
(10007728, '10007728', '123', 'JORGE ELIECER', 'CORREA CARDONA', 23, 1, 'jorgecorrea@magnetron.com.co', 'PLANTA 1'),
(10007967, NULL, NULL, 'DANILO ', 'RAMOS PEREZ', 20, 4, '', 'PLANTA 2'),
(10008060, '10008060', '123', 'OMAR AUGUSTO', 'CORTES MEDINA', 16, 1, 'omar.cortes@magnetron.com.co', 'PLANTA 1'),
(10008530, NULL, NULL, 'LUIS ANIBAL', 'OSORIO OSPINA', 12, 4, '', 'PLANTA 1'),
(10008852, NULL, NULL, 'RUBEN DARIO', 'ALZATE OBANDO', 12, 4, '', 'PLANTA 1'),
(10009819, NULL, NULL, 'RODRIGO ALBERTO', 'MONTES HINCAPIE', 18, 4, '', 'PLANTA 2'),
(10010571, NULL, NULL, 'CESAR AUGUSTO', 'GIRALDO PANIAGUA', 19, 4, '', 'PLANTA 2'),
(10010664, NULL, NULL, 'JULIO ANDRES', 'MARIN VARGAS', 3, 4, '', 'PLANTA 1'),
(10010807, NULL, NULL, 'ARIEL ', 'CASTANO FRANCO', 12, 4, '', 'PLANTA 1'),
(10011569, NULL, NULL, 'FABIAN DE JESUS', 'QUINTERO TORRES', 14, 4, '', 'PLANTA 2'),
(10012730, NULL, NULL, 'JOSE MIGUEL', 'PARDO AGUIRRE', 8, 4, '', 'PLANTA 1'),
(10013622, '10013622', '123', 'ADRIAN EDUARDO', 'CASTRO ARIAS', 11, 1, 'adrian@magnetron.com.co', 'PLANTA 1'),
(10013865, NULL, NULL, 'LENIN ', 'BEDOYA OSORIO', 5, 4, '', 'PLANTA 1'),
(10013949, NULL, NULL, 'OSCAR ', 'BUSTAMANTE VALENCIA', 20, 4, '', 'PLANTA 2'),
(10014523, NULL, NULL, 'CARLOS IVAN', 'MEJIA HERRERA', 5, 4, '', 'PLANTA 2'),
(10014693, '10014693', '123', 'WILBER HUMBERTO', 'AGUIRRE SARMIENTO', 29, 1, 'wilberaguirre@magnetron.com.co', 'PLANTA 2'),
(10014857, NULL, NULL, 'JUAN ANDRES', 'CARDONA DIAZ', 14, 4, '', 'PLANTA 2'),
(10015053, NULL, NULL, 'JHON HARLEN', 'HURTADO MARIN', 13, 4, '', 'PLANTA 1'),
(10015854, NULL, NULL, 'ANTONIO JAVIER', 'BARRIGA ISAZA', 20, 4, '', 'PLANTA 2'),
(10018513, NULL, NULL, 'JOSE MANUEL', 'GOMEZ JARAMILLO', 13, 4, '', 'PLANTA 1'),
(10018697, NULL, NULL, 'CARLOS ALBERTO', 'GRAJALES DUQUE', 13, 4, '', 'PLANTA 1'),
(10019092, NULL, NULL, 'AURELIO DE JESUS', 'TORO MEJIA', 2, 4, '', 'PLANTA 2'),
(10019367, NULL, NULL, 'RODOLFO ', 'LOAIZA GALLEGO', 14, 4, '', 'PLANTA 2'),
(10019542, NULL, NULL, 'JORGE ALBERTO', 'VALLEJO RIOS', 14, 4, '', 'PLANTA 2'),
(10022850, NULL, NULL, 'JHOHANNY ', 'CARRILLO CARDONA', 20, 4, '', 'PLANTA 2'),
(10022899, NULL, NULL, 'JHON JAIME', 'HINCAPIE HERNANDEZ', 14, 4, '', 'PLANTA 2'),
(10023094, NULL, NULL, 'JAVIER ', 'DAVILA SIERRA', 3, 4, '', 'PLANTA 1'),
(10023306, NULL, NULL, 'LUIS FERNEY', 'CARDONA VANEGAS', 12, 4, '', 'PLANTA 1'),
(10023380, NULL, NULL, 'WILSON FABER', 'MUNOZ HINCAPIE', 5, 4, '', 'PLANTA 2'),
(10023416, NULL, NULL, 'JOSE HENRY', 'SOTO LOPEZ', 3, 4, '', 'PLANTA 1'),
(10023833, NULL, NULL, 'JORGE ELIECER', 'AGUDELO USAQUEN', 26, 4, '', 'PLANTA 1'),
(10023838, NULL, NULL, 'JAIME ANDRES', 'RAMIREZ SERNA', 8, 4, '', 'PLANTA 1'),
(10024973, NULL, NULL, 'GERMAN ', 'MURILLO LOPEZ', 22, 4, '', 'PLANTA 1'),
(10026530, NULL, NULL, 'GILBERTO ', 'CADENA MORENO', 8, 4, '', 'PLANTA 1'),
(10027426, NULL, NULL, 'JULIAN ', 'NIETO SANCHEZ', 19, 4, '', 'PLANTA 1'),
(10027503, NULL, NULL, 'JULIO CESAR', 'ARANGO URIBE', 2, 4, '', 'PLANTA 2'),
(10027582, NULL, NULL, 'ORLANDO ', 'CARDENAS OSPINA', 8, 4, '', 'PLANTA 1'),
(10030425, NULL, NULL, 'ANDRES JAVIER', 'OROZCO GIRALDO', 10, 4, '', 'PLANTA 1'),
(10030548, NULL, NULL, 'EDUARD ARLEY', 'CASTILLO ESCOBAR', 10, 4, '', 'PLANTA 1'),
(10030910, NULL, NULL, 'JUAN PABLO', 'RESTREPO ACOSTA', 18, 4, '', 'PLANTA 2'),
(10031215, NULL, NULL, 'CARLOS ANDRES', 'CORREA CORREA', 20, 4, '', 'PLANTA 2'),
(10031374, NULL, NULL, 'JOHN EDUAR', 'PELAEZ GUERRERO', 2, 4, '', 'PLANTA 2'),
(10032254, NULL, NULL, 'MAURICIO ', 'BUITRAGO ORTIZ', 11, 4, '', 'PLANTA 1'),
(10032484, NULL, NULL, 'JHON DENNY', 'JIMENEZ HINCAPIE', 17, 4, '', 'PLANTA 1'),
(10033379, NULL, NULL, 'MAURICIO ALEXANDER', 'CARDONA AGUDELO', 5, 4, '', 'PLANTA 1'),
(10033597, NULL, NULL, 'MARIO ESTEBAN', 'MUNOZ CASTANO', 3, 4, '', 'PLANTA 1'),
(10033621, NULL, NULL, 'FABIO NELSON', 'CRUZ LARGO', 26, 4, '', 'PLANTA 1'),
(10033996, NULL, NULL, 'EDUARDO ', 'OCAMPO OSCAR', 3, 4, '', 'PLANTA 1'),
(10034009, NULL, NULL, 'VLADIMIR ', 'OSORIO POSADA', 14, 4, '', 'PLANTA 2'),
(10034222, NULL, NULL, 'ALEXANDER ', 'VILLADA GONZALEZ', 17, 4, '', 'PLANTA 1'),
(10034809, NULL, NULL, 'JAVIER ', 'ARENAS CASTANO', 8, 4, '', 'PLANTA 1'),
(10034949, NULL, NULL, 'JORGE IGNACIO', 'CELIS GUZMAN', 18, 4, '', 'PLANTA 2'),
(10050373, NULL, NULL, 'LUIS ENRIQUE', 'MORALES MARIN', 18, 4, '', 'PLANTA 2'),
(10051410, NULL, NULL, 'MAURICIO ', 'MUNOZ LOPEZ', 8, 4, '', 'PLANTA 1'),
(10051413, NULL, NULL, 'JOHN FREDY', 'CASTANEDA RESTREPO', 24, 4, '', 'PLANTA 2'),
(10070439, NULL, NULL, 'EDGAR ', 'ARIAS OSORIO', 8, 4, '', 'PLANTA 1'),
(10071949, NULL, NULL, 'GREGORIO ALBERTO', 'GUZMAN CARDONA', 1, 4, '', 'PLANTA 1'),
(10081020, NULL, NULL, 'ELEVI ', 'SUAREZ BANOL', 20, 4, '', 'PLANTA 2'),
(10086329, NULL, NULL, 'ALVARO ', 'BENJUMEA PULGARIN', 21, 4, '', 'PLANTA 1'),
(10087599, NULL, NULL, 'GILDARDO ', 'GRISALES CASTANEDA', 14, 4, '', 'PLANTA 2'),
(10091433, NULL, NULL, 'HERNANDO ', 'RAMIREZ PINEDA', 3, 4, '', 'PLANTA 1'),
(10092970, NULL, NULL, 'CARLOS ARTURO', 'OSORIO RODRIGUEZ', 3, 4, '', 'PLANTA 1'),
(10093444, NULL, NULL, 'CARLOS FERNANDO', 'VERA ZUNIGA', 21, 4, '', 'PLANTA 2'),
(10097777, NULL, NULL, 'GUSTAVO ALBERTO', 'OCAMPO OSORIO', 26, 4, '', 'PLANTA 1'),
(10098180, NULL, NULL, 'JOSE MILTON', 'CORREA CORREA', 2, 4, '', 'PLANTA 2'),
(10099226, NULL, NULL, 'DARBEY ARTURO', 'ARCILA ZULUAGA', 14, 4, '', 'PLANTA 2'),
(10105791, NULL, NULL, 'LUIS EMILIO', 'MUNOZ LOPEZ', 8, 4, '', 'PLANTA 1'),
(10105910, NULL, NULL, 'FABIO ENRIQUE', 'OROZCO DIAZ', 18, 4, '', 'PLANTA 2'),
(10107435, NULL, NULL, 'JAIRO EMILIO', 'GASPAR VILLADA', 15, 4, '', 'PLANTA 1'),
(10109032, NULL, NULL, 'MIGUEL ANTONIO', 'VELASQUEZ SANTAMARIA', 6, 4, '', 'PLANTA 1'),
(10116522, NULL, NULL, 'JORGE WILLIAM', 'OSPINA CARDONA', 20, 4, '', 'PLANTA 2'),
(10117008, NULL, NULL, 'JORGE ELIECER', 'TAPASCO IBARRA', 14, 4, '', 'PLANTA 2'),
(10117454, NULL, NULL, 'DIEGO ', 'FRANCO ARENAS', 3, 4, '', 'PLANTA 1'),
(10119637, NULL, NULL, 'ESTEBAN ', 'BENJUMEA PULGARIN', 14, 4, '', 'PLANTA 2'),
(10122593, NULL, NULL, 'ALEJANDRO ', 'NAVARRO GONZALEZ', 10, 4, '', 'PLANTA 1'),
(10123762, NULL, NULL, 'LUIS FERNANDO', 'GAVIRIA SANCHEZ', 18, 4, '', 'PLANTA 2'),
(10128925, NULL, NULL, 'JHON JAIRO', 'GONZALEZ DAVILA', 21, 4, '', 'PLANTA 1'),
(10129505, NULL, NULL, 'JOHN FREDY', 'MESA RUIZ', 8, 4, '', 'PLANTA 1'),
(10131272, '10131272', '123', 'RAFAEL IGNACIO', 'RAMIREZ ANGEL', 17, 1, 'rafael@magnetron.com.co', 'PLANTA 1'),
(10131915, NULL, NULL, 'JAIRO ', 'HIDALGO CUARTAS', 3, 4, '', 'PLANTA 1'),
(10132130, NULL, NULL, 'LUIS FERNANDO', 'VARELA ESCALANTE', 8, 4, '', 'PLANTA 1'),
(10135119, '10135119', '123', 'JUAN CARLOS', 'LONDONO MARIN', 8, 1, 'jlondono@magnetron.com.co', 'PLANTA 1'),
(10135375, NULL, NULL, 'NURIEL DE JESUS', 'OSORIO GARCIA', 8, 4, '', 'PLANTA 1'),
(10136406, NULL, NULL, 'DARIO ', 'MONTOYA RUBEN', 3, 4, '', 'PLANTA 1'),
(10136930, NULL, NULL, 'BEDER DE JESUS ', 'ZAPATA RAMIREZ', 8, 4, '', 'PLANTA 1'),
(10138298, '10138298', '123', 'JAIRO ALBERTO', 'DIOSA MONTOYA', 9, 1, 'jairo.diosa@magnetron.com.co', 'PLANTA 1'),
(10139817, NULL, NULL, 'JOSE ENRIQUE', 'QUINTERO CARDONA', 18, 4, '', 'PLANTA 2'),
(10140897, NULL, NULL, 'LEONARDO ', 'LOAIZA GALLEGO', 20, 4, '', 'PLANTA 2'),
(10198224, NULL, NULL, 'UBERNEY ', 'GOMEZ TOBAR', 20, 4, '', 'PLANTA 2'),
(10198930, NULL, NULL, 'JOSE RAMIRO', 'FORONDA FORONDA', 8, 4, '', 'PLANTA 1'),
(10199088, NULL, NULL, 'LUIS ENRIQUE', 'VANEGAS AMAYA', 13, 4, '', 'PLANTA 1'),
(10259119, NULL, NULL, 'MARIO ', 'BALLESTEROS BETANCUR', 11, 4, '', 'PLANTA 1'),
(10273816, NULL, NULL, 'YHON HENRY', 'QUINTERO NARANJO', 14, 4, '', 'PLANTA 2'),
(14225198, NULL, NULL, 'MAXIMILIANO ', 'GAMBOA PEREZ', 26, 4, '', 'PLANTA 1'),
(14568655, NULL, NULL, 'JUAN CARLOS', 'LLANO CORREA', 12, 4, '', 'PLANTA 1'),
(14570532, NULL, NULL, 'ALEJANDRO ', 'MARIN CARDENAS', 8, 4, '', 'PLANTA 1'),
(14570824, NULL, NULL, 'ALEXANDER ', 'RESTREPO BERMUDEZ', 1, 4, '', 'PLANTA 1'),
(14572387, NULL, NULL, 'JAIME ANDRES', 'QUICENO GONZALEZ', 9, 4, '', 'PLANTA 2'),
(14572643, NULL, NULL, 'JOHN JANNENCEN', 'HOLGUIN RAMIREZ', 3, 4, '', 'PLANTA 1'),
(15535730, NULL, NULL, 'LUIS ALFONSO', 'MONTOYA ISAZA', 20, 4, '', 'PLANTA 2'),
(15914322, NULL, NULL, 'GILDARDO DE JESUS', 'LARGO CALVO', 18, 4, '', 'PLANTA 2'),
(16090408, NULL, NULL, 'JOSE LEONARDO', 'HOLGUIN CARVAJAL', 8, 4, '', 'PLANTA 1'),
(16090481, NULL, NULL, 'MARIO ANDRES', 'ANGEL CASTANO', 8, 4, '', 'PLANTA 1'),
(16218920, NULL, NULL, 'CARLOS EDINSON', 'RADA LOZANO', 21, 4, '', 'PLANTA 2'),
(16228797, NULL, NULL, 'MAURICIO ARDUL', 'BENITEZ MOLINA', 3, 4, '', 'PLANTA 1'),
(16231995, NULL, NULL, 'ALEJANDRO ', 'VALLEJO AYALA', 14, 4, '', 'PLANTA 2'),
(16232512, NULL, NULL, 'JOHN FREDY', 'LONGA BENITEZ', 25, 4, '', 'PLANTA 2'),
(16233621, NULL, NULL, 'DIEGO BERNARDO', 'AGUDELO MEJIA', 14, 4, '', 'PLANTA 2'),
(16641152, NULL, NULL, 'HELADIO ', 'LOPEZ CRUZ', 15, 4, '', 'PLANTA 1'),
(16827479, NULL, NULL, 'GUSTAVO ', 'HENAO MARIN', 3, 4, '', 'PLANTA 1'),
(16894006, NULL, NULL, 'JHON HENRY', 'CASTRO PULIDO', 29, 4, '', 'ZF'),
(16894037, NULL, NULL, 'ALEXANDER ', 'CAJIAO CASTRO', 17, 4, '', 'PLANTA 1'),
(17644159, NULL, NULL, 'FRANCISCO JAVIER', 'VELASQUEZ PALACIO', 14, 4, '', 'PLANTA 2'),
(18370396, NULL, NULL, 'CRISTIAN ANDRES', 'MONTOYA GOMEZ', 29, 4, '', 'ZF'),
(18370433, NULL, NULL, 'JOSE OSWALDO', 'PULGARIN LOPEZ', 29, 4, '', 'ZF'),
(18370634, NULL, NULL, 'NEIDER EUGENIO', 'ARDILA ANGULO', 28, 4, '', 'ZF'),
(18371281, NULL, NULL, 'ARLIXON FREISLAK', 'PEREZ ZAMORA', 29, 4, '', 'ZF'),
(18371604, NULL, NULL, 'JULIO CESAR', 'RUIZ TABARES', 29, 4, '', 'ZF'),
(18417869, NULL, NULL, 'ARBEY DE JESUS', 'MESA ORTIZ', 29, 4, '', 'ZF'),
(18504440, NULL, NULL, 'HECTOR AUGUSTO', 'RIVERA BEDOYA', 3, 4, '', 'PLANTA 1'),
(18505447, NULL, NULL, 'GENRY ', 'ALFONSO DIAZ', 20, 4, '', 'PLANTA 2'),
(18506539, NULL, NULL, 'FRANCISCO ', 'RODRIGUEZ CASTRILLON', 8, 4, '', 'PLANTA 1'),
(18506637, NULL, NULL, 'JUAN CARLOS', 'ZAPATA JARAMILLO', 20, 4, '', 'PLANTA 2'),
(18506979, NULL, NULL, 'LEONEL ', 'PEREZ TRIANA', 19, 4, '', 'PLANTA 2'),
(18507418, NULL, NULL, 'OMAR ', 'CHICA CASTANO', 29, 4, '', 'ZF'),
(18508142, NULL, NULL, 'ALBERTO ', 'CARMONA BUILES', 19, 4, '', 'PLANTA 2'),
(18509875, NULL, NULL, 'ARIEL ', 'BETANCUR MUNOZ', 3, 4, '', 'PLANTA 1'),
(18510255, NULL, NULL, 'JUAN CARLOS', 'GARCIA GARCIA', 5, 4, '', 'PLANTA 1'),
(18513936, NULL, NULL, 'EDUAR EVELIO', 'VILLEGAS RIOS', 21, 4, '', 'PLANTA 1'),
(18514884, NULL, NULL, 'MIGUEL ANGEL', 'VALENCIA LOPEZ', 2, 4, '', 'PLANTA 2'),
(18514939, NULL, NULL, 'JHON JAIRO', 'PALACIO MARIN', 14, 4, '', 'PLANTA 2'),
(18514987, NULL, NULL, 'CARLOS ANDRES', 'OCAMPO PINO', 8, 4, '', 'PLANTA 1'),
(18515095, NULL, NULL, 'GEOVANNY ALBERTO', 'CANO QUICENO', 12, 4, '', 'PLANTA 1'),
(18515228, NULL, NULL, 'FERNANDO ALBERTO', 'BUITRAGO OSORNO', 20, 4, '', 'PLANTA 2'),
(18515305, NULL, NULL, 'MARIO ALEJANDRO', 'MARIN RAIGOSA', 26, 4, '', 'PLANTA 1'),
(18515497, NULL, NULL, 'JOHN FABER', 'HENAO BALLESTEROS', 3, 4, '', 'PLANTA 1'),
(18515690, NULL, NULL, 'HOWARD STEWAR', 'TABARES QUINTERO', 20, 4, '', 'PLANTA 2'),
(18516512, NULL, NULL, 'ANDRES MAURICIO', 'CARDONA PEREZ', 5, 4, '', 'PLANTA 1'),
(18516934, NULL, NULL, 'ORLANDO ', 'RAMIREZ CASTANO', 21, 4, '', 'PLANTA 2'),
(18517018, NULL, NULL, 'JOSE EDWIN', 'CARDENAS RAMIREZ', 1, 4, '', 'PLANTA 1'),
(18517261, NULL, NULL, 'OSCAR EDUARDO', 'CUARTAS MORALES', 1, 4, '', 'PLANTA 1'),
(18517284, NULL, NULL, 'OSCAR HERNANDO', 'CORTES MEDINA', 23, 4, '', 'PLANTA 1'),
(18517426, '18517426', '123', 'JHON EDWARD', 'VALENCIA VELASQUEZ', 19, 2, 'edward@magnetron.com.co', 'PLANTA 2'),
(18517440, NULL, NULL, 'JEFFERSON ANDRES', 'TARAPUES RAMIREZ', 5, 4, '', 'PLANTA 1'),
(18517781, NULL, NULL, 'WILLIAM ANDRES', 'LOPEZ ARIAS', 8, 4, '', 'PLANTA 1'),
(18517908, NULL, NULL, 'RUBEN DARIO', 'RIOS GONZALEZ', 19, 4, '', 'PLANTA 2'),
(18517972, NULL, NULL, 'CARLOS ALBERTO', 'MARTINEZ VALENCIA', 12, 4, '', 'PLANTA 1'),
(18518124, NULL, NULL, 'OSVALDO ', 'ZAPATA TORO', 12, 4, '', 'PLANTA 1'),
(18518129, NULL, NULL, 'JORGE ALEXANDER', 'GALEANO RUIZ', 14, 4, '', 'PLANTA 2'),
(18518155, NULL, NULL, 'MILLER DAURIEN', 'VARGAS GARCIA', 12, 4, '', 'PLANTA 1'),
(18518419, NULL, NULL, 'STEWART ', 'BEDOYA TORRES', 19, 4, '', 'PLANTA 1'),
(18518483, NULL, NULL, 'WILINTON ', 'ORTIZ CLAVIJO', 8, 4, '', 'PLANTA 1'),
(18518496, NULL, NULL, 'JHON ALEXANDER', 'VASQUEZ VALENCIA', 23, 4, '', 'PLANTA 1'),
(18518842, NULL, NULL, 'JOSUE DAVID', 'OSPINA VANEGAS', 8, 4, '', 'PLANTA 1'),
(18519155, NULL, NULL, 'DIEGO ALEXANDER', 'ARROYAVE MORALES', 8, 4, '', 'PLANTA 1'),
(18519284, NULL, NULL, 'JOSE JOHANNY', 'SOTO RAMIREZ', 26, 4, '', 'PLANTA 1'),
(18519692, NULL, NULL, 'PABLO ALEJANDRO', 'RESTREPO GARCIA', 5, 4, '', 'PLANTA 1'),
(18520170, NULL, NULL, 'CESAR ANDRES', 'ZAPATA ESTRADA', 5, 4, '', 'PLANTA 1'),
(18520346, NULL, NULL, 'DALADIER ', 'FRANCO SALAZAR', 5, 4, '', 'PLANTA 1'),
(18521371, '18521371', '123', 'DIEGO MAURICIO', 'SANCHEZ CARDONA', 10, 1, 'diegosanchez@magnetron.com.co', 'PLANTA 1'),
(18522314, NULL, NULL, 'ALEXIS ', 'GARCIA MARIN', 21, 4, '', 'PLANTA 1'),
(18522639, NULL, NULL, 'ALES MAURICIO', 'CANAS POSADA', 3, 4, '', 'PLANTA 1'),
(18531489, NULL, NULL, 'OMAR ALONSO', 'GRISALES MONTOYA', 2, 4, '', 'PLANTA 2'),
(18592487, NULL, NULL, 'NELSON ', 'CASTRO LOPEZ', 14, 4, '', 'PLANTA 2'),
(18593173, NULL, NULL, 'CARLOS ARTURO', 'AGUDELO ZAMORA', 5, 4, '', 'PLANTA 2'),
(18593808, NULL, NULL, 'JAIRO MARIA', 'ARIAS VALLEJO', 3, 4, '', 'PLANTA 1'),
(18595811, NULL, NULL, 'BERNARDO ', 'GOMEZ DAVILA', 10, 4, '', 'PLANTA 1'),
(18596044, NULL, NULL, 'HERIBERTO ', 'RENDON CEBALLOS', 17, 4, '', 'PLANTA 1'),
(18597208, NULL, NULL, 'GEOVANI ', 'VARGAS CARDONA', 2, 4, '', 'PLANTA 2'),
(18598016, NULL, NULL, 'JHON JAIRO', 'LOPEZ GONZALEZ', 8, 4, '', 'PLANTA 1'),
(18601114, NULL, NULL, 'LUIS GONZAGA', 'PELAEZ HEREDIA', 15, 4, '', 'PLANTA 1'),
(18601130, NULL, NULL, 'SILVIO ANDRES', 'GOMEZ ALVAREZ', 19, 4, '', 'PLANTA 2'),
(18606952, NULL, NULL, 'WILLIAM HERNAN', 'MUNOZ ZAPATA', 20, 4, '', 'PLANTA 2'),
(18607575, NULL, NULL, 'GREGORIO EDUARDO', 'CAIRASCO GALLEGO', 20, 4, '', 'PLANTA 2'),
(18608420, NULL, NULL, 'CARLOS AUGUSTO', 'VANEGAS BEDOYA', 2, 4, '', 'PLANTA 2'),
(18609027, NULL, NULL, 'JOSE JAVIER', 'LONDONO VELASQUEZ', 14, 4, '', 'PLANTA 2'),
(18609364, NULL, NULL, 'DANNY ALEJANDRO', 'SUAREZ AGUIRRE', 19, 4, '', 'PLANTA 2'),
(18615827, NULL, NULL, 'ALEJANDRO ', 'GUTIERREZ AMAYA', 19, 4, '', 'PLANTA 2'),
(18616508, NULL, NULL, 'CAMILO AUGUSTO', 'MONTOYA SANZ', 10, 4, '', 'PLANTA 1'),
(22667536, NULL, NULL, 'GICELA PAOLA', 'SANJUAN PAEZ', 27, 4, '', 'PLANTA 1'),
(24344561, NULL, NULL, 'ADRIANA JULIETH', 'VALENCIA FRANCO', 9, 4, '', 'PLANTA 2'),
(24373010, NULL, NULL, 'ADRIANA PATRICIA', 'CASTANO LOPEZ', 19, 4, '', 'PLANTA 1'),
(25038450, NULL, NULL, 'LUCY ', 'CLAVIJO MARIA', 3, 4, '', 'PLANTA 1'),
(25159031, NULL, NULL, 'ADRIANA MARIA', 'SANCHEZ VALLEJO', 16, 4, '', 'PLANTA 1'),
(25175167, '25175167', '123', 'LUISA INES', 'OSORIO SALCEDO', 9, 4, 'luisao@magnetron.com.co', 'PLANTA 2'),
(25175268, NULL, NULL, 'BIBIANA MARIA', 'MOLINA RODRIGUEZ', 1, 4, '', 'PLANTA 1'),
(25179820, NULL, NULL, 'CLAUDIA PATRICIA', 'CASTRO PEREZ', 16, 4, '', 'PLANTA 1'),
(25181816, NULL, NULL, 'CATERINE ', 'AGUDELO BELTRAN', 6, 4, '', 'PLANTA 1'),
(30361269, NULL, NULL, 'PAULA ANDREA', 'GIRALDO VALENCIA', 3, 4, '', 'PLANTA 1'),
(31276549, NULL, NULL, 'LILIANA ', 'POSADA GRILLO', 1, 4, '', 'PLANTA 1'),
(31421753, NULL, NULL, 'CLAUDIA LILIANA', 'OSORIO SALAZAR', 4, 4, '', 'PLANTA 2'),
(31426160, NULL, NULL, 'SANDRA MILENA', 'GOMEZ PAREJA', 7, 4, '', 'PLANTA 1'),
(31429238, NULL, NULL, ' JESICA', 'VALENCIA', 1, 4, '', 'PLANTA 1'),
(32180900, '32180900', '123', 'LUCIA CAROLINA', 'SALINAS ZAPATA', 6, 4, 'lcsalinas@magnetron.com.co', 'PLANTA 1'),
(32763104, NULL, NULL, 'LILIA ', 'ALZATE SEPULVEDA', 9, 4, '', 'PLANTA 2'),
(32776323, NULL, NULL, 'EVELYN MARIA', 'CANTILLO ROJAS', 1, 4, '', 'PLANTA 1'),
(41929244, NULL, NULL, 'LUZ ARELIS', 'LOPEZ TREJOS', 28, 4, '', 'ZF'),
(41957382, NULL, NULL, 'LUZ BRENDA', 'ARIAS OSORIO', 28, 4, '', 'ZF'),
(42005789, NULL, NULL, 'LUZ STELLA', 'CARMONA BUILES', 6, 4, '', 'PLANTA 1'),
(42015263, NULL, NULL, 'LUZ ESTELA', 'MARSIGLIA PUELLO', 1, 4, '', 'PLANTA 1'),
(42017705, NULL, NULL, 'LUZ ADRIANA', 'AGUIRRE PELAEZ', 3, 4, '', 'PLANTA 1'),
(42018278, NULL, NULL, 'MILENA MARIA', 'JIMENEZ HERNANDEZ', 1, 4, '', 'PLANTA 1'),
(42019049, NULL, NULL, 'JHOANA ANDREA', 'JIMENEZ GOMEZ', 1, 4, '', 'PLANTA 1'),
(42066092, NULL, NULL, 'MARIA NANCY', 'PEREZ URREA', 16, 4, '', 'PLANTA 1'),
(42070503, NULL, NULL, 'MARTHA LUCIA', 'BEDOYA ORTIZ', 19, 4, '', 'PLANTA 1'),
(42085644, NULL, NULL, 'NOHEMY ', 'GALLEGO BRITTO', 9, 4, '', 'PLANTA 2'),
(42088413, NULL, NULL, 'OLGA LUCIA', 'FRANCO HENAO', 1, 4, '', 'PLANTA 1'),
(42091967, NULL, NULL, 'ARGENIS ', 'SANCHEZ GOMEZ', 3, 4, '', 'PLANTA 1'),
(42092910, NULL, NULL, 'LUZ AMPARO', 'SALAZAR CARDONA', 6, 4, '', 'PLANTA 1'),
(42106475, NULL, NULL, 'SANDRA MILENA', 'PARIS RAMIREZ', 9, 4, '', 'PLANTA 2'),
(42115182, NULL, NULL, 'MYRIAM PATRICIA', 'LOAIZA AMESQUITA', 10, 4, '', 'PLANTA 2'),
(42119105, NULL, NULL, 'DIANA MARIA', 'ALVAREZ PULGARIN', 6, 4, '', 'PLANTA 1'),
(42120279, NULL, NULL, 'LUZ IRENE', 'CALVO CASTANO', 6, 4, '', 'PLANTA 2'),
(42121677, NULL, NULL, 'DIANA PATRICIA', 'HERNANDEZ RESTREPO', 12, 4, '', 'PLANTA 1'),
(42127752, NULL, NULL, 'ELIZABETH ', 'SEPULVEDA TABAREZ', 10, 4, '', 'PLANTA 1'),
(42129201, NULL, NULL, 'GLADYS ', 'OROZCO NERY', 3, 4, '', 'PLANTA 1'),
(42134841, NULL, NULL, 'ALEJANDRA MARIA', 'MORALES MORALES', 19, 4, '', 'PLANTA 1'),
(42136110, NULL, NULL, 'JOHANA ANDREA', 'CORREA TORO', 3, 4, '', 'PLANTA 1'),
(42142372, NULL, NULL, 'DIANA ALEXANDRA', 'CASTANEDA SOTO', 28, 4, '', 'ZF'),
(42142393, NULL, NULL, 'YADI MILENA', 'OSORIO LOPEZ', 6, 4, '', 'PLANTA 1'),
(42145367, NULL, NULL, 'MARY LUZ', 'RUIZ JARAMILLO', 16, 4, '', 'PLANTA 1'),
(42145509, '42145509', '123', 'CAROLINA ', 'CASTILLO LOPEZ', 4, 1, 'carolinacastillo@magnetron.com.co', 'PLANTA 1'),
(42147286, NULL, NULL, 'LUZ ANDREA', 'HENAO FRANCO', 16, 4, '', 'PLANTA 1'),
(42152382, NULL, NULL, 'MARIA LUZ ENIT', 'GIRALDO QUICENO', 5, 4, '', 'PLANTA 1'),
(42154911, NULL, NULL, 'CLAUDIA ANDREA', 'VARGAS RIOS', 3, 4, '', 'PLANTA 1'),
(42159037, NULL, NULL, 'MARIA FERNANDA', 'HERNANDEZ SARMIENTO', 1, 4, '', 'PLANTA 1'),
(42161257, NULL, NULL, 'BIBIANA ', 'LONDONO GONZALEZ', 16, 4, '', 'PLANTA 1'),
(42162296, NULL, NULL, 'LINA MARIA', 'VELEZ GOMEZ', 12, 4, '', 'PLANTA 2'),
(43448768, NULL, NULL, 'NIDIA SOFIA', 'ARANGO HURTADO', 9, 4, '', 'PLANTA 2'),
(43596373, NULL, NULL, 'PIEDAD PATRICIA', 'RESTREPO PUERTA', 1, 4, '', 'PLANTA 1'),
(51791651, '51791651', '123', 'CLAUDIA M', 'GOMEZ MOSCOSO', 6, 1, 'jeisoncordoba@magnetron.com.co', 'PLANTA 1'),
(66746630, NULL, NULL, 'YANETH PATRICIA', 'GRANADOS CARDONA', 28, 4, '', 'ZF'),
(70954839, NULL, NULL, 'JUAN DIEGO', 'GUTIERREZ RENDON', 18, 4, '', 'PLANTA 2'),
(72050463, NULL, NULL, 'HERNANDO ANTONIO', 'BARRAZA SANCHEZ', 27, 4, '', 'PLANTA 1'),
(72274003, NULL, NULL, 'OMAR ', 'RADA REALES', 27, 4, '', 'PLANTA 1'),
(75004763, NULL, NULL, 'JULIAN ', 'OROZCO ARDILA', 13, 4, '', 'PLANTA 2'),
(75051806, '75051806', '123', 'HUGO NELSON', 'CHAVERRA OSPINA', 31, 1, 'hugonchaverra@magnetron.com.co', 'PLANTA 2'),
(75091651, NULL, NULL, 'LUIS JOVANI', 'VALENCIA ZAPATA', 12, 4, '', 'PLANTA 1'),
(75092204, NULL, NULL, 'CARLOS ADOLFO', 'AVILA LARGO', 16, 4, '', 'PLANTA 1'),
(75099412, '75099412', '123', 'CARLOS ALBERTO', 'SERRANO HUERTAS', 13, 1, 'csh@magnetron.com.co', 'PLANTA 1'),
(75145961, NULL, NULL, 'WILLIAM TADEO', 'NARBAEZ GOMEZ', 13, 4, '', 'PLANTA 1'),
(76325863, '76325863', '123', 'RICHARD ERNESTO', 'HIGUITA MUNOZ', 15, 1, 'rhiguita@magnetron.com.co', 'PLANTA 1'),
(79600258, '79600258', '123', 'JUAN FRANCISCO', 'CORDOBA ALGARRA', 12, 1, 'juancordoba@magnetron.com.co', 'PLANTA 1'),
(79694778, NULL, NULL, 'CESAR AUGUSTO', 'SANCHEZ GONZALEZ', 2, 4, '', 'PLANTA 2'),
(79959915, '79959915', '123', 'DIEGO ERNESTO', 'ZULETA ORTIGOZA', 20, 1, 'diegozuleta@magnetron.com.co', 'PLANTA 1'),
(80886808, NULL, NULL, 'JUAN PABLO', 'GUTIERREZ HENAO', 10, 4, '', 'PLANTA 1'),
(82384611, NULL, NULL, 'VICTOR ', 'DAVILA SIERRA', 17, 4, '', 'PLANTA 1'),
(89007632, NULL, NULL, 'JOHN ALEXANDER', 'VIANA BOLANOS', 20, 4, '', 'PLANTA 2'),
(93285542, NULL, NULL, 'CARLOS ANTONIO', 'RUIZ ORTIZ', 14, 4, '', 'PLANTA 2'),
(93439500, NULL, NULL, 'JHON DEIVER', 'GRANADOS ARANGO', 8, 4, '', 'PLANTA 1'),
(94286513, NULL, NULL, 'GIOVANNY ANDRES', 'TORO ARBOLEDA', 20, 4, '', 'PLANTA 2'),
(94480695, NULL, NULL, 'EDIER ', 'CABRERA LATORRE', 29, 4, '', 'ZF'),
(94526524, NULL, NULL, 'PABLO CESAR', 'JARAMILLO PENA', 13, 4, '', 'PLANTA 2'),
(1004627204, NULL, NULL, 'YAMID ALEJANDRO', 'TORO DUQUE', 2, 4, '', 'PLANTA 2'),
(1004671267, NULL, NULL, 'BERNARDO ', 'RODRIGUEZ HENAO', 2, 4, '', 'PLANTA 2'),
(1007220471, NULL, NULL, 'DAVID ', 'CORREA CARLOS', 2, 4, '', 'PLANTA 2'),
(1010071465, NULL, NULL, 'YINA JULIANA', 'CORREA MORALES', 6, 4, '', 'PLANTA 1'),
(1010100373, NULL, NULL, 'ANGEL ', 'GUERRA MIGUEL', 19, 4, '', 'PLANTA 1'),
(1016053728, NULL, NULL, 'JUAN ANDRES', 'GORDILLO HERRERA', 5, 4, '', 'PLANTA 1'),
(1020396004, NULL, NULL, 'JOHN ESTIBEN', 'QUINTERO ARANGO', 20, 4, '', 'PLANTA 2'),
(1026260962, NULL, NULL, 'ANGELLO ENRIQUE', 'MORALES GARCIA', 19, 4, '', 'PLANTA 2'),
(1036628502, NULL, NULL, 'FERNAN CAMILO', 'GRANADOS CADAVID', 21, 4, '', 'PLANTA 2'),
(1039694581, NULL, NULL, 'ANDRES JAVIER', 'ZAMORA SUAREZ', 17, 4, '', 'PLANTA 1'),
(1053767608, '1053767608', '123', 'JUAN SEBASTIAN', 'ALZATE MEJIA', 14, 1, 'sebastianalzate@magnetron.com.co', 'PLANTA 2'),
(1053769909, NULL, NULL, 'SERGIO RICARDO', 'DIAZ JIMENEZ', 5, 4, '', 'PLANTA 1'),
(1054549347, NULL, NULL, 'VICTOR HUGO', 'GALINDO RAMIREZ', 13, 4, '', 'PLANTA 1'),
(1054916058, NULL, NULL, 'LUIS FERNANDO', 'GRAJALES GIL', 3, 4, '', 'PLANTA 1'),
(1054988405, NULL, NULL, 'JAMES ANDRES', 'MORENO MORENO', 29, 4, '', 'ZF'),
(1055830366, NULL, NULL, 'JORGE IVAN', 'PATINO ACEVEDO', 14, 4, '', 'PLANTA 2'),
(1055832850, NULL, NULL, 'JUAN DAVID', 'ACEVEDO MARQUEZ', 13, 4, '', 'PLANTA 2'),
(1060592055, NULL, NULL, 'DEYSI DANIELA', 'ALZATE SANCHEZ', 6, 4, '', 'PLANTA 1'),
(1061368370, NULL, NULL, 'YESID ALEJANDRO', 'RENTERIA MONTOYA', 20, 4, '', 'PLANTA 2'),
(1061371611, NULL, NULL, 'JUAN FELIPE', 'GALICIA ARANGO', 12, 4, '', 'PLANTA 1'),
(1085306116, NULL, NULL, 'DIANA CAROLINA', 'ERAZO LASSO', 19, 4, '', 'PLANTA 1'),
(1087487900, NULL, NULL, 'NATALY ', 'SOLORZANO OROZCO', 19, 4, '', 'PLANTA 1'),
(1087546428, NULL, NULL, 'JHON ALBERTO', 'LEON TABARES', 20, 4, '', 'PLANTA 2'),
(1087546748, NULL, NULL, 'DANIEL ALBERTO', 'YEPES AGUIRRE', 20, 4, '', 'PLANTA 2'),
(1087549345, NULL, NULL, 'JUAN CARLOS', 'AGUIRRE BARTOLO', 2, 4, '', 'PLANTA 2'),
(1087549347, NULL, NULL, 'LUIS EVELIO', 'AGUIRRE BARTOLO', 2, 4, '', 'PLANTA 2'),
(1087549369, NULL, NULL, 'JEIMY DAIHANA', 'PATINO HERNANDEZ', 10, 4, '', 'PLANTA 1'),
(1087549603, NULL, NULL, 'LUIS NOLBERTO', 'MARIN MOLINA', 13, 4, '', 'PLANTA 2'),
(1087549753, NULL, NULL, 'ERIKA ANDREA', 'PAREJA PARRA', 19, 4, '', 'PLANTA 1'),
(1087550518, NULL, NULL, 'DANIEL ', 'AGUIRRE CASTRILLON', 2, 4, '', 'PLANTA 2'),
(1087552035, '1087552035', '123', 'ANDRES ADOLFO', 'AGUDELO LONDONO', 27, 1, 'andresagudelo@magnetron.com.co', 'PLANTA 2'),
(1087552232, NULL, NULL, 'HECTOR FABIO', 'VILLEGAS DIAZ', 8, 4, '', 'PLANTA 1'),
(1087552417, NULL, NULL, 'JEINNER ANDRES', 'VARGAS COLORADO', 17, 4, '', 'PLANTA 1'),
(1087553861, NULL, NULL, 'RUBEN DARIO', 'CASTANEDA SERNA', 14, 4, '', 'PLANTA 2'),
(1087553862, NULL, NULL, 'LUIS ALBERTO', 'HERRERA SALAZAR', 13, 4, '', 'PLANTA 2'),
(1087554043, NULL, NULL, 'EDWIN MARINO', 'VALLEJO BERRIO', 8, 4, '', 'PLANTA 1'),
(1087554193, NULL, NULL, 'ALEJANDRO ', 'MONTOYA TABORDA', 2, 4, '', 'PLANTA 2'),
(1087554207, NULL, NULL, 'JUAN CAMILO', 'GIL CONTRERAS', 2, 4, '', 'PLANTA 2'),
(1087554242, NULL, NULL, 'JUAN DAVID', 'RESTREPO OCAMPO', 14, 4, '', 'PLANTA 2'),
(1087554714, NULL, NULL, 'LEONARDO ', 'BETANCUR PEREZ', 2, 4, '', 'PLANTA 2'),
(1087554826, NULL, NULL, 'JUAN CAMILO', 'GOMEZ ALVAREZ', 12, 4, '', 'PLANTA 1'),
(1087556136, NULL, NULL, 'VICTOR JULIAN', 'GIRALDO FLOREZ', 26, 4, '', 'PLANTA 1'),
(1087556414, NULL, NULL, 'JUAN MANUEL', 'BUENO GALLEGO', 15, 4, '', 'PLANTA 2'),
(1087556462, NULL, NULL, 'FABIANA ', 'OSPINA RAIGOZA', 1, 4, '', 'PLANTA 1'),
(1087556597, NULL, NULL, 'YONIER ANDRES', 'ORTEGA RIVERA', 17, 4, '', 'PLANTA 1'),
(1087556665, NULL, NULL, 'YESID CAMILO', 'FRANCO DIAZ', 3, 4, '', 'PLANTA 1'),
(1087556728, NULL, NULL, 'DANNY ALEJANDRO', 'ALZATE CARDONA', 2, 4, '', 'PLANTA 2'),
(1087556887, NULL, NULL, 'CARLOS ADRIAN', 'COLORADO BEDOYA', 5, 4, '', 'PLANTA 1'),
(1087557141, NULL, NULL, 'AYDER ALONSO', 'ESCUDERO JIMENEZ', 2, 4, '', 'PLANTA 2'),
(1087557490, NULL, NULL, 'YEISON ALEXANDER', 'GORDILLO CARDONA', 3, 4, '', 'PLANTA 1'),
(1087557643, NULL, NULL, 'JUAN DIEGO', 'PELAEZ ZAPATA', 14, 4, '', 'PLANTA 2'),
(1087557776, NULL, NULL, 'DANIEL ', 'HERRERA DIAZ', 19, 4, '', 'PLANTA 1'),
(1087558740, NULL, NULL, 'ANDRES ', 'CANAVERAL CARDENAS', 14, 4, '', 'PLANTA 2'),
(1087559954, NULL, NULL, 'JESUS ANDRES', 'LEON TABARES', 19, 4, '', 'PLANTA 1'),
(1087985348, NULL, NULL, 'JHONY ALEXANDER', 'GOMEZ MARIN', 19, 4, '', 'PLANTA 1'),
(1087985543, NULL, NULL, 'MANUEL ANDRES', 'ESTRADA VANEGAS', 2, 4, '', 'PLANTA 2'),
(1087987471, NULL, NULL, 'JOHN EFRAIN', 'MEJIA VASQUEZ', 21, 4, '', 'PLANTA 2'),
(1087989689, NULL, NULL, 'JULIAN ANDRES', 'VALENCIA GAVIRIA', 21, 4, '', 'PLANTA 2'),
(1087989780, NULL, NULL, 'JOHN BAYRON', 'GOMEZ MOLINA', 20, 4, '', 'PLANTA 2'),
(1087989986, NULL, NULL, 'VERONICA ', 'RUBIO HENAO', 16, 4, '', 'PLANTA 1'),
(1087991055, NULL, NULL, 'JAIME ANDRES', 'AVILA CARDENAS', 19, 4, '', 'PLANTA 2'),
(1087991166, NULL, NULL, 'CRISTHIAN ', 'SALAZAR RODRIGUEZ', 12, 4, '', 'PLANTA 1'),
(1087992016, NULL, NULL, 'JHON EDISON', 'RAMIREZ TORO', 3, 4, '', 'PLANTA 1'),
(1087993223, NULL, NULL, 'HUGO ALBEIRO', 'TANGARIFE HERRERA', 21, 4, '', 'PLANTA 1'),
(1087994531, NULL, NULL, 'RUBEN DARIO', 'CASTILLO CASTANO', 18, 4, '', 'PLANTA 2'),
(1087994682, NULL, NULL, 'JEISON ALBERTO', 'CASTANO VALLEJO', 2, 4, '', 'PLANTA 2'),
(1087994994, NULL, NULL, 'JORGE HERNAN', 'VALLEJO ARENAS', 1, 4, '', 'PLANTA 1'),
(1087995634, NULL, NULL, 'LUIS CARLOS ALEJANDRO', 'JIMENEZ RENDON', 2, 4, '', 'PLANTA 2'),
(1087996260, NULL, NULL, 'JHON EDISON', 'HOLGUIN CARVAJAL', 19, 4, '', 'PLANTA 1'),
(1087996707, NULL, NULL, 'FABIAN ANDRES', 'MONTOYA AGUIRRE', 2, 4, '', 'PLANTA 2'),
(1087997018, NULL, NULL, 'ELIZABETH ', 'NARANJO JEREZ', 1, 4, '', 'PLANTA 1'),
(1087997075, NULL, NULL, 'ANDRES FELIPE', 'DURAN TAPIAS', 3, 4, '', 'PLANTA 1'),
(1087997832, NULL, NULL, 'MAURICIO ', 'HERNANDEZ GOMEZ', 19, 4, '', 'PLANTA 1'),
(1087997849, NULL, NULL, 'DANIEL ', 'RENDON IBARRA', 8, 4, '', 'PLANTA 1'),
(1087999234, NULL, NULL, 'JORGE ANDRES', 'LOPEZ BURITICA', 14, 4, '', 'PLANTA 2'),
(1087999600, NULL, NULL, 'JUAN PABLO', 'SANCHEZ GASPAR', 2, 4, '', 'PLANTA 2'),
(1088001509, NULL, NULL, 'MILTON ANDRES', 'CASTRILLON SOTO', 3, 4, '', 'PLANTA 1'),
(1088002641, NULL, NULL, 'HERNAN DAVID', 'GIRALDO HERRERA', 14, 4, '', 'PLANTA 2'),
(1088002892, NULL, NULL, 'LINA MARCELA', 'MARIN HURTADO', 1, 4, '', 'PLANTA 1'),
(1088004425, '1088004425', '123', 'JEISON STEVENS', 'CORDOBA SANCHEZ', 1, 1, 'jeisoncordoba@magnetron.com.co', 'PLANTA 2'),
(1088004664, NULL, NULL, 'CLAUDIA FERNANDA', 'ARROYAVE MOLINA', 3, 4, '', 'PLANTA 1'),
(1088004902, NULL, NULL, 'ARIEL ALEJANDRO', 'BASTIDAS ORTEGA', 21, 4, '', 'PLANTA 1'),
(1088005281, NULL, NULL, 'YEFERSON ', 'VALBUENA MARULANDA', 14, 4, '', 'PLANTA 2'),
(1088006120, NULL, NULL, 'CRISTIAN ', 'SANCHEZ VALENCIA', 19, 4, '', 'PLANTA 1'),
(1088006945, NULL, NULL, 'OSCAR EDUARDO', 'ALVAREZ GARCIA', 11, 4, '', 'PLANTA 1'),
(1088008050, NULL, NULL, 'JHOAN ALEXANDER', 'GONZALEZ CAMARGO', 13, 4, '', 'PLANTA 2'),
(1088009483, NULL, NULL, 'MATEO ALEXANDER', 'ZULETA ARENAS', 2, 4, '', 'PLANTA 2'),
(1088010008, NULL, NULL, 'CARLOS ANDRES', 'CARMONA RINCON', 3, 4, '', 'PLANTA 1'),
(1088010212, NULL, NULL, 'SEBASTIAN ', 'TABARES GONZALEZ', 11, 4, '', 'PLANTA 1'),
(1088011317, NULL, NULL, 'ADRIAN HUMBERTO', 'SUAREZ OSORIO', 7, 4, '', 'PLANTA 1'),
(1088012216, NULL, NULL, 'BRIAN ARIEL', 'OSORIO RAMIREZ', 11, 4, '', 'PLANTA 1'),
(1088013767, '1088013767', '123', 'JUAN PABLO', 'CARRASCO CANAS', 24, 1, 'juancarrasco@magnetron.com.co', 'PLANTA 1'),
(1088015101, NULL, NULL, 'CRISTHIAN DAVID', 'MARIN BLANDON', 1, 4, '', 'PLANTA 1'),
(1088017268, NULL, NULL, 'CRISTIAN DAVID', 'HERRERA GALLO', 17, 4, '', 'PLANTA 1'),
(1088017974, NULL, NULL, 'ANA MARIA', 'HERNANDEZ RIVERA', 5, 4, '', 'PLANTA 1'),
(1088018113, NULL, NULL, 'SEBASTIAN ', 'MARTINEZ CARDENAS', 10, 4, '', 'PLANTA 1'),
(1088018398, NULL, NULL, 'SEBASTIAN ', 'ROMERO PATINO', 5, 4, '', 'PLANTA 1'),
(1088026571, NULL, NULL, 'SEBASTIAN ', 'GALEANO RUIZ', 19, 4, '', 'PLANTA 1'),
(1088032376, NULL, NULL, 'JUAN CAMILO', 'MARTINEZ DIAZ', 19, 4, '', 'PLANTA 1'),
(1088236832, NULL, NULL, 'GUSTAVO ADOLFO', 'CASTANO CIFUENTES', 3, 4, '', 'PLANTA 1'),
(1088237115, '1088237115', '123', 'DIEGO ALEJANDRO', 'BETANCOURTH GONZALEZ', 30, 1, 'diegobetancourt@magnetron.com.co', 'PLANTA 2'),
(1088237670, NULL, NULL, 'DARWIN ', 'CASTILLO PACHECO', 5, 4, '', 'PLANTA 2'),
(1088237674, NULL, NULL, 'JHOSMAN JULIAN', 'GRISALES SANCHEZ', 3, 4, '', 'PLANTA 1'),
(1088237973, NULL, NULL, 'JAIRO ANDRES', 'RESTREPO VASCO', 1, 4, '', 'PLANTA 1'),
(1088240425, NULL, NULL, 'JOHN EDISSON', 'GUTIERREZ GRANADOS', 19, 4, '', 'PLANTA 1'),
(1088241360, NULL, NULL, 'CRHISTIAN DAVID', 'ROJAS RUIZ', 19, 4, '', 'PLANTA 1'),
(1088241441, NULL, NULL, 'WILDER JULIAN', 'GARCIA CANO', 13, 4, '', 'PLANTA 1'),
(1088242360, NULL, NULL, 'DIEGO ARMANDO', 'SARAY GALVIS', 3, 4, '', 'PLANTA 1'),
(1088242831, NULL, NULL, 'CRISTIAN ESTEBAN', 'LOAIZA CORREA', 12, 4, '', 'PLANTA 1'),
(1088243058, NULL, NULL, 'VICTOR ALFONSO', 'GUZMAN SILVA', 8, 4, '', 'PLANTA 1'),
(1088243077, NULL, NULL, 'CARMEN EMILIA', 'GUERRERO PAREJA', 6, 4, '', 'PLANTA 2'),
(1088243362, NULL, NULL, 'WILMAR ', 'RESTREPO MOSQUERA', 19, 4, '', 'PLANTA 1'),
(1088244525, NULL, NULL, 'ANDRES FELIPE', 'GRAJALES LOPEZ', 1, 4, '', 'PLANTA 1'),
(1088245727, NULL, NULL, 'JUAN CARLOS', 'RODRIGUEZ RAMIREZ', 10, 4, '', 'PLANTA 1'),
(1088245895, NULL, NULL, 'HERIBERTO ', 'GARCIA LONDONO', 3, 4, '', 'PLANTA 1'),
(1088246337, NULL, NULL, 'CARLOS ANDRES', 'RENDON LONDONO', 12, 4, '', 'PLANTA 1'),
(1088247122, NULL, NULL, 'ANDREY ', 'ESPINAL ARANGO', 21, 4, '', 'PLANTA 2'),
(1088247736, NULL, NULL, 'DARIO ', 'RESTREPO JAIBER', 12, 4, '', 'PLANTA 2'),
(1088248120, NULL, NULL, 'JAIME ', 'OSPINA BARRAGAN', 5, 4, '', 'PLANTA 1'),
(1088248262, '1088248262', '123', 'HARRISON ', 'HERRENO LONDONO', 19, 2, 'jeisoncordoba@magnetron.com.co', 'PLANTA 1'),
(1088248287, NULL, NULL, 'PAULA MELISSA', 'CANDAMIL LONDONO', 1, 4, '', 'PLANTA 1'),
(1088248734, NULL, NULL, 'ANDRES FELIPE', 'ZAPATA PAREJA', 3, 4, '', 'PLANTA 1'),
(1088248742, NULL, NULL, 'JOHN WILLIAM', 'CASTANEDA RUIZ', 3, 4, '', 'PLANTA 1'),
(1088249038, '1088249038', '123', 'DIEGO FERNANDO', 'MONTOYA VAQUIRO', 25, 1, 'diegofmontoya@magnetron.com.co', 'PLANTA 1'),
(1088249833, NULL, NULL, 'JONATHAN ANDRES', 'GONZALEZ AGUDELO', 20, 4, '', 'PLANTA 2'),
(1088250781, NULL, NULL, 'CHRISTIAN ', 'GALVIS HERNANDEZ', 5, 4, '', 'PLANTA 1'),
(1088251477, NULL, NULL, 'RICHARD ANDRES', 'MONTOYA PINZON', 14, 4, '', 'PLANTA 2'),
(1088251605, NULL, NULL, 'KELLY YHOANA', 'QUICENO VALLE', 10, 4, '', 'PLANTA 1'),
(1088251721, NULL, NULL, 'MARTHA LEONOR', 'LOPEZ ROMERO', 23, 4, '', 'PLANTA 1'),
(1088253045, '1088253045', '123', 'ALEJANDRO ', 'GARCIA AGUIRRE', 3, 1, 'alejandrogarcia@magnetron.com.co', 'PLANTA 1'),
(1088254066, NULL, NULL, 'ANTONY ', 'CASTANO RUIZ', 26, 4, '', 'PLANTA 1'),
(1088254137, NULL, NULL, 'JHON ALEJANDRO', 'QUINTANA ALVAREZ', 8, 4, '', 'PLANTA 1'),
(1088254767, NULL, NULL, 'JUAN PABLO', 'MEJIA PINEDA', 3, 4, '', 'PLANTA 1'),
(1088255039, NULL, NULL, 'ADRIAN FELIPE', 'VERA RIOS', 19, 4, '', 'PLANTA 1'),
(1088255197, NULL, NULL, 'JONATHAN ', 'VILLA GONZALEZ', 8, 4, '', 'PLANTA 1'),
(1088255290, NULL, NULL, 'JOHN EDISON', 'CASTANO DUQUE', 14, 4, '', 'PLANTA 2'),
(1088255745, NULL, NULL, 'CAMILO ANDRES', 'BLANDON TOBON', 3, 4, '', 'PLANTA 1'),
(1088255952, NULL, NULL, 'MARISOL ', 'OCAMPO CASTANO', 5, 4, '', 'PLANTA 1'),
(1088255978, NULL, NULL, 'JIMY ANDERSON', 'LOPEZ GUTIERREZ', 2, 4, '', 'PLANTA 2'),
(1088256280, NULL, NULL, 'ALEJANDRO ', 'LOAIZA PULGARIN', 13, 4, '', 'PLANTA 1'),
(1088257465, NULL, NULL, 'MICHAEL STEVENS', 'OROZCO PIEDRAHITA', 1, 4, '', 'PLANTA 1'),
(1088257487, NULL, NULL, 'MAURICIO ', 'JARAMILLO MOLINA', 24, 4, '', 'PLANTA 2'),
(1088258892, NULL, NULL, 'CARLOS AMILKAR', 'CALDERON RAMIREZ', 7, 4, '', 'PLANTA 1'),
(1088259096, NULL, NULL, 'JEIMY PAOLA', 'MEJIA ROMAN', 12, 4, '', 'PLANTA 1'),
(1088260236, NULL, NULL, 'NILTON ANDRES', 'VELASQUEZ RINCON', 5, 4, '', 'PLANTA 2'),
(1088260339, NULL, NULL, 'MARIANA ', 'GUTIERREZ MONTOYA', 28, 4, '', 'ZF'),
(1088260955, NULL, NULL, 'MILTON JOHANY', 'ORREGO GIRALDO', 10, 4, '', 'PLANTA 1'),
(1088261620, NULL, NULL, 'SEBASTIAN ', 'ROBLEDO LOAIZA', 10, 4, '', 'PLANTA 2'),
(1088262415, NULL, NULL, 'EDWARD ', 'OROZCO CASTRILLON', 11, 4, '', 'PLANTA 1'),
(1088262454, NULL, NULL, 'GUSTAVO ADOLFO', 'TABARES LOAIZA', 8, 4, '', 'PLANTA 1'),
(1088262973, NULL, NULL, 'JUAN ALEJANDRO', 'PATINO BANOL', 19, 4, '', 'PLANTA 1'),
(1088264751, NULL, NULL, 'ANDRES FELIPE', 'CORTES GIRALDO', 10, 4, '', 'PLANTA 1'),
(1088264843, NULL, NULL, 'ESTEBAN ', 'HERNANDEZ HERNANDEZ', 17, 4, '', 'PLANTA 1'),
(1088265434, NULL, NULL, 'ANGELA ', 'MENESES LOPEZ', 9, 4, '', 'PLANTA 2'),
(1088267991, NULL, NULL, 'JAIME ALEXANDER', 'BUSTAMANTE MONTOYA', 16, 4, '', 'PLANTA 1'),
(1088268054, NULL, NULL, 'YEISON ANDRES', 'RIOS CORREA', 14, 4, '', 'PLANTA 2'),
(1088268392, NULL, NULL, 'AZAEL ', 'GARCIA CARDONA', 8, 4, '', 'PLANTA 1'),
(1088268578, NULL, NULL, 'HUGO ALEJANDRO', 'BERMUDEZ ROSERO', 10, 4, '', 'PLANTA 1'),
(1088270315, NULL, NULL, 'MAIKON ', 'VALENCIA LOPEZ', 12, 4, '', 'PLANTA 1'),
(1088270876, NULL, NULL, 'CARLOS ANDRES', 'SEPULVEDA SANCHEZ', 5, 4, '', 'PLANTA 2'),
(1088270999, NULL, NULL, 'JAUMER ANTONIO', 'PESCADOR JARAMILLO', 20, 4, '', 'PLANTA 2'),
(1088271121, NULL, NULL, 'JOHN EDISON', 'MEJIA GOMEZ', 21, 4, '', 'PLANTA 2'),
(1088273026, NULL, NULL, 'CRISTIAN MAURICIO', 'BECERRA GONZALEZ', 20, 4, '', 'PLANTA 2'),
(1088273146, NULL, NULL, 'NATALIA ', 'ALZATE BLANDON', 25, 4, '', 'PLANTA 1'),
(1088273542, NULL, NULL, 'ALEXANDER ', 'ZAPATA SALGUERO', 19, 4, '', 'PLANTA 1'),
(1088273627, NULL, NULL, 'HECTOR FABIO', 'GOMEZ MARTINEZ', 12, 4, '', 'PLANTA 1'),
(1088274087, NULL, NULL, 'JUAN SEBASTIAN', 'OSPINA BEDOYA', 7, 4, '', 'PLANTA 1'),
(1088274322, NULL, NULL, 'CESAR GUILLERMO', 'GARCIA LOPEZ', 14, 4, '', 'PLANTA 2'),
(1088274412, NULL, NULL, 'JHON FREDY', 'GIL GRAJALES', 3, 4, '', 'PLANTA 1'),
(1088274487, NULL, NULL, 'LINA MARCELA', 'UCHIMA ARCE', 19, 4, '', 'PLANTA 1'),
(1088274547, NULL, NULL, 'LUISA MARIA', 'MENESES LOPEZ', 1, 4, '', 'PLANTA 1'),
(1088274686, NULL, NULL, 'ANDRES FELIPE', 'HOYOS SALAMANCA', 4, 4, '', 'PLANTA 2'),
(1088274776, NULL, NULL, 'JEISON JULIAN', 'CASTANEDA RUIZ', 18, 4, '', 'PLANTA 2'),
(1088274834, NULL, NULL, 'DIEGO FERNANDO', 'GANAN HERNANDEZ', 3, 4, '', 'PLANTA 1'),
(1088275271, NULL, NULL, 'CRISTIAN DARIO', 'CORTEZ MURIEL', 19, 4, '', 'PLANTA 1'),
(1088275546, NULL, NULL, 'ROBINSON ', 'BENJUMEA ARCILA', 3, 4, '', 'PLANTA 1'),
(1088276723, NULL, NULL, 'JUAN CARLOS', 'BAENA HERRERA', 13, 4, '', 'PLANTA 2'),
(1088277909, NULL, NULL, 'ANDRES GENARO', 'ROMERO GARCIA', 3, 4, '', 'PLANTA 1'),
(1088281663, NULL, NULL, 'JESSICA ALEXANDRA', 'RENDON RODRIGUEZ', 19, 4, '', 'PLANTA 1'),
(1088282444, '1088282444', '123', 'CARLOS ARTURO', 'BUSTAMANTE MONTOYA', 22, 1, 'carlosabustamante@magnetron.com.co', 'PLANTA 1'),
(1088282505, NULL, NULL, 'JOSE RAUL', 'PEREZ LOPEZ', 26, 4, '', 'PLANTA 1'),
(1088282931, NULL, NULL, 'JUAN DAVID', 'VELASQUEZ PIEDRAHITA', 10, 4, '', 'PLANTA 1'),
(1088283148, NULL, NULL, 'JHON DAIRO', 'VALENCIA ARBOLEDA', 21, 4, '', 'PLANTA 1'),
(1088283200, NULL, NULL, 'CLAUDIA MILENA', 'HENAO GRAJALES', 1, 4, '', 'PLANTA 1'),
(1088284779, NULL, NULL, 'SEBASTIAN ', 'OSPINA CASTILLO', 2, 4, '', 'PLANTA 2'),
(1088285311, NULL, NULL, 'JUAN PABLO', 'RAMIREZ LOPEZ', 8, 4, '', 'PLANTA 1'),
(1088286751, NULL, NULL, 'DANIEL ', 'DELGADO RAMIREZ', 4, 4, '', 'PLANTA 2'),
(1088287437, NULL, NULL, 'JHONNIER STEVENS', 'VERA RIOS', 26, 4, '', 'PLANTA 1'),
(1088287886, NULL, NULL, 'LINA MARIA', 'GAHYDA FRANCO', 10, 4, '', 'PLANTA 1'),
(1088288528, NULL, NULL, 'EDWIN AUGUSTO', 'SERNA VELASQUEZ', 8, 4, '', 'PLANTA 1'),
(1088288650, NULL, NULL, 'JHON JAIRO', 'MEJIA GONZALEZ', 3, 4, '', 'PLANTA 1'),
(1088290537, NULL, NULL, 'KATERINE LIZET', 'MARULANDA CORDOBA', 10, 4, '', 'PLANTA 1'),
(1088291013, NULL, NULL, 'JHON JAIRO', 'ARIAS PORRAS', 21, 4, '', 'PLANTA 1'),
(1088291042, NULL, NULL, 'CAMILO ', 'VILLEGAS VILLALOBOS', 19, 4, '', 'PLANTA 2'),
(1088291160, NULL, NULL, 'MARTHA OLIVIA', 'SANCHEZ JORDAN', 3, 4, '', 'PLANTA 1'),
(1088291329, NULL, NULL, 'NELSON DAVID', 'DIAZ MONTOYA', 17, 4, '', 'PLANTA 1'),
(1088292559, NULL, NULL, 'YEFERSON ', 'VERA RAMIREZ', 19, 4, '', 'PLANTA 1'),
(1088293866, NULL, NULL, 'LUIS EDUARDO', 'MARULANDA QUINTERO', 20, 4, '', 'PLANTA 2'),
(1088293945, NULL, NULL, 'LADY JOHANNA', 'LOPEZ GALLEGO', 12, 4, '', 'PLANTA 1'),
(1088296072, NULL, NULL, 'LUISA FERNANDA', 'PUERTA GALLEGO', 6, 4, '', 'PLANTA 1'),
(1088296496, NULL, NULL, 'CRISTINA ', 'LAM MEI', 1, 4, '', 'PLANTA 1'),
(1088297401, NULL, NULL, 'JHONATHAN FERNEY', 'SALAZAR BONILLA', 3, 4, '', 'PLANTA 1'),
(1088298929, NULL, NULL, 'MICHAEL ', 'LOZANO PATINO', 14, 4, '', 'PLANTA 2'),
(1088299114, NULL, NULL, 'JUAN CARLOS', 'SOTO CARDONA', 3, 4, '', 'PLANTA 1'),
(1088300247, NULL, NULL, 'DIEGO ', 'FALLA SANCHEZ', 14, 4, '', 'PLANTA 2'),
(1088300529, NULL, NULL, 'JUAN PABLO', 'CASTRO OCAMPO', 8, 4, '', 'PLANTA 1'),
(1088303158, NULL, NULL, 'HECTOR FABIO', 'VILLA LONDONO', 10, 4, '', 'PLANTA 2'),
(1088303315, NULL, NULL, 'OSCAR ANDRES', 'OROZCO CALLE', 20, 4, '', 'PLANTA 2'),
(1088304273, NULL, NULL, 'FAUSTO ', 'LEON PEREZ', 13, 4, '', 'PLANTA 1'),
(1088304423, NULL, NULL, 'BRENDA YULIETH', 'LOPEZ MARTINEZ', 19, 4, '', 'PLANTA 1'),
(1088304648, NULL, NULL, 'ALBERTO ', 'GUERRERO YONY', 17, 4, '', 'PLANTA 1'),
(1088304830, NULL, NULL, 'ALEJANDRO ', 'BARCO ABALO', 3, 4, '', 'PLANTA 1'),
(1088307521, NULL, NULL, 'BYRON DANIEL', 'RAMIREZ LOPEZ', 14, 4, '', 'PLANTA 2'),
(1088307869, NULL, NULL, 'LUIS ANCIZAR', 'HERNANDEZ OSORIO', 13, 4, '', 'PLANTA 1'),
(1088307898, NULL, NULL, 'HAROLD MAURICIO', 'CALLE SUAREZ', 20, 4, '', 'PLANTA 2'),
(1088309410, NULL, NULL, 'KEVIN ANDRES', 'SALAZAR TREJOS', 10, 4, '', 'PLANTA 2'),
(1088311737, NULL, NULL, 'CARLOS MARIO', 'ALZATE MONTOYA', 14, 4, '', 'PLANTA 2'),
(1088312090, NULL, NULL, 'ANDERSON ', 'BENJUMEA BUITRAGO', 20, 4, '', 'PLANTA 2'),
(1088312668, NULL, NULL, 'JHONY ALEJANDRO', 'AGUDELO PACHECO', 20, 4, '', 'PLANTA 2'),
(1088317457, NULL, NULL, 'ANGELA DANIELA', 'GOMEZ RESTREPO', 28, 4, '', 'ZF'),
(1088318173, NULL, NULL, 'MIGUEL ANGEL', 'CARMONA RIVERA', 19, 4, '', 'PLANTA 1'),
(1088318650, NULL, NULL, 'SEBASTIAN ', 'RENDON AGUDELO', 12, 4, '', 'PLANTA 1'),
(1088320375, NULL, NULL, 'BRAYAN STIVEN', 'BETANCUR CORRALES', 5, 4, '', 'PLANTA 1'),
(1088320708, NULL, NULL, 'GUSTAVO ADOLFO', 'MORALES PARRA', 19, 4, '', 'PLANTA 1'),
(1088323433, NULL, NULL, 'JUAN DIEGO', 'FLOREZ CALDERON', 8, 4, '', 'PLANTA 1'),
(1088325978, NULL, NULL, 'MIGUEL ANGEL', 'MONTOYA AGUDELO', 12, 4, '', 'PLANTA 1'),
(1088326909, NULL, NULL, 'YESID ', 'OSPINA VALENCIA', 3, 4, '', 'PLANTA 1'),
(1088328491, NULL, NULL, 'ESTEBAN ', 'JARAMILLO CEBALLOS', 13, 4, '', 'PLANTA 1'),
(1088330367, NULL, NULL, 'DIEGO ANDRES', 'ANDICA DAVILA', 11, 4, '', 'PLANTA 1'),
(1088331556, NULL, NULL, 'KAREN JOHANNA', 'GARCIA GUTIERREZ', 6, 4, '', 'PLANTA 1'),
(1088331722, NULL, NULL, 'HECTOR ALEJANDRO', 'LONDONO LOPEZ', 21, 4, '', 'PLANTA 2'),
(1088332558, NULL, NULL, 'DAVID ', 'ACEVEDO MORALES', 11, 4, '', 'PLANTA 1'),
(1088332568, NULL, NULL, 'LUIS FELIPE', 'LOPEZ CASADIEGO', 19, 4, '', 'PLANTA 1'),
(1088333386, NULL, NULL, 'SANDRA MILENA', 'HIDALGO SILVA', 19, 4, '', 'PLANTA 1'),
(1088334398, NULL, NULL, 'CRISTHIAN CAMILO', 'JARAMILLO ACEVEDO', 19, 4, '', 'PLANTA 1'),
(1088338007, NULL, NULL, 'LUIS DANIEL', 'MARIN RENDON', 13, 4, '', 'PLANTA 1'),
(1088344017, NULL, NULL, 'VICTOR ANDRES', 'GRANADA PESCADOR', 1, 4, '', 'PLANTA 1'),
(1089721159, NULL, NULL, 'GUSTAVO ALBERTO', 'TABARES RAMIREZ', 2, 4, '', 'PLANTA 2'),
(1089745600, NULL, NULL, 'DEISY TATIANA', 'RODRIGUEZ ZAPATA', 3, 4, '', 'PLANTA 1'),
(1093212537, NULL, NULL, 'ANA MARIA', 'GALLEGO ZULUAGA', 28, 4, '', 'ZF'),
(1093212998, NULL, NULL, 'JUAN PABLO', 'MARULANDA RODRIGUEZ', 21, 4, '', 'PLANTA 1'),
(1093213509, NULL, NULL, 'OSCAR DAVID', 'SIERRA MARTINEZ', 3, 4, '', 'PLANTA 1'),
(1093213577, NULL, NULL, 'ISABEL CRISTINA', 'SANCHEZ IDARRAGA', 1, 4, '', 'PLANTA 1'),
(1093214532, NULL, NULL, 'NIN YEISON', 'HENAO ALMANZA', 8, 4, '', 'PLANTA 1'),
(1093215316, NULL, NULL, 'ALEXANDER ', 'RODRIGUEZ CORTES', 12, 4, '', 'PLANTA 1'),
(1093215722, NULL, NULL, 'CESAR AUGUSTO', 'PABON PEREZ', 10, 4, '', 'PLANTA 1'),
(1093216183, NULL, NULL, 'JONATHAN ', 'CORTES OSSA', 3, 4, '', 'PLANTA 1'),
(1093218334, NULL, NULL, 'GLORIA MARCELA', 'PAREJA ESPINOSA', 25, 4, '', 'PLANTA 1'),
(1093219524, NULL, NULL, 'JHONNY ALEXANDER', 'AGUDELO GONZALEZ', 2, 4, '', 'PLANTA 2'),
(1093219599, NULL, NULL, 'CARLOS ARIEL', 'MALDONADO MEJIA', 3, 4, '', 'PLANTA 1'),
(1093219925, NULL, NULL, 'LIZETH FERNANDA', 'AGUDELO SALAZAR', 19, 4, '', 'PLANTA 1'),
(1094887280, NULL, NULL, 'CESAR AUGUSTO', 'CORTES BUITRAGO', 29, 4, '', 'ZF'),
(1094890781, NULL, NULL, 'JONATHAN ', 'MADRID CUERVO', 29, 4, '', 'ZF'),
(1094891138, NULL, NULL, 'CRISTIAN CAMILO', 'PIEDRAHITA SILVA', 29, 4, '', 'ZF'),
(1094896576, NULL, NULL, 'KAROL VIVIANA', 'VARGAS RAYO', 28, 4, '', 'ZF'),
(1094896937, NULL, NULL, 'JONATAN ', 'CEBALLOS UPEGUI', 29, 4, '', 'ZF'),
(1094905969, NULL, NULL, 'ALEJANDRO ', 'CASTANO PALACIO', 29, 4, '', 'ZF'),
(1094911941, NULL, NULL, 'YONATAN ', 'RAMIREZ CARDONA', 29, 4, '', 'ZF'),
(1094913814, NULL, NULL, 'JHON FREDY', 'LARRADONDO YEPES', 29, 4, '', 'ZF'),
(1094917374, NULL, NULL, 'MARIO ', 'MORALES MOLINA', 29, 4, '', 'ZF'),
(1094918325, NULL, NULL, 'DIANA MARCELA', 'CASTANO DE LOS RIOS', 28, 4, '', 'ZF'),
(1094930765, NULL, NULL, 'JORGE ANTONIO', 'VANEGAS REYES', 29, 4, '', 'ZF'),
(1094937402, NULL, NULL, 'DAVID ', 'MONJE PACHECO', 28, 4, '', 'ZF'),
(1094937521, '1094937521', '123', 'JESSICA JOHANA', 'ARENAS GARZON', 19, 1, 'jessicajarenas@magnetron.com.co', 'ZF'),
(1094943973, NULL, NULL, 'DANIELA ', 'GARCIA PUENTES', 29, 4, '', 'ZF');
INSERT INTO `usuario` (`idusuario`, `usuario_usuario`, `usuario_clave`, `usuario_nombre`, `usuario_apellido`, `seccion_usuario_id`, `tipo_usuario_id`, `email`, `usuario_planta`) VALUES
(1094952690, NULL, NULL, 'JENIFFER ', 'MATALLANA CARVAJAL', 28, 4, '', 'ZF'),
(1096032770, NULL, NULL, 'MIGUEL ', 'ASPRILLA VALENCIA', 29, 4, '', 'ZF'),
(1096033200, NULL, NULL, 'JHON ALEXANDER', 'VANEGAS CARO', 29, 4, '', 'ZF'),
(1096033303, NULL, NULL, 'MAYERLI ', 'ZAMORA FLOREZ', 28, 4, '', 'ZF'),
(1096035986, NULL, NULL, 'OSCAR DARIO', 'MAZO RUEDA', 29, 4, '', 'ZF'),
(1096036039, NULL, NULL, 'LEISON STIVEN', 'ARANGO CALVO', 29, 4, '', 'ZF'),
(1096037062, NULL, NULL, 'DANIEL JESUS', 'CARDONA CASTANO', 29, 4, '', 'ZF'),
(1099206650, NULL, NULL, 'EINER MAURICIO', 'LOPEZ MORENO', 29, 4, '', 'ZF'),
(1107046924, NULL, NULL, 'HARRY WILSON', 'GIRALDO URUENA', 29, 4, '', 'ZF'),
(1112760651, NULL, NULL, 'JORGE WILLIAM', 'GOMEZ LANCHEROS', 12, 4, '', 'PLANTA 1'),
(1112762402, NULL, NULL, 'WILLONGER ', 'NARANJO GUTIERREZ', 12, 4, '', 'PLANTA 1'),
(1112762941, NULL, NULL, 'CARLOS ALBERTO', 'LOPEZ PIEDRAHITA', 17, 4, '', 'PLANTA 1'),
(1112763444, NULL, NULL, 'FRANCISCO JAVIER', 'LLANO MOSQUERA', 12, 4, '', 'PLANTA 1'),
(1112764195, NULL, NULL, 'CARLOS ANDRES', 'LONDONO VILLA', 21, 4, '', 'PLANTA 1'),
(1112765801, NULL, NULL, 'FERNANDO ', 'RODRIGUEZ PENILLA', 8, 4, '', 'PLANTA 1'),
(1112769676, NULL, NULL, 'DANIEL ', 'BENITEZ SABOGAL', 10, 4, '', 'PLANTA 1'),
(1112770529, NULL, NULL, 'CARLOS ALBERTO', 'MURILLO RESTREPO', 14, 4, '', 'PLANTA 2'),
(1112774977, NULL, NULL, 'ROBINSON ', 'ESTRADA ALVAREZ', 3, 4, '', 'PLANTA 1'),
(1112775207, NULL, NULL, 'FATIMA LORENA', 'HERRERA POSADA', 28, 4, '', 'ZF'),
(1112775301, NULL, NULL, 'LEIDY VANESSA', 'PARRA CASTRO', 1, 4, '', 'PLANTA 1'),
(1112776123, NULL, NULL, 'HAIVER ', 'RADA SALAZAR', 10, 4, '', 'PLANTA 2'),
(1112776728, NULL, NULL, 'HARNOLD GERMAN', 'RODAS MURCIA', 3, 4, '', 'PLANTA 1'),
(1112778476, NULL, NULL, 'LEONARDO ', 'ABADIA CASTRO', 22, 4, '', 'PLANTA 1'),
(1112783250, NULL, NULL, 'GERMAN ', 'ECHEVERRY MARIN', 3, 4, '', 'PLANTA 1'),
(1112784613, NULL, NULL, 'EDIEN JARLINTON', 'CORREA LONDONO', 20, 4, '', 'PLANTA 2'),
(1112786161, NULL, NULL, 'JUAN JOSE', 'JIMENEZ BERMUDEZ', 12, 4, '', 'PLANTA 1'),
(1114398516, NULL, NULL, 'JULIAN ANDRES', 'VELEZ MARIN', 3, 4, '', 'PLANTA 1'),
(1117884771, NULL, NULL, 'DIEGO ', 'PABON AROS', 11, 4, '', 'PLANTA 1'),
(1118236274, NULL, NULL, 'SAMUEL ALEJANDRO', 'RAMIREZ LOPEZ', 5, 4, '', 'PLANTA 1'),
(1118298635, NULL, NULL, 'JHON JAIRO', 'MUNOZ ALVAREZ', 2, 4, '', 'PLANTA 2'),
(1118308112, NULL, NULL, 'KEVIN ', 'RODRIGUEZ SERNA', 21, 4, '', 'PLANTA 1'),
(1125618995, NULL, NULL, 'ELKIN DARIO', 'SIERRA OSORIO', 8, 4, '', 'PLANTA 1'),
(1127802950, NULL, NULL, 'JORGE MARIO', 'HERNANDEZ CARDENAS', 12, 4, '', 'PLANTA 1'),
(1128226558, NULL, NULL, 'JEAN FREDERICK', 'ALCANTARA ORREGO', 11, 4, '', 'PLANTA 1'),
(1129575045, NULL, NULL, 'SANDRA ', 'PICON CRESPO', 27, 4, '', 'PLANTA 1'),
(1144059790, NULL, NULL, 'LORENA ', 'WAGNER VARELA', 1, 4, '', 'PLANTA 1'),
(1151954200, NULL, NULL, 'MARLON ALBEIRO', 'RODRIGUEZ SEPULVEDA', 24, 4, '', 'PLANTA 2'),
(1225088671, NULL, NULL, 'DANIEL ', 'MONTOYA ORTIZ', 19, 4, '', 'PLANTA 1'),
(98042369381, NULL, NULL, 'ANDERSON JULIAN', 'LONDONO LOPEZ', 29, 4, '', 'ZF'),
(98061164588, NULL, NULL, 'DIEGO ALEJANDRO', 'JIMENEZ AGUDELO', 19, 4, '', 'PLANTA 1'),
(99011110513, NULL, NULL, 'VALENTINA ', 'BEDOYA MONTOYA', 19, 4, '', 'PLANTA 1'),
(99100303848, NULL, NULL, 'JHONNIER ', 'CLAVIJO SANTANA', 19, 4, '', 'PLANTA 1');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_mejoramiento_participante`
--
CREATE TABLE `v_mejoramiento_participante` (
`id_pre_mejoramiento` int(11)
,`pre_mejoramiento_titulo` varchar(45)
,`tipo_mejoramiento_id` int(11)
,`pre_mejoramiento_date` date
,`clase_mejoramiento_id` int(11)
,`seccion_usuario_id` int(11)
,`pre_mejoramiento_consecutivo` int(11)
,`tipo_aprobacion_id` int(10)
,`seccion_usuario_descripcion` varchar(45)
,`clase_mejoramiento_descripcion` varchar(45)
,`pre_mejoramiento_id` int(11)
,`usuario_id` bigint(30)
,`usuario_usuario` varchar(30)
,`usuario_clave` varchar(30)
,`usuario_nombre` varchar(45)
,`usuario_apellido` varchar(45)
,`tipo_usuario_id` int(30)
,`seccion_usuario_usuarioid` int(30)
,`idtipo_mejoramiento` int(11)
,`tipo_mejoramiento_descripcion` varchar(45)
,`idtipo_usuario` int(11)
,`tipo_usuario_descripcion` varchar(45)
,`idtipo_aprobacion` int(11)
,`tipo_aprobacion_descripcion` varchar(45)
,`idjefe_area` int(15)
,`jefe_usuario_id` int(15)
,`sugerencia_ahorro` tinyint(1)
,`sugerencia_fahorro` varchar(100)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_pre_post_estandar`
--
CREATE TABLE `v_pre_post_estandar` (
`pre_mejoramiento_id` int(11)
,`post_mejoramiento_despues` varchar(500)
,`post_mejoramiento_evidencia` varchar(100)
,`post_mejoramiento_date` date
,`estandarizacion_descripcion` varchar(500)
,`pre_mejoramiento_titulo` varchar(45)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_usuarios_jefe`
--
CREATE TABLE `v_usuarios_jefe` (
`idusuario` bigint(30)
,`usuario_usuario` varchar(30)
,`usuario_clave` varchar(30)
,`usuario_nombre` varchar(45)
,`usuario_apellido` varchar(45)
,`seccion_usuario_id` int(30)
,`tipo_usuario_id` int(30)
,`email` varchar(100)
,`idjefe_area` int(15)
,`usuario_id` int(15)
);

-- --------------------------------------------------------

--
-- Estructura para la vista `v_mejoramiento_participante`
--
DROP TABLE IF EXISTS `v_mejoramiento_participante`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_mejoramiento_participante`  AS  select `pre_mejoramiento`.`id_pre_mejoramiento` AS `id_pre_mejoramiento`,`pre_mejoramiento`.`pre_mejoramiento_titulo` AS `pre_mejoramiento_titulo`,`pre_mejoramiento`.`tipo_mejoramiento_id` AS `tipo_mejoramiento_id`,`pre_mejoramiento`.`pre_mejoramiento_date` AS `pre_mejoramiento_date`,`pre_mejoramiento`.`clase_mejoramiento_id` AS `clase_mejoramiento_id`,`pre_mejoramiento`.`seccion_usuario_id` AS `seccion_usuario_id`,`pre_mejoramiento`.`pre_mejoramiento_consecutivo` AS `pre_mejoramiento_consecutivo`,`pre_mejoramiento`.`tipo_aprobacion_id` AS `tipo_aprobacion_id`,`seccion_usuario`.`seccion_usuario_descripcion` AS `seccion_usuario_descripcion`,`clase_mejoramiento`.`clase_mejoramiento_descripcion` AS `clase_mejoramiento_descripcion`,`participante`.`pre_mejoramiento_id` AS `pre_mejoramiento_id`,`usuario`.`idusuario` AS `usuario_id`,`usuario`.`usuario_usuario` AS `usuario_usuario`,`usuario`.`usuario_clave` AS `usuario_clave`,`usuario`.`usuario_nombre` AS `usuario_nombre`,`usuario`.`usuario_apellido` AS `usuario_apellido`,`usuario`.`tipo_usuario_id` AS `tipo_usuario_id`,`usuario`.`seccion_usuario_id` AS `seccion_usuario_usuarioid`,`tipo_mejoramiento`.`idtipo_mejoramiento` AS `idtipo_mejoramiento`,`tipo_mejoramiento`.`tipo_mejoramiento_descripcion` AS `tipo_mejoramiento_descripcion`,`tipo_usuario`.`idtipo_usuario` AS `idtipo_usuario`,`tipo_usuario`.`tipo_usuario_descripcion` AS `tipo_usuario_descripcion`,`tipo_aprobacion`.`idtipo_aprobacion` AS `idtipo_aprobacion`,`tipo_aprobacion`.`tipo_aprobacion_descripcion` AS `tipo_aprobacion_descripcion`,`jefe_area`.`idjefe_area` AS `idjefe_area`,`jefe_area`.`usuario_id` AS `jefe_usuario_id`,`sugerencia`.`sugerencia_ahorro` AS `sugerencia_ahorro`,`sugerencia`.`sugerencia_fahorro` AS `sugerencia_fahorro` from (((((((((`pre_mejoramiento` join `sugerencia` on((`sugerencia`.`pre_mejoremiento_id` = `pre_mejoramiento`.`id_pre_mejoramiento`))) join `seccion_usuario` on((`pre_mejoramiento`.`seccion_usuario_id` = `seccion_usuario`.`idseccion_usuario`))) join `clase_mejoramiento` on((`pre_mejoramiento`.`clase_mejoramiento_id` = `clase_mejoramiento`.`idclase_mejoramiento`))) join `tipo_mejoramiento` on((`pre_mejoramiento`.`tipo_mejoramiento_id` = `tipo_mejoramiento`.`idtipo_mejoramiento`))) join `tipo_aprobacion` on((`pre_mejoramiento`.`tipo_aprobacion_id` = `tipo_aprobacion`.`idtipo_aprobacion`))) join `participante` on((`pre_mejoramiento`.`id_pre_mejoramiento` = `participante`.`pre_mejoramiento_id`))) join `usuario` on((`usuario`.`idusuario` = `participante`.`usuario_id`))) left join `jefe_area` on((`jefe_area`.`seccion_usuario_id` = `seccion_usuario`.`idseccion_usuario`))) join `tipo_usuario` on((`tipo_usuario`.`idtipo_usuario` = `usuario`.`tipo_usuario_id`))) where ((`usuario`.`idusuario` = `participante`.`usuario_id`) and (`pre_mejoramiento`.`id_pre_mejoramiento` = `participante`.`pre_mejoramiento_id`)) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_pre_post_estandar`
--
DROP TABLE IF EXISTS `v_pre_post_estandar`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pre_post_estandar`  AS  select `post_mejoramiento`.`pre_mejoramiento_id` AS `pre_mejoramiento_id`,`post_mejoramiento`.`post_mejoramiento_despues` AS `post_mejoramiento_despues`,`post_mejoramiento`.`post_mejoramiento_evidencia` AS `post_mejoramiento_evidencia`,`post_mejoramiento`.`post_mejoramiento_date` AS `post_mejoramiento_date`,`estandarizacion`.`estandarizacion_descripcion` AS `estandarizacion_descripcion`,`pre_mejoramiento`.`pre_mejoramiento_titulo` AS `pre_mejoramiento_titulo` from ((`post_mejoramiento` join `estandarizacion` on((`estandarizacion`.`idestandarizacion` = `post_mejoramiento`.`estandarizacion_id`))) join `pre_mejoramiento` on((`pre_mejoramiento`.`id_pre_mejoramiento` = `post_mejoramiento`.`pre_mejoramiento_id`))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_usuarios_jefe`
--
DROP TABLE IF EXISTS `v_usuarios_jefe`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_usuarios_jefe`  AS  select `usuario`.`idusuario` AS `idusuario`,`usuario`.`usuario_usuario` AS `usuario_usuario`,`usuario`.`usuario_clave` AS `usuario_clave`,`usuario`.`usuario_nombre` AS `usuario_nombre`,`usuario`.`usuario_apellido` AS `usuario_apellido`,`usuario`.`seccion_usuario_id` AS `seccion_usuario_id`,`usuario`.`tipo_usuario_id` AS `tipo_usuario_id`,`usuario`.`email` AS `email`,`jefe_area`.`idjefe_area` AS `idjefe_area`,`jefe_area`.`usuario_id` AS `usuario_id` from ((`usuario` left join `jefe_area` on((`jefe_area`.`usuario_id` = `usuario`.`idusuario`))) left join `seccion_usuario` on((`seccion_usuario`.`idseccion_usuario` = `jefe_area`.`seccion_usuario_id`))) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aprobacion`
--
ALTER TABLE `aprobacion`
  ADD PRIMARY KEY (`idaprobacion`);

--
-- Indices de la tabla `clase_mejoramiento`
--
ALTER TABLE `clase_mejoramiento`
  ADD PRIMARY KEY (`idclase_mejoramiento`);

--
-- Indices de la tabla `criterio_evaluacion`
--
ALTER TABLE `criterio_evaluacion`
  ADD PRIMARY KEY (`idcriterio_evaluacion`);

--
-- Indices de la tabla `estado_mejoramiento`
--
ALTER TABLE `estado_mejoramiento`
  ADD PRIMARY KEY (`idestado_mejoramiento`);

--
-- Indices de la tabla `estandarizacion`
--
ALTER TABLE `estandarizacion`
  ADD PRIMARY KEY (`idestandarizacion`);

--
-- Indices de la tabla `evaluacion`
--
ALTER TABLE `evaluacion`
  ADD PRIMARY KEY (`idevaluacion`),
  ADD UNIQUE KEY `pre_mejoramiento_id` (`pre_mejoramiento_id`),
  ADD UNIQUE KEY `novedad_id` (`novedad_id`),
  ADD UNIQUE KEY `novedad_id_2` (`novedad_id`);

--
-- Indices de la tabla `jefe_area`
--
ALTER TABLE `jefe_area`
  ADD PRIMARY KEY (`idjefe_area`),
  ADD UNIQUE KEY `seccion_usuario_id` (`seccion_usuario_id`);

--
-- Indices de la tabla `novedad`
--
ALTER TABLE `novedad`
  ADD PRIMARY KEY (`idnovedad`);

--
-- Indices de la tabla `participante`
--
ALTER TABLE `participante`
  ADD PRIMARY KEY (`idparticipante`);

--
-- Indices de la tabla `post_mejoramiento`
--
ALTER TABLE `post_mejoramiento`
  ADD PRIMARY KEY (`idpost_mejoramiento`),
  ADD UNIQUE KEY `pre_mejoramiento_id` (`pre_mejoramiento_id`);

--
-- Indices de la tabla `pre_mejoramiento`
--
ALTER TABLE `pre_mejoramiento`
  ADD PRIMARY KEY (`id_pre_mejoramiento`),
  ADD UNIQUE KEY `pre_mejoramiento_consecutivo` (`pre_mejoramiento_consecutivo`);

--
-- Indices de la tabla `seccion_usuario`
--
ALTER TABLE `seccion_usuario`
  ADD PRIMARY KEY (`idseccion_usuario`);

--
-- Indices de la tabla `sugerencia`
--
ALTER TABLE `sugerencia`
  ADD PRIMARY KEY (`idsugerencia`),
  ADD UNIQUE KEY `pre_mejoremiento_id` (`pre_mejoremiento_id`);

--
-- Indices de la tabla `tipo_aprobacion`
--
ALTER TABLE `tipo_aprobacion`
  ADD PRIMARY KEY (`idtipo_aprobacion`);

--
-- Indices de la tabla `tipo_mejoramiento`
--
ALTER TABLE `tipo_mejoramiento`
  ADD PRIMARY KEY (`idtipo_mejoramiento`);

--
-- Indices de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  ADD PRIMARY KEY (`idtipo_usuario`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`),
  ADD UNIQUE KEY `idusuario` (`idusuario`),
  ADD UNIQUE KEY `usuario_usuario` (`usuario_usuario`),
  ADD UNIQUE KEY `usuario_usuario_2` (`usuario_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aprobacion`
--
ALTER TABLE `aprobacion`
  MODIFY `idaprobacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT de la tabla `clase_mejoramiento`
--
ALTER TABLE `clase_mejoramiento`
  MODIFY `idclase_mejoramiento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `criterio_evaluacion`
--
ALTER TABLE `criterio_evaluacion`
  MODIFY `idcriterio_evaluacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `estado_mejoramiento`
--
ALTER TABLE `estado_mejoramiento`
  MODIFY `idestado_mejoramiento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `estandarizacion`
--
ALTER TABLE `estandarizacion`
  MODIFY `idestandarizacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `evaluacion`
--
ALTER TABLE `evaluacion`
  MODIFY `idevaluacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `jefe_area`
--
ALTER TABLE `jefe_area`
  MODIFY `idjefe_area` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT de la tabla `novedad`
--
ALTER TABLE `novedad`
  MODIFY `idnovedad` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT de la tabla `participante`
--
ALTER TABLE `participante`
  MODIFY `idparticipante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT de la tabla `post_mejoramiento`
--
ALTER TABLE `post_mejoramiento`
  MODIFY `idpost_mejoramiento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `pre_mejoramiento`
--
ALTER TABLE `pre_mejoramiento`
  MODIFY `id_pre_mejoramiento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT de la tabla `seccion_usuario`
--
ALTER TABLE `seccion_usuario`
  MODIFY `idseccion_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT de la tabla `sugerencia`
--
ALTER TABLE `sugerencia`
  MODIFY `idsugerencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT de la tabla `tipo_aprobacion`
--
ALTER TABLE `tipo_aprobacion`
  MODIFY `idtipo_aprobacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `tipo_mejoramiento`
--
ALTER TABLE `tipo_mejoramiento`
  MODIFY `idtipo_mejoramiento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  MODIFY `idtipo_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

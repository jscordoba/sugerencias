<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_proponente extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function m_query($consulta){
		//echo "$consulta".$consulta;

		$exec=$this->db->query($consulta);

		if ($exec) {
			return $exec;
		}else{
			return false;
		}
	}

	public function m_numrows($id_user,$clase_mejoramiento_id,$tipo_aprobacion){
		//echo "Tipo: ".$tipo;
		//echo "<br>Usuario: ".$id_user;
		//echo "<br>Usuario: ".$tipo_aprobacion;

		if ($clase_mejoramiento_id==0) {//SE USA CLASE MEJORAMIENTO 0 PARA ACCEDER ALOS VALORES DE JEFE
			$query="SELECT COUNT(*) AS num
					FROM pre_mejoramiento
					JOIN seccion_usuario on seccion_usuario.idseccion_usuario=pre_mejoramiento.seccion_usuario_id
					JOIN jefe_area on jefe_area.seccion_usuario_id=pre_mejoramiento.seccion_usuario_id
					WHERE usuario_id=$id_user 
					AND tipo_aprobacion_id=$tipo_aprobacion";
					
				//echo $query;
		}else{
			if ($tipo_aprobacion == 2) {
				$tipo_aprobacion="tipo_aprobacion_id BETWEEN 1 AND 5";
			}else{
				$tipo_aprobacion="tipo_aprobacion_id=$tipo_aprobacion";
			}

			$query="SELECT COUNT(*) AS num
					FROM pre_mejoramiento
					JOIN seccion_usuario on seccion_usuario.idseccion_usuario=pre_mejoramiento.seccion_usuario_id
					JOIN participante on participante.pre_mejoramiento_id=pre_mejoramiento.id_pre_mejoramiento
					WHERE usuario_id=$id_user 
					AND $tipo_aprobacion
					AND clase_mejoramiento_id=$clase_mejoramiento_id
				";
				//echo $query;
		}
		//echo $query;

		$result=$this->db->query($query);
	
		if ($result) {
			if ($result->num_rows()>0) {
				$row=$result->row();
				return $row->num;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}

	public function registrados($id_usuario,$clase_mejoramiento_id,$tipo_aprobacion){

		if ($tipo_aprobacion == 2) {
			$tipo_aprobacion="v_mejoramiento_participante.tipo_aprobacion_id BETWEEN 1 AND 5";
			$join="";
		}elseif($tipo_aprobacion==3){
			$tipo_aprobacion="1"; // CONSULTA PARA PLANTA
			$join="";
		}elseif ($tipo_aprobacion==7) {
			$tipo_aprobacion="v_mejoramiento_participante.tipo_aprobacion_id=$tipo_aprobacion";
			$join="JOIN aprobacion ON aprobacion.pre_mejoramiento_id=v_mejoramiento_participante.id_pre_mejoramiento
					LEFT JOIN evaluacion ON evaluacion.pre_mejoramiento_id=v_mejoramiento_participante.id_pre_mejoramiento";
		}else{
			$tipo_aprobacion="v_mejoramiento_participante.tipo_aprobacion_id=$tipo_aprobacion";
			$join="";
		}

		if ($id_usuario==1 || $id_usuario==2) {
			$usuario="1"; 
		}else{
			$usuario="v_mejoramiento_participante.usuario_id='$id_usuario'";
		}

		if ($clase_mejoramiento_id==0) {
			$query="SELECT *
				FROM v_mejoramiento_participante 
				WHERE jefe_usuario_id='$id_usuario' 
				AND $tipo_aprobacion
				GROUP BY id_pre_mejoramiento
				ORDER BY pre_mejoramiento_date DESC";
		}else{
			$query="SELECT *
				FROM v_mejoramiento_participante 
				$join
				WHERE $usuario
				AND v_mejoramiento_participante.clase_mejoramiento_id=$clase_mejoramiento_id
				AND $tipo_aprobacion
				GROUP BY id_pre_mejoramiento
				ORDER BY pre_mejoramiento_date DESC";

			//echo $query;
		}

		//echo $query;
		
		$result=$this->db->query($query);

		if ($result) {
			if ($result->num_rows()>0) {
				return $result;
			}else{
				return null;
			}
		}else{
			echo "Error al realizar la busqueda.";
		}
	}

	public function evaluacion($id_usuario,$clase_mejoramiento_id,$tipo_aprobacion){

		if ($tipo_aprobacion == 2) {
			$tipo_aprobacion="v_mejoramiento_participante.tipo_aprobacion_id BETWEEN 1 AND 5";
			$join="";
		}elseif($tipo_aprobacion==3){
			$tipo_aprobacion="1"; // CONSULTA PARA PLANTA
			$join="";
		}elseif ($tipo_aprobacion==7) {
			$tipo_aprobacion="v_mejoramiento_participante.tipo_aprobacion_id=$tipo_aprobacion";
			$join="JOIN aprobacion ON aprobacion.pre_mejoramiento_id=v_mejoramiento_participante.id_pre_mejoramiento
				JOIN evaluacion ON evaluacion.pre_mejoramiento_id=v_mejoramiento_participante.id_pre_mejoramiento";
		}else{
			$tipo_aprobacion="v_mejoramiento_participante.tipo_aprobacion_id=$tipo_aprobacion";
			$join="";
		}

		// if ($id_usuario==1 || $id_usuario==2) {
		// 	$usuario="1"; 
		// }else{
		// 	$usuario="v_mejoramiento_participante.usuario_id='$id_usuario'";
		// }

		if ($clase_mejoramiento_id==0) {
			$query="SELECT *
				FROM v_mejoramiento_participante 
				WHERE jefe_usuario_id='$id_usuario' 
				AND $tipo_aprobacion
				ORDER BY pre_mejoramiento_date DESC";
		}else{
			$query="SELECT *
				FROM v_mejoramiento_participante 
				$join
				WHERE v_mejoramiento_participante.clase_mejoramiento_id=$clase_mejoramiento_id
				AND $tipo_aprobacion
				ORDER BY pre_mejoramiento_date DESC";
		}

		//echo $query;
		
		$result=$this->db->query($query);

		if ($result) {
			if ($result->num_rows()>0) {
				return $result;
			}else{
				return null;
			}
		}else{
			echo "Error al realizar la busqueda.";
		}
	}


	public function ver_detalle($id_mejoramiento,$id_fase){
		if ($id_fase==2) {
			try {
				$result=$this->db->query("SELECT *
									FROM v_pre_post_estandar vp
									JOIN v_mejoramiento_participante vm ON vm.id_pre_mejoramiento=vp.pre_mejoramiento_id
									WHERE vp.pre_mejoramiento_id=$id_mejoramiento");
				return $result->row();
			} catch (Exception $e) {
				return NULL;
			}
		}elseif($id_fase==1){
			$this->db->select('*');
			$this->db->from('v_mejoramiento_participante');
			$this->db->join('sugerencia', 'sugerencia.pre_mejoremiento_id = v_mejoramiento_participante.id_pre_mejoramiento');
			$this->db->join('novedad ', 'novedad.pre_mejoramiento_id=v_mejoramiento_participante.id_pre_mejoramiento','left');
			$this->db->where('id_pre_mejoramiento', $id_mejoramiento);

			
			$result=$this->db->get();

			if ($result) {
				return $result->row();
			}else{
				return null;
			}
			//echo "detalle";
		}
	}

	public function ver_participantes($id_mejoramiento){
		try {
			$result=$this->db->query("SELECT participante.*,usuario.* FROM `pre_mejoramiento`
									JOIN participante ON participante.pre_mejoramiento_id=pre_mejoramiento.id_pre_mejoramiento
									JOIN usuario ON usuario.idusuario=participante.usuario_id
									WHERE pre_mejoramiento.id_pre_mejoramiento=$id_mejoramiento");
			return $result;
		} catch (Exception $e) {
			return NULL;
		}
	}

	public function ver_novedad($id_mejoramiento,$aprobacion_id){
		
		try {
			$this->db->where('tipo_aprobacion_id', $aprobacion_id);
			$this->db->where('pre_mejoramiento_id', $id_mejoramiento);
			$query=$this->db->get('novedad');

			//print_r($query);

			return $query->row();

		} catch (Exception $e) {
			//echo "Error al recuperar Novedad.";
			return NULL;
		}
		
	}

	public function reg_f2($id_pre_mejoramiento,$despues_mejoramiento,$evidencias,$estandarizacion,$fecha,$usuario_evaluador_id){
		try {
			$query="INSERT INTO estandarizacion
					VALUES (NULL,'$estandarizacion')";
			
			$result=$this->db->query($query);

			if ($result) {
				$estandarizacion_id=$this->db->insert_id();

				$query2="INSERT INTO post_mejoramiento
						VALUES (NULL,$id_pre_mejoramiento,'$despues_mejoramiento','$evidencias',$estandarizacion_id,'$fecha')";

				$result2=$this->db->query($query2);

				if ($result2) {
					//echo "OK post MEJORAMIENTO.";

					$query3="UPDATE aprobacion
							SET tipo_aprobacion_id=4, 
								aprobacion_date='$fecha',
								usuario_evaluador_id=$usuario_evaluador_id
							WHERE pre_mejoramiento_id=$id_pre_mejoramiento";
					$result3=$this->db->query($query3);

					$query4="UPDATE pre_mejoramiento 
							SET tipo_aprobacion_id=4
							WHERE id_pre_mejoramiento=$id_pre_mejoramiento";
					$result4=$this->db->query($query4);

					if ($result3) {
						if ($result4) {
							//echo "estado actualizado.";
							return "ok";
						}else{
							echo "Error al actualizar pre_mejoramiento.";
						}						
					}else{
						//echo "Error al actualizar el estado.";
						return NULL;
					}
				}else{
					//echo "Error al registrar el post mejoramiento.";
					return NULL;
				}
			}else{
				return NULL;
			}
		} catch (Exception $e) {
			//echo "Error al registrar la estandarizacion de la mejora.";
			return NULL;
		}
	}

	public function eliminar($id_pre_mejoramiento){
		$this->db->where('id_pre_mejoramiento', $id_pre_mejoramiento);
		$delete_pre_mejoramiento=$this->db->delete('pre_mejoramiento');

		if ($delete_pre_mejoramiento) {
			try {
				$this->db->where('pre_mejoremiento_id', $id_pre_mejoramiento);
				$delete_participante=$this->db->delete('sugerencia');

				$this->db->where('pre_mejoramiento_id', $id_pre_mejoramiento);
				$result=$this->db->get('post_mejoramiento');
				$object=$result->result();
				//print_r($estandar);
				$estandar_id=$object[0];
				//print_r($estand);
				//echo $estand->idpost_mejoramiento;
				

				$this->db->where('idestandarizacion', $estandar_id->estandarizacion_id);
				$delete_estandar=$this->db->delete('estandarizacion');

				if ($delete_estandar) {
					$this->db->where('pre_mejoramiento_id', $id_pre_mejoramiento);
					$delete_participante=$this->db->delete('post_mejoramiento');
				}

				$this->db->where('pre_mejoramiento_id', $id_pre_mejoramiento);
				$delete_participante=$this->db->delete('participante');

				$this->db->where('pre_mejoramiento_id', $id_pre_mejoramiento);
				$delete_participante=$this->db->delete('aprobacion');

				$this->db->where('pre_mejoramiento_id', $id_pre_mejoramiento);
				$delete_participante=$this->db->delete('novedad');
			} catch (Exception $e) {
				echo "Error al realizar eliminación del arbol de registro.";
			}
		}else{
			echo "Error al eliminar pre_mejoramiento.";
		}
	}



	public function obt_funcionarios($seccion_id){
		try {
			//$this->db->where('seccion_usuario_id', $seccion_id);
			$query=$this->db->query("SELECT * 
									FROM usuario 
									WHERE seccion_usuario_id=$seccion_id 
									AND tipo_usuario_id NOT IN(2,3)");

			if ($query) {
				return $query;
			}else{
				return null;
			}
		} catch (Exception $e) {
			return null;
		}

	}


}

/* End of file m_proponente.php */
/* Location: ./application/models/m_proponente.php */
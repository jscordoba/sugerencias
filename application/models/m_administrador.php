<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_administrador extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function m_numrows($tipo_usuario,$tipo_busqueda,$aprobacion){
		//echo "Tipo: ".$tipo;
		//echo "<br>Usuario: ".$id_user;
		//echo "<br>Usuario: ".$tipo_aprobacion;

		if ($tipo_busqueda ==1) {
			$query="SELECT COUNT(*) num FROM `usuario` WHERE tipo_usuario_id=$tipo_usuario";
		}elseif ($tipo_busqueda==2) {
			if ($aprobacion == 2) {
				$aprobacion="tipo_aprobacion_id BETWEEN 1 AND 5";
			}else{
				$aprobacion="tipo_aprobacion_id=$aprobacion";
			}

			$query="SELECT COUNT(*) num FROM `pre_mejoramiento` WHERE $aprobacion";
		}elseif ($tipo_busqueda==3) { // busqueda de usuarios jefes
			$query="SELECT COUNT(*) num 
					FROM `usuario` 
					JOIN jefe_area ON jefe_area.usuario_id=usuario.idusuario
					WHERE tipo_usuario_id=$tipo_usuario";
		}
		
		//echo $query;
		$result=$this->db->query($query);
	
		if ($result) {
			if ($result->num_rows()>0) {
				$row=$result->row();
				return $row->num;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}

	public function usuarios($tipo){
		//$result=$this->db->get('usuario');
		if ($tipo==3) {//usuarios solo jefes
			$add=" JOIN jefe_area ON jefe_area.usuario_id=usuario.idusuario";
			$tipo=1;
			$add_where=" ";
		}elseif ($tipo==1 || $tipo==2) {//proponentes y evaluadores
			$add=" LEFT JOIN jefe_area ON jefe_area.usuario_id=usuario.idusuario";
			$add_where=" AND jefe_area.usuario_id IS NULL";
		}else{
			$add=" LEFT JOIN jefe_area ON jefe_area.usuario_id=usuario.idusuario";
			$add_where=" AND jefe_area.usuario_id IS NULL";
		}

		$query="SELECT * FROM usuario 
			 	JOIN seccion_usuario ON seccion_usuario.idseccion_usuario=usuario.seccion_usuario_id
			 	$add
				WHERE tipo_usuario_id=$tipo $add_where
				GROUP BY usuario.idusuario";

		//echo "$query";

		$result=$this->db->query($query);

		if ($result) {
			return $result;
		}else{
			return 0;
		}
	}

	public function search($c_buscar,$tipo_usuario){
		if ($tipo_usuario==5) {//funcion para buscar solo jefes
			$tipo_usuario=1;
			$add="JOIN jefe_area ON jefe_area.usuario_id=usuario.idusuario";
			$add_where=" ";
		}else{
			$add="LEFT JOIN jefe_area ON jefe_area.usuario_id=usuario.idusuario";
			$add_where=" AND jefe_area.usuario_id IS NULL";
		}

		if ($c_buscar=="") {
			$query="SELECT * 
					FROM usuario 
					JOIN seccion_usuario ON seccion_usuario.idseccion_usuario=usuario.seccion_usuario_id
					$add
					WHERE usuario.tipo_usuario_id=$tipo_usuario $add_where
					GROUP BY usuario.idusuario";
		}else{
			$query="SELECT * 
					FROM usuario 
					JOIN seccion_usuario ON seccion_usuario.idseccion_usuario=usuario.seccion_usuario_id
					$add
					WHERE usuario.tipo_usuario_id=$tipo_usuario $add_where
					AND (
							usuario.idusuario LIKE ('%$c_buscar%') 
							OR usuario.usuario_nombre LIKE ('%$c_buscar%')
						)
					GROUP BY usuario.idusuario";
		}
		//echo "$query";

		$result=$this->db->query($query);

		if ($result) {
			return $result;
		}else{
			return 0;
		}
	}

	public function mejoras($tipo){
		if ($tipo == 2) {
			$tipo="tipo_aprobacion_id BETWEEN 1 AND 5";
		}else{
			$tipo="tipo_aprobacion_id=$tipo";
		}

		$result=$this->db->query("SELECT *
								FROM `v_mejoramiento_participante` 
								WHERE $tipo 
								GROUP BY id_pre_mejoramiento");

		if ($result) {
			return $result;
		}else{
			return 0;
		}
	}

	public function ver_detalle($id_mejoramiento){
		$query="SELECT * 
				FROM sugerencia
				JOIN participante ON participante.pre_mejoramiento_id=sugerencia.pre_mejoremiento_id
				JOIN usuario ON usuario.idusuario=participante.usuario_id
				JOIN pre_mejoramiento ON pre_mejoramiento.id_pre_mejoramiento=sugerencia.pre_mejoremiento_id
				LEFT JOIN post_mejoramiento ON post_mejoramiento.pre_mejoramiento_id=sugerencia.pre_mejoremiento_id
				LEFT JOIN estandarizacion ON estandarizacion.idestandarizacion=post_mejoramiento.estandarizacion_id
				LEFT JOIN novedad ON novedad.pre_mejoramiento_id=sugerencia.pre_mejoremiento_id
				JOIN aprobacion ON aprobacion.pre_mejoramiento_id=sugerencia.pre_mejoremiento_id
				JOIN tipo_aprobacion ON tipo_aprobacion.idtipo_aprobacion=aprobacion.tipo_aprobacion_id
				JOIN tipo_mejoramiento ON tipo_mejoramiento.idtipo_mejoramiento=pre_mejoramiento.tipo_mejoramiento_id
				JOIN seccion_usuario ON seccion_usuario.idseccion_usuario=pre_mejoramiento.seccion_usuario_id
				WHERE sugerencia.pre_mejoremiento_id=$id_mejoramiento";

		//echo "$query";

		$result=$this->db->query($query);

		if ($result) {
			return $result->row();
		}else{
			return null;
		}
		//echo "detalle";
	}

	public function ver_seccion(){
		$query="SELECT * FROM seccion_usuario";

		//echo "$query";

		try {
			$exec=$this->db->query($query);

			return $exec;
		} catch (Exception $e) {
			return null;
		}
	}


	public function reg_user($id_usuario,$nombre_usuario,$apellido_usuario,$clave_usuario,$seccion_usuario,$tipo_usuario,$email_usuario,$planta_usuario){
		$query="INSERT INTO usuario VALUES($id_usuario,'$id_usuario','$clave_usuario','$nombre_usuario','$apellido_usuario',$seccion_usuario,$tipo_usuario,'$email_usuario','$planta_usuario')";

		//echo "$query";

		try {
			if ($this->db->query($query)) {
				return "ok";
			}else{
				echo "Error!!.<br>Verifique los datos e intentelo nuevamente.";
			}

		} catch (Exception $e) {
			return null;
		}
	}

	public function del_us($id_usuario){
		try {
			$this->db->query("DELETE FROM usuario WHERE idusuario=$id_usuario");

			return "ok";
		} catch (Exception $e) {
			return null;
		}
	}

	public function user_edi($id_usuario){
		$query="SELECT * 
				FROM usuario 
				JOIN seccion_usuario ON seccion_usuario.idseccion_usuario=usuario.seccion_usuario_id
				JOIN tipo_usuario ON tipo_usuario.idtipo_usuario=usuario.tipo_usuario_id
				WHERE usuario.idusuario=$id_usuario";

		//echo $query;

		try {
			$exec=$this->db->query($query);

			return $exec->row();
		} catch (Exception $e) {
			return null;
		}
	}

	public function upd_user($id_user,$nombre_usuario,$apellido_usuario,$clave_usuario,$seccion_usuario,$tipo_usuario,$email_usuario,$planta_usuario){
		if ($tipo_usuario==4) {
			$clave_usuario="";
			$email_usuario="";
		}

		$query="UPDATE usuario SET  usuario_clave='$clave_usuario',
									usuario_nombre='$nombre_usuario',
									usuario_apellido='$apellido_usuario',
									seccion_usuario_id=$seccion_usuario,
									tipo_usuario_id=$tipo_usuario,
									email='$email_usuario',
									usuario_planta='$planta_usuario'
								WHERE idusuario=$id_user";

		//echo "$query";

		try {
			if ($this->db->query($query)) {
				return "ok";
			}else{
				echo "Error!!.<br>Verifique los datos e intentelo nuevamente.";
			}

		} catch (Exception $e) {
			return null;
		}
	}



}

/* End of file m_administrador.php */
/* Location: ./application/views/administrador/m_administrador.php */
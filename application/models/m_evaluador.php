<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_evaluador extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function m_query($consulta){
		//echo "$consulta".$consulta;

		$exec=$this->db->query($consulta);

		if ($exec) {
			return $exec;
		}else{
			return false;
		}
	}

	public function m_numrows($id_user, $aprobacion, $clase_mejoramiento){
		//echo "Tipo: ".$tipo;
		//echo "<br>Usuario: ".$id_user;
		//echo "<br>Usuario: ".$tipo_aprobacion;

		if ($aprobacion == 3) {
			$aprobacion="aprobacion.tipo_aprobacion_id BETWEEN 1 AND 5";

			$query="SELECT COUNT(*) as num
						FROM aprobacion JOIN pre_mejoramiento ON aprobacion.pre_mejoramiento_id=pre_mejoramiento.id_pre_mejoramiento
						WHERE $aprobacion
						AND pre_mejoramiento.clase_mejoramiento_id=$clase_mejoramiento ";
		}elseif ($aprobacion == 4) {//SUGERENCIAS FINALIZADAS PARA EVALUADOR ENTRE 7 Y 9
			$aprobacion="aprobacion.tipo_aprobacion_id = 7 || aprobacion.tipo_aprobacion_id = 9";

			$query="SELECT COUNT(pre_mejoramiento_id) as num
						FROM aprobacion 
						JOIN pre_mejoramiento ON aprobacion.pre_mejoramiento_id=pre_mejoramiento.id_pre_mejoramiento
						WHERE $aprobacion
						AND pre_mejoramiento.clase_mejoramiento_id=$clase_mejoramiento";
		}else{
			$aprobacion="aprobacion.tipo_aprobacion_id=$aprobacion ";

			$query="SELECT COUNT(pre_mejoramiento_id) as num
						FROM aprobacion 
						JOIN pre_mejoramiento ON aprobacion.pre_mejoramiento_id=pre_mejoramiento.id_pre_mejoramiento
						WHERE $aprobacion
						
						AND pre_mejoramiento.clase_mejoramiento_id=$clase_mejoramiento";
		}

		$result=$this->db->query($query);
	
		if ($result) {
			if ($result->num_rows()>0) {
				$row=$result->row();
				return $row->num;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}


	public function pendientes($id_usuario,$clase_mejoramiento,$aprobacion){

		if ($aprobacion == 4) {
			$aprobacion="aprobacion.tipo_aprobacion_id = 7 OR aprobacion.tipo_aprobacion_id = 9";

			//MOSTRAR TODAS LAS SOLICITUDES REGISTRADAS EN SUS ESTADOS PENDIENTES O EN TRANSITO PARA EL PERFIL DE EVALUADOR
			$query="SELECT *
				FROM aprobacion 
				JOIN pre_mejoramiento ON aprobacion.pre_mejoramiento_id=pre_mejoramiento.id_pre_mejoramiento
				JOIN tipo_mejoramiento ON tipo_mejoramiento.idtipo_mejoramiento=pre_mejoramiento.tipo_mejoramiento_id
				JOIN usuario ON usuario.idusuario=aprobacion.usuario_id
				JOIN seccion_usuario ON seccion_usuario.idseccion_usuario=pre_mejoramiento.seccion_usuario_id
				JOIN tipo_aprobacion ON pre_mejoramiento.tipo_aprobacion_id=tipo_aprobacion.idtipo_aprobacion
				JOIN clase_mejoramiento ON clase_mejoramiento.idclase_mejoramiento=pre_mejoramiento.clase_mejoramiento_id
				LEFT JOIN evaluacion ON evaluacion.pre_mejoramiento_id=pre_mejoramiento.id_pre_mejoramiento
				WHERE $aprobacion
				AND pre_mejoramiento.clase_mejoramiento_id=$clase_mejoramiento
				ORDER BY pre_mejoramiento.id_pre_mejoramiento DESC";

				//echo $query;

		}elseif ($aprobacion == 3) {
			$aprobacion="aprobacion.tipo_aprobacion_id BETWEEN 1 AND 5";

			//MOSTRAR TODAS LAS SOLICITUDES REGISTRADAS EN SUS ESTADOS PENDIENTES O EN TRANSITO PARA EL PERFIL DE EVALUADOR
			$query="SELECT *
				FROM aprobacion 
				JOIN pre_mejoramiento ON aprobacion.pre_mejoramiento_id=pre_mejoramiento.id_pre_mejoramiento
				JOIN tipo_mejoramiento ON tipo_mejoramiento.idtipo_mejoramiento=pre_mejoramiento.tipo_mejoramiento_id
				JOIN usuario ON usuario.idusuario=aprobacion.usuario_id
				JOIN seccion_usuario ON seccion_usuario.idseccion_usuario=pre_mejoramiento.seccion_usuario_id
				JOIN tipo_aprobacion ON pre_mejoramiento.tipo_aprobacion_id=tipo_aprobacion.idtipo_aprobacion
				LEFT JOIN evaluacion ON evaluacion.pre_mejoramiento_id=pre_mejoramiento.id_pre_mejoramiento
				WHERE $aprobacion
				AND pre_mejoramiento.clase_mejoramiento_id=$clase_mejoramiento
				ORDER BY pre_mejoramiento.id_pre_mejoramiento DESC";

			//echo "$query";
		}else{
			$aprobacion="aprobacion.tipo_aprobacion_id=$aprobacion";

			//MOSTRAR TODAS LAS SOLICITUDES REGISTRADAS EN SUS ESTADOS PENDIENTES O EN TRANSITO PARA EL PERFIL DE EVALUADOR
			$query="SELECT *
				FROM aprobacion 
				JOIN pre_mejoramiento ON aprobacion.pre_mejoramiento_id=pre_mejoramiento.id_pre_mejoramiento
				JOIN tipo_mejoramiento ON tipo_mejoramiento.idtipo_mejoramiento=pre_mejoramiento.tipo_mejoramiento_id
				JOIN usuario ON usuario.idusuario=aprobacion.usuario_id
				JOIN seccion_usuario ON seccion_usuario.idseccion_usuario=pre_mejoramiento.seccion_usuario_id
				JOIN tipo_aprobacion ON pre_mejoramiento.tipo_aprobacion_id=tipo_aprobacion.idtipo_aprobacion
				LEFT JOIN evaluacion ON evaluacion.pre_mejoramiento_id=pre_mejoramiento.id_pre_mejoramiento
				WHERE $aprobacion
				
				AND pre_mejoramiento.clase_mejoramiento_id=$clase_mejoramiento
				ORDER BY pre_mejoramiento.id_pre_mejoramiento DESC";
		}

		//echo "$query";
		
		$result=$this->db->query($query);

		if ($result) {
			if ($result->num_rows()>0) {
				return $result;
			}else{
				return 0;
			}
		}else{
			echo "Error al realizar la busqueda.";
		}
	}

	public function evaluacion($id_usuario,$clase_mejoramiento_id,$tipo_aprobacion){

		if ($tipo_aprobacion == 2) {
			$tipo_aprobacion="v_mejoramiento_participante.tipo_aprobacion_id BETWEEN 1 AND 5";
			$join="";
		}elseif($tipo_aprobacion==3){
			$tipo_aprobacion="1"; // CONSULTA PARA PLANTA
			$join="";
		}elseif ($tipo_aprobacion==7) {
			$tipo_aprobacion="v_mejoramiento_participante.tipo_aprobacion_id=$tipo_aprobacion OR v_mejoramiento_participante.tipo_aprobacion_id=9";
			$join="JOIN aprobacion ON aprobacion.pre_mejoramiento_id=v_mejoramiento_participante.id_pre_mejoramiento
				JOIN evaluacion ON evaluacion.pre_mejoramiento_id=v_mejoramiento_participante.id_pre_mejoramiento";
		}else{
			$tipo_aprobacion="v_mejoramiento_participante.tipo_aprobacion_id=$tipo_aprobacion";
			$join="";
		}

		// if ($id_usuario==1 || $id_usuario==2) {
		// 	$usuario="1"; 
		// }else{
		// 	$usuario="v_mejoramiento_participante.usuario_id='$id_usuario'";
		// }

		if ($clase_mejoramiento_id==0) {
			$query="SELECT *
				FROM v_mejoramiento_participante 
				WHERE jefe_usuario_id='$id_usuario' 
				AND $tipo_aprobacion
				GROUP BY id_pre_mejoramiento
				ORDER BY pre_mejoramiento_date DESC";
		}else{
			$query="SELECT *
				FROM v_mejoramiento_participante 
				$join
				WHERE v_mejoramiento_participante.clase_mejoramiento_id=$clase_mejoramiento_id
				AND $tipo_aprobacion
				GROUP BY id_pre_mejoramiento
				ORDER BY pre_mejoramiento_date DESC";
		}

		//echo $query;
		
		$result=$this->db->query($query);

		if ($result) {
			if ($result->num_rows()>0) {
				return $result;
			}else{
				return null;
			}
		}else{
			echo "Error al realizar la busqueda.";
		}
	}

	public function m_aprobar($id_mejoramiento, $comentario_aprobacion, $num_mejora){
		$fecha=date("Y-m-d");
		$id_usuario=$this->session->userdata('id');

		try {
			$query1="UPDATE pre_mejoramiento 
					SET tipo_aprobacion_id=3
					WHERE id_pre_mejoramiento=$id_mejoramiento";//CAMBIA A APROBACION DE EA (ESPERA DE AHORRO)

			$result1=$this->db->query($query1);

			$query2="UPDATE aprobacion 
					SET tipo_aprobacion_id=3, 
					aprobacion_date='$fecha', 
					usuario_evaluador_id=$id_usuario
					WHERE pre_mejoramiento_id=$id_mejoramiento";
			$result2=$this->db->query($query2);//ESTADO PASA DE PL A EA (ESPERA DE AHORRO), FECHA DE CAMBIO

			$query3="INSERT INTO novedad 
					VALUES ('',$id_mejoramiento,'$comentario_aprobacion','$fecha',$id_usuario,2)";
			$result3=$this->db->query($query3);//ESTADO PASA DE PL A EA (ESPERA DE AHORRO), FECHA DE CAMBIO

			if ($result1) {
				if ($result2) {
					//echo "Mejoramiento Actualizado.";
					if ($result3) {
						//echo "Comentario Ingresado.";
						redirect(base_url().'evaluador/','refresh');
					}else{
						echo "Error al insertar comentario.";
					}
				}else{
					echo "Error al actualizar aprobacion.";
				}
				
			}else{
				echo "Error al actualizar Mejoramiento.";
			}
		} catch (Exception $e) {
			echo "Error al realizar actualizacion del mejoramiento.";
		}

		$result=$this->db->query($query);

		if ($result) {
			return $result;
		}else{
			return null;
		}
	}

	public function ver_detalle($id_mejoramiento){
		$query="SELECT * 
				FROM sugerencia
				JOIN participante ON participante.pre_mejoramiento_id=sugerencia.pre_mejoremiento_id
				JOIN usuario ON usuario.idusuario=participante.usuario_id
				JOIN pre_mejoramiento ON pre_mejoramiento.id_pre_mejoramiento=sugerencia.pre_mejoremiento_id
				LEFT JOIN post_mejoramiento ON post_mejoramiento.pre_mejoramiento_id=sugerencia.pre_mejoremiento_id
				LEFT JOIN estandarizacion ON estandarizacion.idestandarizacion=post_mejoramiento.estandarizacion_id
				LEFT JOIN novedad ON novedad.pre_mejoramiento_id=sugerencia.pre_mejoremiento_id
				JOIN aprobacion ON aprobacion.pre_mejoramiento_id=sugerencia.pre_mejoremiento_id
				JOIN tipo_aprobacion ON tipo_aprobacion.idtipo_aprobacion=aprobacion.tipo_aprobacion_id
				JOIN tipo_mejoramiento ON tipo_mejoramiento.idtipo_mejoramiento=pre_mejoramiento.tipo_mejoramiento_id
				JOIN seccion_usuario ON seccion_usuario.idseccion_usuario=pre_mejoramiento.seccion_usuario_id
				WHERE sugerencia.pre_mejoremiento_id=$id_mejoramiento";
			
			//echo "$query";
			try {
				$result=$this->db->query($query);

				if ($result) {
					//print_r($result->row());
					return $result->row();
					echo "OK!!!";
				}else{
					echo "NULL!!!";
					return null;
				}
			} catch (Exception $e) {
				
			}

			
			//echo "detalle";
	}

	public function evaluar($concepcion,$metodo,$estandarizacion,$esfuerzo,$efecto,$comentario,$id_mejoramiento){
		$fecha=date("Y-m-d");
		$usuario_evaluador_id=$this->session->userdata('id');//USUARIO ACTUAL EN ESTE CASO EL EVALUADOR


		if ($comentario!="" || $comentario!=NULL) {
			$this->db->query("INSERT INTO novedad 
						VALUES(NULL,$id_mejoramiento,'$comentario','$fecha',$usuario_evaluador_id,5)");
			$id_novedad=$this->db->insert_id();
		}else{
			$id_novedad=0;
		}

		try {
			$total=$concepcion+$metodo+$estandarizacion+$esfuerzo+$efecto;
			$this->db->query("INSERT INTO evaluacion 
						VALUES(NULL,$id_mejoramiento,$usuario_evaluador_id,$concepcion,$metodo,$estandarizacion,$esfuerzo,$efecto,'$fecha',$id_novedad,$total)");

			$query1="UPDATE pre_mejoramiento 
					SET tipo_aprobacion_id=7
					WHERE id_pre_mejoramiento=$id_mejoramiento";//CAMBIA A APROBACION DE PRESENTACION LIDER
			$result1=$this->db->query($query1);

			$query2="UPDATE aprobacion 
					SET tipo_aprobacion_id=7, 
					aprobacion_date='$fecha', 
					usuario_evaluador_id=$usuario_evaluador_id
					WHERE pre_mejoramiento_id=$id_mejoramiento";
			$result2=$this->db->query($query2);//

			if ($result1) {
				//echo "actualizacion de mejoramiento OK.";
				if ($result2) {
					//echo "actualizacion de aprobacion OK.";
					return "OK";
				}else{
					return NULL;
				}
			}else{
				return NULL;
			}

		} catch (Exception $e) {
			echo "Error al realizar proceso de finalización.";
		}

	}


}

/* End of file m_evaluador.php */
/* Location: ./application/models/m_evaluador.php */
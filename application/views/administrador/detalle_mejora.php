<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* End of file pre_solicitud.php */
/* Location: ./application/views/proponente/pre_solicitud.php */
?>
<?php 
if ($this->session->userdata('login')){
    if ($this->session->userdata('tipo')==3) {//USUARIO TIPO ADMINISTRADOR
    ?>
 <!-- Page Content -->
<div class="container">
    <div class="row">
        <legend><?= $detalles->id_pre_mejoramiento;?> - <?= $detalles->pre_mejoramiento_titulo; ?></legend>
        
        <div class="col-lg-12">
          <fieldset>
            <!-- Select Basic -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="selectbasic">Tipo de Mejoramiento</label>
                <div class="col-md-8">
                    <label for="" class="help-block"><?= $detalles->tipo_mejoramiento_descripcion; ?></label>
                </div>
            </div>

            <!-- Appended Input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="appendedtext">Participantes</label>
                <div class="col-md-8">
                  <?php foreach ($participantes->result() as $usuario): ?>
                    <label for="" class="help-block"><?= $usuario->usuario_nombre; ?> <?= $usuario->usuario_apellido; ?></label>
                  <?php endforeach ?>
                    <!--<label for="" class="help-block">2XXXXXX</label>-->
                </div>
            </div>
            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="textinput">Fecha de Registro</label>  
                <div class="col-md-8">
                    <label for="" class="help-block"><?= $detalles->pre_mejoramiento_date; ?></label>
                </div>
            </div>

            <!-- Select Basic -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="selectbasic">Sección a Aplicar</label>
              <div class="col-md-8">
                <label for="" class="help-block"><?= $detalles->seccion_usuario_descripcion; ?></label>
              </div>
            </div>

            <!-- Textarea -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="textarea">Antes del Mejoramiento</label>
              <div class="col-md-8">                     
                <textarea class="form-control" id="textarea" name="textarea" rows="5" disabled><?= $detalles->sugerencia_antes_mejora; ?></textarea>
              </div>
              <div class="col-lg-8 col-lg-offset-4"><p class="help-block"></p></div>
            </div>

            <!-- Textarea -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="textarea">Problemas</label>
              <div class="col-md-8">                     
                <textarea class="form-control" id="textarea" name="textarea" rows="5" disabled><?= $detalles->sugerencia_problema; ?></textarea>
              </div>
              <div class="col-lg-8 col-lg-offset-4"><p class="help-block"></p></div>
            </div>

            <!-- Textarea -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="textarea">Efecto esperado</label>
              <div class="col-md-8">                     
                <textarea class="form-control" id="textarea" name="textarea" rows="5" disabled><?= $detalles->sugerencia_esperado; ?></textarea>
              </div>
              <div class="col-lg-8 col-lg-offset-4"><p class="help-block"></p></div>
            </div>

            <!-- Button -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="singlebutton">Formato de Ahorro</label>
              <div class="col-md-8">
                <!--<button id="singlebutton" name="singlebutton" class="btn btn-primary">Enviar</button>-->
                <?php if ($detalles->sugerencia_ahorro==0) {
                  $ahorro="No aplica Ahorros.";
                }elseif($detalles->sugerencia_ahorro==1 && $detalles->sugerencia_fahorro!=""){
                  $ahorro="<a href=\"".base_url()."proponente/download_file/".$detalles->sugerencia_fahorro."/F_A\">".$detalles->sugerencia_fahorro."</a>";
                }elseif($detalles->sugerencia_ahorro==1 && $detalles->sugerencia_fahorro==""){
                  $ahorro="Ahorro Pendiente Confirmar.";
                }
                ?>
                <label for="" class="control-label text-danger"><?= $ahorro; ?></label>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-4 control-label" for="singlebutton">Descripción o bosquejo</label>
              <div class="col-md-8">
               <a href="<?=base_url();?>proponente/download_file/<?=$detalles->sugerencia_propuesta; ?>/EV"><?= $detalles->sugerencia_propuesta; ?></a>
              </div>
            </div>
          </div>


          <!-- ----------------------------------- FASE 2 --------------------------------- -->
          <div class="col-md-12">
            <hr>
          </div>

          <div class="col-lg-12">

            <br>
            <br>

          <fieldset>
            <!-- Form Name -->
            <legend>Detalles de Fase Implementación para - <b><?=$detalles->pre_mejoramiento_titulo; ?></b></legend>

            <input type="hidden" name="id_pre_mejoramiento" value="<?=$detalles->pre_mejoramiento_id; ?>">

            <!-- Textarea -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="antes_mejoramiento">Despúes del Mejoramiento</label>
              <div class="col-md-8">                     
                <textarea class="form-control" id="despues_mejoramiento" rows="5" disabled name="despues_mejoramiento" placeholder="Sin Datos"><?= $detalles->post_mejoramiento_despues; ?></textarea>
              </div>
              <div class="col-lg-8 col-lg-offset-4"><p class="help-block">*Max 500 Caracteres.</p></div>
            </div>

            <!-- Textarea -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="problema">Estandarización</label>
              <div class="col-md-8">                     
                <textarea class="form-control" id="estandarizacion" rows="5" disabled name="estandarizacion" placeholder="Sin Datos"><?= $detalles->estandarizacion_descripcion; ?></textarea>
              </div>
              <div class="col-lg-8 col-lg-offset-4"><p class="help-block">*Max 500 Caracteres.</p></div>
            </div>

            <!-- Multiple Radios -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="ahorro">Evidencias</label>
              <div class="col-md-4 help-block">
                <a href="<?=base_url();?>proponente/download_file/<?=$detalles->post_mejoramiento_evidencia; ?>/EV"><?= $detalles->post_mejoramiento_evidencia; ?></a>
              </div>
              <div class="col-lg-8 col-lg-offset-4">
                <p class="help-block">*Documento con (Excel/Word) del resultado de la sugerencia.</p>
                <p class="help-block">*Máximo 5MB.</p>
              </div>
            </div>

          </fieldset>
          </div>


          
          <div class="col-lg-12">               
            <!-- Button -->
            <div class="form-group">
              <div class="col-lg-4">
                 <?php if ($indicador==6){
                  ?><div class="col-lg-12"><a href="<?= base_url(); ?>administrador/ver_mejora/6" id="singlebutton" name="singlebutton" class="btn btn-primary">Volver</a></div><?php
                }elseif ($indicador==2) {
                  ?><div class="col-lg-12"><a href="<?= base_url(); ?>administrador/ver_mejora/2" id="singlebutton" name="singlebutton" class="btn btn-primary">Volver</a></div><?php
                }elseif ($indicador==7) {
                  ?><div class="col-lg-12"><a href="<?= base_url(); ?>administrador/ver_mejora/7" id="singlebutton" name="singlebutton" class="btn btn-primary">Volver</a></div><?php
                }else{
                  ?><div class="col-lg-12"><a href="<?= base_url(); ?>administrador/" id="singlebutton" name="singlebutton" class="btn btn-primary">Volver</a></div><?php
                } ?>
              </div>
            </div>
          </div>

          </fieldset>

        </div>
        

    </div>
<?php }
}else{
    redirect('main/login','refresh');

} ?>
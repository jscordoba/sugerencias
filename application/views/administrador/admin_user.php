<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	
	<div class="container">
		
	    <div class="row">
	      <div class="col-lg-12">
	          <h1 class="page-header">Nuevo usuario</h1>
	      </div>
	      <!-- /.col-lg-12 -->
	    </div>

	    <div class="row">
	      <div class="col-md-12">
	        <form class="form-horizontal" action="<?=base_url();?>administrador/reg_user">
	          <fieldset>
	            <div class="col-md-6">
	               <!-- Text input-->
	              <div class="form-group">
	                <label class="col-md-4 control-label" for="id_usuario">Identificación *</label>  
	                <div class="col-md-8">
	                <input id="id_usuario" name="id_usuario" type="number" placeholder="Cedula de Ciudadania" class="form-control input-md">
	                <span class="help-block">*Digite el número de Identidad</span>  
	                </div>
	              </div>


	              <!-- Text input-->
	              <div class="form-group">
	                <label class="col-md-4 control-label" for="nombre_usuario">Nombre *</label>  
	                <div class="col-md-8">
	                <input id="nombre_usuario" name="nombre_usuario" type="text" placeholder="Nombre(s)" class="form-control input-md" required>
	                <span class="help-block">*Digite el(los) Nombre(s)</span>  
	                </div>
	              </div>

	              <!-- Text input-->
	              <div class="form-group">
	                <label class="col-md-4 control-label" for="apellido_usuario">Apellidos *</label>  
	                <div class="col-md-8">
	                <input id="apellido_usuario" name="apellido_usuario" type="text" placeholder="Apellido(s)" class="form-control input-md" required>
	                <span class="help-block">*Digite el(los) Apellido(s)</span>  
	                </div>
	              </div>

	              <!-- Text input-->
	              <div class="form-group">
	                <label class="col-md-4 control-label" for="clave_usuario">Clave *</label>  
	                <div class="col-md-8">
	                <input id="clave_usuario" name="clave_usuario" type="password" placeholder="Ingrese la clave" class="form-control input-md" required>
	                <span class="help-block">*Se sugiere sea mínimo de 8 dígitos</span>  
	                </div>
	              </div>
	            </div>

	            <div class="col-md-6">
	              <!-- Select Basic -->
	              <div class="form-group">
	                <label class="col-md-4 control-label" for="seccion_usuario">Sección *</label>
	                <div class="col-md-8">
	                  <select id="" name="seccion_usuario" class="form-control">
	                    <option value="">Seleccione...</option>
	                    <?php 
	                      foreach ($secciones->result() as $seccionesr): ?>
	                        <option value="<?= $seccionesr->idseccion_usuario; ?>"><?= $seccionesr->seccion_usuario_descripcion; ?></option>
                      	<?php endforeach ?>
	                  </select>
	                  <span class="help-block">*Seleccione la Sección correspondiente</span>  
	                </div>
	              </div>

	              <!-- Select Basic -->
	              <div class="form-group">
	                <label class="col-md-4 control-label" for="planta_usuario">Planta *</label>
	                <div class="col-md-8">
	                  <select id="" name="planta_usuario" class="form-control" required>
	                    <option value="">Seleccione...</option>
	                    <option value="PLANTA 1">Planta 1</option>
	                    <option value="PLANTA 2">Planta 2</option>
	                    <option value="ZF">Zona Franca</option>
	                  </select>
	                  <span class="help-block">*Seleccione Planta</span>  
	                </div>
	              </div>

	              <!-- Select Basic -->
	              <div class="form-group">
	                <label class="col-md-4 control-label" for="tipo_usuario">Perfil *</label>
	                <div class="col-md-8">
	                  <select id="" name="tipo_usuario" class="form-control" required>
	                    <option value="">Seleccione...</option>
	                    <option value="1">Administrativo</option>
	                    <option value="4">Planta</option>
	                    <option value="2">Evaluador</option>
	                  </select>
	                  <span class="help-block">*Seleccione el tipo de Usuario</span>  
	                </div>
	              </div>

	              <!-- Text input-->
	              <div class="form-group">
	                <label class="col-md-4 control-label" for="email_usuario">Email</label>  
	                <div class="col-md-8">
	                <input id="email_usuario" name="email_usuario" type="email" placeholder="email@magnetron.com.co" class="form-control input-md">
	                <span class="help-block">*Digite correo electrónico Empresarial</span>  
	                </div>
	              </div>

	              <!-- Button -->
	              <div class="form-group">
	                <label class="col-md-4 control-label" for="reg_new_user"></label>
	                <div class="col-md-8 text-center">
	                  <button id="btn_reg_new_user" name="btn_reg_new_user" class="btn btn-primary">Registrar</button>
	                </div>
	              </div>
	            </div>      

	          </fieldset>
	        </form>
	      </div>
	    </div>
	</div>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* End of file header.php */
/* Location: ./application/views/header.php */

if ($this->session->userdata('login')){ 
    if ($this->session->userdata('tipo')==3) {//USUARIO TIPO ADMINISTRADOR
    	// if (condition) {
    	// 	# code...
    	// }
    	?>
		<div class="container">
	    	<div class="row">
				<div class="col-md-8">
					<legend>Administración de Usuarios - Proponentes</legend>
				</div>	

				<div class="col-md-4">
                    <form class="form-horizontal" method="POST" action="<?=base_url();?>administrador/search" enctype="multipart/form-data">
                        <h4>Consultas</h4>
                        <div class="input-group">
                            <input type="text" class="form-control" name="c_buscar" placeholder="Nombre o ID del usuario">
                            <input type="hidden" name="tipo_usuario" value="<?= $tipo_user; ?>">
                            <span class="input-group-btn">
	                            <button class="btn btn-default" type="button">
	                                    <span class="glyphicon glyphicon-search"></span>
	                            </button>
                            </span>
                        </div>
                        <br>
                        <!-- /.input-group -->
                    </form>
				</div>

				<br>
	
				
	            <div class="col-lg-12">
	                <table class="table table-hover">
	                    <tr><th>ID</th><th>Nombre(s)</th><th>Apellido(s)</th><th>Sección</th><th>Usuario</th><th>Clave</th><th>Tipo</th><th>Acción</th></tr>
	               <?php
	                   if ($usuarios) {
	                       foreach ($usuarios->result() as $usuario) { 
	                       	if ($usuario->tipo_usuario_id==1) {
	                       		$usuario_tipo="Proponente";
	                       	}elseif ($usuario->tipo_usuario_id==2) {
	                       		$usuario_tipo="Lider Mejoramiento";
	                       	}elseif ($usuario->tipo_usuario_id==3) {
	                       		$usuario_tipo="Administrador";
	                       	}elseif ($usuario->tipo_usuario_id==4) {
	                       		$usuario_tipo="Proponente Planta";
	                       	} ?>
	                        <tr>
	                            <td><?=$usuario->idusuario;?></td>
	                            <td><?=$usuario->usuario_nombre;?></td>
	                            <td><?=$usuario->usuario_apellido;?></td>
	                            <td><?=$usuario->seccion_usuario_descripcion;?></td>
	                            <td><?=$usuario->usuario_usuario; ?></td>
	                            <td><?=$usuario->usuario_clave;?></td>
	                            <?php if ($usuario->idjefe_area){ ?>
	                            	<td><b>JEFE DE SECCIÓN</b></td>
	                            <?php }else{ ?>
	                            <td><?=$usuario_tipo;?></td>
	                            <?php } ?>
	                            <td>
	                            	<a href="<?=base_url();?>administrador/us_edi/<?=$usuario->idusuario; ?>" class="btn btn-sm btn-primary">Editar</a> 
	                            	<a href="<?=base_url();?>administrador/us_del/<?=$usuario->idusuario; ?>" class="btn btn-sm btn-success btn-danger">Eliminar</a></td></tr>
	                        </tr>
	                    <?php } 
	                   }
	                ?>
	                </table>
	            </div>
	        </div>

<?php }else{
    redirect('main/login','refresh');
    //diesel 
	}
}
 
?>
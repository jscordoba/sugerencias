<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* End of file header.php */
/* Location: ./application/views/header.php */

if ($this->session->userdata('login')){ 
    if ($this->session->userdata('tipo')==3) {//USUARIO TIPO ADMINISTRADOR?>
		<div class="container">
	    	<div class="row">
			<legend>Administración de Mejoras</legend>

	            <div class="col-lg-12">
	                <table class="table table-hover table-responsive">
	                    <tr><th>ID</th><th>Titulo Mejoramiento</th><th>Tipo de Mejoramiento</th><th>Estado</th><th>Fecha Registro</th><th>Clase</th><th>Usuario</th><th>Acción</th></tr>
	               <?php
	                   if ($mejoras) {
	                       foreach ($mejoras->result() as $mejora) { ?>
	                        <tr>
	                            <td><?=$mejora->id_pre_mejoramiento;?></td>
                                <td><?=$mejora->pre_mejoramiento_titulo;?></td>
                                <td><?=$mejora->tipo_mejoramiento_descripcion;?></td>
                                <td><?=$mejora->tipo_aprobacion_descripcion;?></td>
                                <td><?=$mejora->pre_mejoramiento_date;?></td>
                                <td><?=$mejora->clase_mejoramiento_descripcion;?></td>
                                <td><?=$mejora->usuario_nombre;?></td>
	                            <td><a href="<?=base_url();?>administrador/detalle/<?=$mejora->id_pre_mejoramiento; ?>/<?= $id; ?>" class="btn btn-sm btn-primary">Detalles</a></td>
	                        </tr>
	                    <?php } 
	                   }
	                ?>
	                </table>
	            </div>
	        </div>

<?php }else{
    redirect('main/login','refresh');
    //diesel 
	}
}
 
?>
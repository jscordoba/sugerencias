<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* End of file pre_solicitud.php */
/* Location: ./application/views/proponente/pre_solicitud.php */
?>

<?php 
if ($this->session->userdata('login')){ 
    if ($this->session->userdata('tipo')==1 || $this->session->userdata('tipo')==4) {//USUARIO TIPO PROPONENTE------------------------------------------
    ?>

 <!-- Page Content -->
<div class="container">
    <div class="row">
        <?php if ($tipo_id==11){// PROPONENTE REGISTRADAS ####################################?>
            <legend><?=$tipo?></legend>
            
            <div class="col-lg-12">
                <table class="table table-hover">
                    <tr><th>ID</th><th>Titulo</th><th>Tipo</th><th>Fecha Registro</th><th>Clase</th><th>Acción</th></tr>
                    <?php 
                    if ($registrados) {
                        foreach ($registrados->result() as $registrado) { ?>
                            <tr>
                                <td><?=$registrado->id_pre_mejoramiento;?></td>
                                <td><?=$registrado->pre_mejoramiento_titulo;?></td>
                                <td><?=$registrado->tipo_mejoramiento_descripcion;?></td>
                                <td><?=$registrado->pre_mejoramiento_date;?></td>
                                <td><?=$registrado->clase_mejoramiento_descripcion; ?></td>
                                <td><a href="<?=base_url();?>proponente/sol_aprobacion/<?=$registrado->id_pre_mejoramiento;?>/<?=$registrado->seccion_usuario_id;?>" class="btn btn-sm btn-success">Solicitar Aprobación</a></td>
                                <td><a href="<?=base_url();?>proponente/editar/<?=$registrado->id_pre_mejoramiento;?>" class="btn btn-sm btn-primary">Editar</a></td>
                                <!--<td><a href="<?=base_url();?>proponente/delete/<?=$registrado->id_pre_mejoramiento;?>" class="btn btn-sm btn-danger">Eliminar</a></td>-->
                            </tr>


                        <?php }
                    }
                    ?>
                </table>
            </div>
        <?php }elseif ($tipo_id==13){// PROPONENTE ACEPTADAS Y ACTIVAS #################################### ?>
            
            <legend><?=$tipo?></legend>
            <?php //echo $tipo_id; ?>
            
            <div class="col-lg-12">
                <table class="table table-hover">
                    <tr><th>ID</th><th>Titulo</th><th>Tipo</th><th>Estado</th><th>Fecha Registro</th><th>Acción</th></tr>
                    <?php 
                    if ($registrados) {
                        foreach ($registrados->result() as $registrado) { 
                            if ($registrado->tipo_aprobacion_descripcion=="F2") {
                                $solicita_evaluacion="<td><a href=\"".base_url()."proponente/pos_sugerencia/$registrado->id_pre_mejoramiento\" class=\"btn btn-sm btn-success btn-success\">Enviar a Fase 2</a> </td>";//PASAR A ESTADO AL (APROBACION DE AHORRO LIDER)
                                
                                if ($registrado->sugerencia_ahorro==1 && $registrado->sugerencia_fahorro=="") {
                                    $solicita_evaluacion.="<td><a href=\"".base_url()."proponente/cargar_ahorro/$registrado->id_pre_mejoramiento/0\" class=\"btn btn-sm btn-warning btn-success\">Cargar Ahorro</a> </td>";//  ENVIAR A FORM DE CARGA DE AHORRO
                                }
                            }else{
                                $solicita_evaluacion="";
                            } ?>
                            <tr>
                                <td><?=$registrado->id_pre_mejoramiento;?></td>
                                <td><?=$registrado->pre_mejoramiento_titulo;?></td>
                                <td><?=$registrado->tipo_mejoramiento_descripcion;?></td>
                                <td><?=$registrado->tipo_aprobacion_descripcion;?></td>
                                <td><?=$registrado->pre_mejoramiento_date;?></td>
                                <td><a href="<?=base_url();?>proponente/detalle/<?=$registrado->id_pre_mejoramiento;?>/<?= $indicador; ?>" class="btn btn-sm btn-primary">Detalles</a> </td>
                                <?= $solicita_evaluacion;?>
                                <!--<td><a href="<?=base_url();?>proponente/delete/ --><?php /*$registrado->id_pre_mejoramiento;*/ ?><!--" class="btn btn-sm btn-danger">Eliminar</a></td>-->
                            </tr>
                        <?php }
                    }
                    ?>
                </table>
            </div>

        <?php }elseif ($tipo_id==14) {// PROPONENTE FINALIZADAS ####################################?>
            <legend><?=$tipo?></legend>
            
            <div class="col-lg-12">
                <table class="table table-hover">
                    <tr><th>ID</th><th>Titulo</th><th>Tipo</th><th>Inicio</th><th>Clase</th><th>Finalización</th><th>Puntaje</th><th colspan="3" class="text-center">Acción</th><th>Aprobación</th></tr>
                <?php 
                if ($registrados) {
                    foreach ($registrados->result() as $registrado) { ?>
                        <tr>
                            <td><?=$registrado->id_pre_mejoramiento;?></td>
                            <td><?=$registrado->pre_mejoramiento_titulo;?></td>
                            <td><?=$registrado->tipo_mejoramiento_descripcion;?></td>
                            <td><?=$registrado->pre_mejoramiento_date;?></td>
                            <td><?=$registrado->clase_mejoramiento_descripcion; ?></td>
                            <td><?=$registrado->aprobacion_date; ?></td>
                            <td><?=$registrado->evaluacion_total; ?>/75</td>
                            <td><a href="<?=base_url();?>proponente/detalle/<?=$registrado->id_pre_mejoramiento;?>/<?= $indicador; ?>" class="btn btn-sm btn-primary">Detalles</a> </td>
                            <td><a href="<?=base_url();?>ver_mejora.php?tipo_id=14&id=<?= $registrado->id_pre_mejoramiento;?>" data-toggle="modal" data-target="#detalle_evaluacion" class="btn btn-sm btn-success">Califización</a> </td>
                        </tr>
                    <?php }
                } ?>
                </table>
            </div>

            <?php if ($evaluacion) { ?>
            <!-- ############################# SECCION DE MODAL ############################## -->
            <div class="modal fade " id="detalle_evaluacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">

                  <div class="modal-header"><!--ENCABEZADO DE MODAL-->
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Resultados de Evaluación</h4>
                  </div>

                  <div class="modal-body row"><!--CONTENIDO DE MODAL-->
                    <div class="col-md-12">

                        <i>Calificación se realiza con un puntaje de 1 a 15, siendo 1 el de Menor peso y 15 el de Mayor.</i>
                        <table class="table table-hover">
                        <tr><th>ID</th><th>Titulo</th><th>Tipo</th><th>Concepción</th><th>Método</th><th>Estandarización</th><th>Esfuerzo</th><th>Efecto</th><th>Total</th></tr>
                        <?php 
                        /*$id=$this->input->get('id');
                        echo "$id";
                        if (!isset($id)) {
                            echo "<br>Sin información de Evaluación.";
                        }else{*/
                        
                            foreach ($evaluacion->result() as $resultado){ 
                            //if ($resultado->id_pre_mejoramiento==$id) { ?>
                                <tr>
                                <td><?=$resultado->id_pre_mejoramiento;?></td>
                                <td><?=$resultado->pre_mejoramiento_titulo;?></td>
                                <td><?=$resultado->tipo_mejoramiento_descripcion;?></td>
                                <td><?=$resultado->evaluacion_concepcion; ?></td>
                                <td><?=$resultado->evaluacion_metodo; ?></td>
                                <td><?=$resultado->evaluacion_estandarizacion; ?></td>
                                <td><?=$resultado->evaluacion_esfuerzo; ?></td>
                                <td><?=$resultado->evaluacion_efecto; ?></td>
                                <td><b><?=$resultado->evaluacion_total; ?></b></td>
                                <!--<td><a href="<?=base_url();?>proponente/delete/<?=$resultado->id_pre_mejoramiento;?>" class="btn btn-sm btn-danger">Eliminar</a></td>-->
                            </tr> <?php
                            //}
                            }
                        //} ?>
                    </table>
                  
                    </div>
                  </div>

                  <div class="modal-footer"><!--PIE DE PAGINA DE MODAL-->
                    <button type="button" class="btn btn-success btn-sm" data-dismiss="modal">Cerrar</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div>
              <?php } ?>
            <!-- ################################## FIN DE SECCION MODAL ############################## -->

        <?php }elseif ($tipo_id==15) {// PROPONENTE NEGADAS####################################?>
            <legend><?=$tipo?></legend>
            
            <div class="col-lg-12">
                <table class="table table-hover">
                    <tr><th>ID</th><th>Titulo</th><th>Tipo</th><th>Inicio</th><th>Clase</th><th>Finalización</th><th colspan="3" class="text-center">Acción</th></tr>
                <?php 
                if ($registrados) {
                    foreach ($registrados->result() as $registrado) { 

                        ?>
                        <tr>
                            <td><?=$registrado->id_pre_mejoramiento;?></td>
                            <td><?=$registrado->pre_mejoramiento_titulo;?></td>
                            <td><?=$registrado->tipo_mejoramiento_descripcion;?></td>
                            <td><?=$registrado->pre_mejoramiento_date;?></td>
                            <td><?=$registrado->clase_mejoramiento_descripcion; ?></td>
                            <td></td>
                            <td><a href="<?=base_url();?>proponente/detalle/<?=$registrado->id_pre_mejoramiento;?>/<?= $indicador; ?>" class="btn btn-sm btn-primary">Detalles</a> </td>
                        </tr>
                    <?php }
                } ?>
                </table>
            </div>

        <?php }elseif ($tipo_id==41) {// PROPONENTE DE PLANTA ####################################?>
                <legend><?=$tipo?></legend>

                <div class="col-lg-12">
                    <ul class="nav nav-pills" role="tablist">
                      <li class="active col-lg-3 text-primary"><label for="">En Desarrollo <span class="badge">-</span></label></li>
                      <li class="col-lg-3 text-primary"><label for="">Finalizados <span class="badge">-</span></label></li>
                      <li class="col-lg-3 text-primary"><label for="">Rechazados <span class="badge">-</span></label></li>
                    </ul>
                    <br>

                     <table class="table table-hover">
                        <tr><th>ID</th><th>Titulo</th><th>Usuario</th><th>Tipo</th><th>Estado</th><th>Fecha Registro</th><th>Acción</th></tr>
                        <?php 
                        if ($registrados) {
                            foreach ($registrados->result() as $registrado) { 
                                if ($registrado->idtipo_usuario==4) {
                                    if ($registrado->tipo_aprobacion_descripcion=="F2") {
                                        $solicita_evaluacion="<td><a href=\"".base_url()."proponente/pos_sugerencia/$registrado->id_pre_mejoramiento\" class=\"btn btn-sm btn-success btn-success\">Enviar a Fase 2</a> </td>";//PASAR A ESTADO AL (APROBACION DE AHORRO LIDER)
                                    }else{
                                        $solicita_evaluacion="";
                                    }

                                    if ($registrado->idtipo_aprobacion==9) {
                                         $comentario_ng="<td><a href=\"".base_url()."proponente/comentario_ng/$registrado->id_pre_mejoramiento\" class=\"btn btn-sm btn-success btn-danger\">Comentario</a> </td>";//PASAR A ESTADO AL (APROBACION DE AHORRO LIDER)
                                     }elseif ($registrado->idtipo_aprobacion==7) {
                                         $comentario_ng="<td><a href=\"".base_url()."ver_mejora.php?tipo_id=14&id=". $registrado->id_pre_mejoramiento."\" data-toggle=\"modal\" data-target=\"#detalle_evaluacion\" class=\"btn btn-sm btn-success\">Calificación</a> </td>";
                                     }else{
                                        $comentario_ng="";
                                     }

                                    if ($registrado->sugerencia_ahorro==1 && $registrado->sugerencia_fahorro=="") {
                                        $ahorro="<td><a href=\"".base_url()."proponente/cargar_ahorro/$registrado->id_pre_mejoramiento/0\" class=\"btn btn-sm btn-warning btn-success\">Cargar Ahorro</a> </td>";//  ENVIAR A FORM DE CARGA DE AHORRO
                                    }else{
                                         $ahorro="";
                                    }
                                ?>

                                <tr>
                                    <td><?=$registrado->id_pre_mejoramiento;?></td>
                                    <td><?=$registrado->pre_mejoramiento_titulo;?></td>
                                    <td><?=$registrado->usuario_nombre;?> <?=$registrado->usuario_apellido;?></td>
                                    <td><?=$registrado->tipo_mejoramiento_descripcion;?></td>
                                    <td><?=$registrado->tipo_aprobacion_descripcion;?></td>
                                    <td><?=$registrado->pre_mejoramiento_date;?></td>
                                    <td><a href="<?=base_url();?>proponente/detalle/<?=$registrado->id_pre_mejoramiento;?>/<?= $indicador; ?>" class="btn btn-sm btn-primary">Detalles</a> </td>
                                    <?= $solicita_evaluacion;?> <?= $ahorro;?>  <?= $comentario_ng;?>
                                    <!--<td><a href="<?=base_url();?>proponente/delete/<?=$registrado->id_pre_mejoramiento;?>" class="btn btn-sm btn-danger">Eliminar</a></td>-->
                                </tr>
                            <?php }else{
                                    //echo "string";
                                }
                            }
                        }
                        ?>
                    </table>
                </div>

                <?php if (isset($evaluacion)) { 


                    ?>
                <!-- ############################# SECCION DE MODAL ############################## -->
                <div class="modal fade " id="detalle_evaluacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">

                      <div class="modal-header"><!--ENCABEZADO DE MODAL-->
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Resultados de Evaluación</h4>
                      </div>

                      <div class="modal-body row"><!--CONTENIDO DE MODAL-->
                        <div class="col-md-12">

                            <i>Calificación se realiza con un puntaje de 1 a 15, siendo 1 el de Menor peso y 15 el de Mayor.</i>
                            <table class="table table-hover">
                            <tr><th>ID</th><th>Titulo</th><th>Tipo</th><th>Concepción</th><th>Método</th><th>Estandarización</th><th>Esfuerzo</th><th>Efecto</th><th>Total</th></tr>
                            <?php 
                                foreach ($evaluacion->result() as $resultado){ 
                                //if ($resultado->id_pre_mejoramiento==$id) { ?>
                                    <tr>
                                    <td><?=$resultado->id_pre_mejoramiento;?></td>
                                    <td><?=$resultado->pre_mejoramiento_titulo;?></td>
                                    <td><?=$resultado->tipo_mejoramiento_descripcion;?></td>
                                    <td><?=$resultado->evaluacion_concepcion; ?></td>
                                    <td><?=$resultado->evaluacion_metodo; ?></td>
                                    <td><?=$resultado->evaluacion_estandarizacion; ?></td>
                                    <td><?=$resultado->evaluacion_esfuerzo; ?></td>
                                    <td><?=$resultado->evaluacion_efecto; ?></td>
                                    <td><b><?=$resultado->evaluacion_total; ?></b></td>
                                    <!--<td><a href="<?=base_url();?>proponente/delete/<?=$resultado->id_pre_mejoramiento;?>" class="btn btn-sm btn-danger">Eliminar</a></td>-->
                                </tr> <?php
                                //}
                                }
                            
                                
                            //} ?>
                        </table>
                      
                        </div>
                      </div>

                      <div class="modal-footer"><!--PIE DE PAGINA DE MODAL-->
                        <button type="button" class="btn btn-success btn-sm" data-dismiss="modal">Cerrar</button>
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div>
                  <?php } ?>
                <!-- ################################## FIN DE SECCION MODAL ############################## -->
        <?php }

        if ($this->session->userdata('jefe')) { 
            if ($tipo_id==31) {//SUGERENCIAS ACTIVAS DESDE PERFIL DE JEFE ####################################?>
                <legend><?=$tipo?></legend>

                <div class="col-lg-12">
                    <table class="table table-hover">
                        <tr><th>ID</th><th>Titulo</th><th>Usuario</th><th>Tipo</th><th>Fecha Registro</th><th>Clase</th><th>Acción</th></tr>
                    <?php 
                    if ($registrados) {
                        foreach ($registrados->result() as $registrado) { ?>
                            <tr>
                                <td><?=$registrado->id_pre_mejoramiento;?></td>
                                <td><?=$registrado->pre_mejoramiento_titulo;?></td>
                                <td><?=$registrado->usuario_nombre;?></td>
                                <td><?=$registrado->tipo_mejoramiento_descripcion;?></td>
                                <td><?=$registrado->pre_mejoramiento_date;?></td>
                                <td><?=$registrado->clase_mejoramiento_descripcion; ?></td>
                                <td><a href="<?=base_url();?>proponente/detalle/<?=$registrado->id_pre_mejoramiento;?>/<?= $indicador; ?>" class="btn btn-sm btn-primary">Detalles</a> </td>
                                <td><a href="<?=base_url();?>proponente/aprobar/<?=$registrado->id_pre_mejoramiento;?>/pre" class="btn btn-sm btn-success">Aprobar</a> </td>
                                <td><a href="<?=base_url();?>proponente/negar/<?=$registrado->id_pre_mejoramiento; ?>/1" class="btn btn-sm btn-success btn-danger">No Aprobar</a></td>
                            </tr>
                        <?php }
                    } ?>
                    </table>
                </div>
        <?php }elseif ($tipo_id==32) {// PROPONENTE JEFE - IMPLEMENTACION ####################################?>
                <legend><?=$tipo?></legend>

                <div class="col-lg-12">
                    <table class="table table-hover">
                        <tr><th>ID</th><th>Titulo</th><th>Tipo</th><th>Fecha Registro</th><th>Estado</th><th>Clase</th><th>Acción</th></tr>
                    <?php 
                    if ($registrados) {
                        foreach ($registrados->result() as $registrado) { ?>
                            <tr>
                                <td><?=$registrado->id_pre_mejoramiento;?></td>
                                <td><?=$registrado->pre_mejoramiento_titulo;?></td>
                                <td><?=$registrado->tipo_mejoramiento_descripcion;?></td>
                                <td><?=$registrado->pre_mejoramiento_date;?></td>
                                <td><?=$registrado->tipo_aprobacion_descripcion;?></td>
                                <td><?=$registrado->clase_mejoramiento_descripcion; ?></td>
                                <td><a href="<?=base_url();?>proponente/detalle/<?=$registrado->id_pre_mejoramiento;?>/<?= $indicador; ?>" class="btn btn-sm btn-primary">Detalles</a> </td>
                                <td><a href="<?=base_url();?>proponente/aprobar/<?=$registrado->id_pre_mejoramiento;?>/imp" class="btn btn-sm btn-success">Aprobar</a> </td>
                                <td><a href="<?=base_url();?>proponente/negar/<?=$registrado->id_pre_mejoramiento; ?>/1" class="btn btn-sm btn-success btn-danger">No Aprobar</a></td>
                            </tr>
                        <?php }
                    } ?>
                    </table>
                </div>
        <?php }
        } ?>
        
    </div>
<?php }
    elseif ($this->session->userdata('tipo')==2) {//USUARIO TIPO EVALUADOR ------------------------------------------
?>
<!-- Page Content -->
<div class="container">
    <div class="row">
        <?php if ($tipo_id==11){// EVALUADOR PENDIENTES SUGERENCIAS####################################?>
            <legend><?=$tipo?></legend>
            
            <div class="col-lg-12">
                <table class="table table-hover">
                    <tr><th>ID Mejora</th><th>Titulo</th><th>Tipo</th><th>Estado</th><th>Sección</th><th>Usuario</th><th>Fecha Solicitud</th><th>Acción</th></tr>
                    <?php
                    if ($pendientes) {
                        foreach ($pendientes->result() as $pendiente) { ?>
                        <tr>
                            <td><?=$pendiente->id_pre_mejoramiento;?></td>
                            <td><?=$pendiente->pre_mejoramiento_titulo;?></td>
                            <td><?=$pendiente->tipo_mejoramiento_descripcion;?></td>
                            <td><?=$pendiente->tipo_aprobacion_descripcion;?></td>
                            <td><?=$pendiente->seccion_usuario_descripcion; ?></td>
                            <td><?=$pendiente->usuario_nombre;?> <?=$pendiente->usuario_apellido;?></td>
                            <td><?=$pendiente->aprobacion_date;?></td>
                            <td><a href="<?=base_url();?>evaluador/detalle/<?=$pendiente->id_pre_mejoramiento; ?>/<?= $indicador; ?>" class="btn btn-sm btn-primary">Detalles</a></td>
                            <td><a href="<?=base_url();?>evaluador/aprobar/<?=$pendiente->id_pre_mejoramiento; ?>/1" class="btn btn-sm btn-success btn-success">Aprobar</a></td>
                            <td><a href="<?=base_url();?>evaluador/negar/<?=$pendiente->id_pre_mejoramiento; ?>/0" class="btn btn-sm btn-success btn-danger">No Aprobar</a></td></tr>
                        </tr>
                    <?php } 
                    }?>                   
                </table>
            </div>
        <?php }elseif ($tipo_id==12){//EVALUADOR PENDIENTES GK ####################################?>
            
            <legend><?=$tipo?></legend>
            <?php //echo $tipo_id; ?>
            
            <div class="col-lg-12">
                <table class="table table-hover">
                    <tr><th>ID Mejora</th><th>Titulo</th><th>Tipo</th><th>Sección</th><th>Usuario</th><th>Fecha Solicitud</th><th>Acción</th></tr>
                    <?php
                     if ($pendientes) {
                        foreach ($pendientes->result() as $pendiente) { ?>
                        <tr>
                            <td><?=$pendiente->id_pre_mejoramiento;?></td>
                            <td><?=$pendiente->pre_mejoramiento_titulo;?></td>
                            <td><?=$pendiente->tipo_mejoramiento_descripcion;?></td>
                            <td><?=$pendiente->seccion_usuario_descripcion; ?></td>
                            <td><?=$pendiente->usuario_nombre;?> <?=$pendiente->usuario_apellido;?></td>
                            <td><?=$pendiente->aprobacion_date;?></td>
                            <td><a href="<?=base_url();?>evaluador/detalle/<?=$pendiente->id_pre_mejoramiento; ?>/<?= $indicador; ?>" class="btn btn-sm btn-primary">Detalles</a> 
                                <a href="<?=base_url();?>evaluador/aprobar/<?=$pendiente->id_pre_mejoramiento; ?>" class="btn btn-sm btn-success btn-success">Aprobar</a> 
                                <a href="<?=base_url();?>evaluador/negar/<?=$pendiente->id_pre_mejoramiento; ?>" class="btn btn-sm btn-success btn-danger">No Aprobar</a></td></tr>
                        </tr>
                    <?php } 
                    } ?>
                </table>
            </div>

        <?php }elseif ($tipo_id==13) {// EVALUADOR PENDIENTES OTROS ####################################?>
            <legend><?=$tipo?></legend>

            <div class="col-lg-12">
                <table class="table table-hover">
                    <tr><th>ID</th><th>Titulo</th><th>Tipo</th><th>Fecha Registro</th><th>Clase</th><th>Acción</th></tr>
               <?php
                   if ($pendientes) {
                       foreach ($pendientes->result() as $pendiente) { ?>
                        <tr>
                            <td><?=$pendiente->id_pre_mejoramiento;?></td>
                            <td><?=$pendiente->pre_mejoramiento_titulo;?></td>
                            <td><?=$pendiente->tipo_mejoramiento_descripcion;?></td>
                            <td><?=$pendiente->seccion_usuario_descripcion; ?></td>
                            <td><?=$pendiente->usuario_nombre;?> <?=$pendiente->usuario_apellido;?></td>
                            <td><?=$pendiente->aprobacion_date;?></td>
                            <td><a href="<?=base_url();?>evaluador/detalle/<?=$pendiente->id_pre_mejoramiento; ?>/<?= $indicador; ?>" class="btn btn-sm btn-primary">Detalles</a> 
                                <a href="<?=base_url();?>evaluador/aprobar/<?=$pendiente->id_pre_mejoramiento; ?>" class="btn btn-sm btn-success btn-success">Aprobar</a> 
                                <a href="<?=base_url();?>evaluador/negar/<?=$pendiente->id_pre_mejoramiento; ?>" class="btn btn-sm btn-success btn-danger">No Aprobar</a></td></tr>
                        </tr>
                    <?php } 
                   }
                ?>
                </table>
            </div>
        <?php }elseif ($tipo_id==21) {// EVALUADOR TRANSITO SUGERENCIA ####################################?>
            <legend><?=$tipo?></legend>

            <div class="col-lg-12">
                <table class="table table-hover">
                    <tr><th>ID</th><th>Titulo</th><th>Tipo</th><th>Sección</th><th>Estado</th><th>Usuario</th><th>Ultima Actualización</th><th colspan="2" class="text-center">Acción</th></tr>
               <?php
                   if ($pendientes) {
                       foreach ($pendientes->result() as $pendiente) { 
                        if ($pendiente->tipo_aprobacion_descripcion=="F2") { //MOSTRAR BOTON DE SOLICITAR EVALUACION SOLO SI ESTA PARA APROBACION DE IMPLEMENTACION JEFE IJ
                                $solicita_evaluacion="";
                            }elseif ($pendiente->tipo_aprobacion_descripcion=="IL") {
                                $solicita_evaluacion="
                                <td><a href=\"".base_url()."evaluador/aprobar/".$pendiente->id_pre_mejoramiento."/imp\" class=\"btn btn-sm btn-success btn-success\">Aprobar</a></td> 
                                <td><a href=\"".base_url()."evaluador/negar/".$pendiente->id_pre_mejoramiento."/1\" class=\"btn btn-sm btn-success btn-danger\">No Aprobar</a></td>";
                            }else{
                                $solicita_evaluacion="";
                            }
                        ?>
                        <tr>
                            <td><?=$pendiente->id_pre_mejoramiento;?></td>
                            <td><?=$pendiente->pre_mejoramiento_titulo;?></td>
                            <td><?=$pendiente->tipo_mejoramiento_descripcion;?></td>
                            <td><?=$pendiente->seccion_usuario_descripcion; ?></td>
                            <td><?=$pendiente->tipo_aprobacion_descripcion;?></td>
                            <td><?=$pendiente->usuario_nombre;?> <?=$pendiente->usuario_apellido;?></td>
                            <td><?=$pendiente->aprobacion_date;?></td>
                            <td><a href="<?=base_url();?>evaluador/detalle/<?=$pendiente->id_pre_mejoramiento; ?>/<?= $indicador; ?>" class="btn btn-sm btn-primary">Detalles</a></td> 
                            <?php 

                            if (isset($pendiente->evaluacion_total) && ($pendiente->evaluacion_total > 0)){ ?>
                                <td class="text-center"><a href="<?=base_url();?>ver_mejora.php?tipo_id=14&id=<?= $pendiente->id_pre_mejoramiento;?>" data-toggle="modal" data-target="#detalle_evaluacion" class="btn btn-sm btn-success">Calificación</a> </td>
                            <?php }elseif ($pendiente->tipo_aprobacion_descripcion=="NG") { ?>
                                <td class="text-center"><a href="#" class="btn btn-sm btn-danger">Rechazado</a> </td>
                            <?php }else{ ?>
                                
                            <?php } ?>
                            <?= $solicita_evaluacion;?>
                        </tr>
                    <?php } 
                   }
                ?>
                </table>

                <?php if (isset($evaluacion)) { ?>
                <!-- ############################# SECCION DE MODAL ############################## -->
                <div class="modal fade " id="detalle_evaluacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">

                      <div class="modal-header"><!--ENCABEZADO DE MODAL-->
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Resultados de Evaluación</h4>
                      </div>

                      <div class="modal-body row"><!--CONTENIDO DE MODAL-->
                        <div class="col-md-12">

                            <i>Calificación se realiza con un puntaje de 1 a 15, siendo 1 el de Menor peso y 15 el de Mayor.</i>
                            <table class="table table-hover">
                                <tr><th>ID</th><th>Titulo</th><th>Tipo</th><th>Concepción</th><th>Método</th><th>Estandarización</th><th>Esfuerzo</th><th>Efecto</th><th>Total</th></tr>
                                <?php 
                                /*$id=$this->input->get('id');
                                echo "$id";
                                if (!isset($id)) {
                                    echo "<br>Sin información de Evaluación.";
                                }else{*/
                            
                                foreach ($evaluacion->result() as $resultado){ 
                                //if ($resultado->id_pre_mejoramiento==$id) { ?>
                                    <tr>
                                    <td><?= $resultado->id_pre_mejoramiento;?></td>
                                    <td><?= $resultado->pre_mejoramiento_titulo;?></td>
                                    <td><?= $resultado->tipo_mejoramiento_descripcion;?></td>
                                    <td><?= $resultado->evaluacion_concepcion; ?></td>
                                    <td><?= $resultado->evaluacion_metodo; ?></td>
                                    <td><?= $resultado->evaluacion_estandarizacion; ?></td>
                                    <td><?= $resultado->evaluacion_esfuerzo; ?></td>
                                    <td><?= $resultado->evaluacion_efecto; ?></td>
                                    <td><b><?= $resultado->evaluacion_total; ?></b></td>
                                </tr> <?php
                                //}
                                }
                            //} ?>
                        </table>
                      
                        </div>
                      </div>

                      <div class="modal-footer"><!--PIE DE PAGINA DE MODAL-->
                        <button type="button" class="btn btn-success btn-sm" data-dismiss="modal">Cerrar</button>
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div>
                  <?php } ?>
                <!-- ################################## FIN DE SECCION MODAL ############################## -->
            </div>
        <?php }elseif ($tipo_id==22) {// EVALUADOR TRANSITO GK ####################################?>
            <legend><?=$tipo?></legend>

            <div class="col-lg-12">
                <table class="table table-hover">
                    <tr><th>ID</th><th>Titulo</th><th>Tipo</th><th>Fecha Registro</th><th>Clase</th><th>Acción</th></tr>
               <?php
                   if ($pendientes) {
                       foreach ($pendientes->result() as $pendiente) { ?>
                        <tr>
                            <td><?=$pendiente->id_pre_mejoramiento;?></td>
                            <td><?=$pendiente->pre_mejoramiento_titulo;?></td>
                            <td><?=$pendiente->tipo_mejoramiento_descripcion;?></td>
                            <td><?=$pendiente->seccion_usuario_descripcion; ?></td>
                            <td><?=$pendiente->usuario_nombre;?> <?=$pendiente->usuario_apellido;?></td>
                            <td><?=$pendiente->aprobacion_date;?></td>
                            <td><a href="<?=base_url();?>evaluador/detalle/<?=$pendiente->id_pre_mejoramiento; ?>/<?= $indicador; ?>" class="btn btn-sm btn-primary">Detalles</a> 
                                <a href="<?=base_url();?>evaluador/aprobar/<?=$pendiente->id_pre_mejoramiento; ?>" class="btn btn-sm btn-success btn-success">Aprobar</a> 
                                <a href="<?=base_url();?>evaluador/negar/<?=$pendiente->id_pre_mejoramiento; ?>" class="btn btn-sm btn-success btn-danger">No Aprobar</a></td></tr>
                        </tr>
                    <?php } 
                   }
                ?>
                </table>
            </div>
        <?php }elseif ($tipo_id==23) {// EVALUADOR TRANSITO OTROS ####################################?>
            <legend><?=$tipo?></legend>

            <div class="col-lg-12">
                <table class="table table-hover">
                    <tr><th>ID</th><th>Titulo</th><th>Tipo</th><th>Fecha Registro</th><th>Clase</th><th>Acción</th></tr>
               <?php
                   if ($pendientes) {
                       foreach ($pendientes->result() as $pendiente) { ?>
                        <tr>
                            <td><?=$pendiente->id_pre_mejoramiento;?></td>
                            <td><?=$pendiente->pre_mejoramiento_titulo;?></td>
                            <td><?=$pendiente->tipo_mejoramiento_descripcion;?></td>
                            <td><?=$pendiente->seccion_usuario_descripcion; ?></td>
                            <td><?=$pendiente->usuario_nombre;?> <?=$pendiente->usuario_apellido;?></td>
                            <td><?=$pendiente->aprobacion_date;?></td>
                            <td><a href="<?=base_url();?>evaluador/detalle/<?=$pendiente->id_pre_mejoramiento; ?>/<?= $indicador; ?>" class="btn btn-sm btn-primary">Detalles</a> 
                                <a href="<?=base_url();?>evaluador/aprobar/<?=$pendiente->id_pre_mejoramiento; ?>" class="btn btn-sm btn-success btn-success">Aprobar</a> 
                                <a href="<?=base_url();?>evaluador/negar/<?=$pendiente->id_pre_mejoramiento; ?>" class="btn btn-sm btn-success btn-danger">No Aprobar</a></td></tr>
                        </tr>
                    <?php } 
                   }
                ?>
                </table>
            </div>
        <?php }elseif ($tipo_id==24) {// EVALUADOR FINALIZADAS Y NEGADAS####################################?>
            <legend><?=$tipo?></legend>


            
            <div class="col-lg-12">
                <table class="table table-hover">
                <tr><th>ID</th><th>Titulo</th><th>Tipo</th><th>Inicio</th><th>Finalización</th><th>Evaluación</th><th class="text-center">Calificación</th><th colspan="3" class="text-center">Acción</th></tr>
                    
                <?php 
                if ($pendientes) {
                    foreach ($pendientes->result() as $pendiente) { ?>
                        <tr>
                            <td><?=$pendiente->id_pre_mejoramiento;?></td>
                            <td><?=$pendiente->pre_mejoramiento_titulo;?></td>
                            <td><?=$pendiente->tipo_mejoramiento_descripcion;?></td>
                            <td><?=$pendiente->pre_mejoramiento_date;?></td>
                            <td><?=$pendiente->aprobacion_date; ?></td>
                            <?php if ($pendiente->tipo_aprobacion_descripcion=="NG"){ ?>
                                <td class="bg-danger">RECHAZADO</td>
                            <?php }elseif ($pendiente->tipo_aprobacion_descripcion=="FN") { ?>
                                <td class="bg-success">FINALIZADO</td>
                            <?php }  
                            
                            if ($pendiente->evaluacion_total >0){ ?>
                                <td class="text-center"><?=$pendiente->evaluacion_total; ?> Pts</td>
                                <td class="text-center"><a href="<?=base_url();?>evaluador/detalle/<?=$pendiente->id_pre_mejoramiento;?>/<?= $indicador; ?>" class="btn btn-sm btn-primary">Detalles</a> </td>
                                <td class="text-center"><a href="<?=base_url();?>ver_mejora.php?tipo_id=14&id=<?= $pendiente->id_pre_mejoramiento;?>" data-toggle="modal" data-target="#detalle_evaluacion" class="btn btn-sm btn-success">Calificación</a> </td>
                            <?php }else { ?> 
                                <td class="text-center">0 Pts</td> 
                                <td class="text-center"><a href="<?=base_url();?>evaluador/detalle/<?=$pendiente->id_pre_mejoramiento;?>/<?= $indicador; ?>" class="btn btn-sm btn-primary">Detalles</a> </td>
                            <?php } ?>
                        </tr>
                    <?php }
                } ?>
                </table>
            </div>

            <?php if (isset($evaluacion)) { ?>
            <!-- ############################# SECCION DE MODAL ############################## -->
            <div class="modal fade " id="detalle_evaluacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">

                  <div class="modal-header"><!--ENCABEZADO DE MODAL-->
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Resultados de Evaluación</h4>
                  </div>

                  <div class="modal-body row"><!--CONTENIDO DE MODAL-->
                    <div class="col-md-12">

                        <i>Calificación se realiza con un puntaje de 1 a 15, siendo 1 el de Menor peso y 15 el de Mayor.</i>
                        <table class="table table-hover">
                            <tr><th>ID</th><th>Titulo</th><th>Tipo</th><th>Concepción</th><th>Método</th><th>Estandarización</th><th>Esfuerzo</th><th>Efecto</th><th>Total</th></tr>
                            <?php 
                            /*$id=$this->input->get('id');
                            echo "$id";
                            if (!isset($id)) {
                                echo "<br>Sin información de Evaluación.";
                            }else{*/
                        
                            foreach ($evaluacion->result() as $resultado){ 
                            //if ($resultado->id_pre_mejoramiento==$id) { ?>
                                <tr>
                                <td><?= $resultado->id_pre_mejoramiento;?></td>
                                <td><?= $resultado->pre_mejoramiento_titulo;?></td>
                                <td><?= $resultado->tipo_mejoramiento_descripcion;?></td>
                                <td><?= $resultado->evaluacion_concepcion; ?></td>
                                <td><?= $resultado->evaluacion_metodo; ?></td>
                                <td><?= $resultado->evaluacion_estandarizacion; ?></td>
                                <td><?= $resultado->evaluacion_esfuerzo; ?></td>
                                <td><?= $resultado->evaluacion_efecto; ?></td>
                                <td><b><?= $resultado->evaluacion_total; ?></b></td>
                            </tr> <?php
                            //}
                            }
                        //} ?>
                    </table>
                  
                    </div>
                  </div>

                  <div class="modal-footer"><!--PIE DE PAGINA DE MODAL-->
                    <button type="button" class="btn btn-success btn-sm" data-dismiss="modal">Cerrar</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div>
              <?php } ?>
            <!-- ################################## FIN DE SECCION MODAL ############################## -->
        <?php } ?>
        
    </div>
   <?php }
}else{
    redirect('main/login','refresh');
    //diesel 
} ?>

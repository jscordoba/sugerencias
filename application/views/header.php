<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* End of file header.php */
/* Location: ./application/views/header.php */
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Mejoramiento Online</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?=base_url();?>styles/css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom CSS -->
    <link href="<?=base_url();?>styles/css/blog-post.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <?php 
    if ($this->session->userdata('login')){ 

        $tipo=$this->session->userdata('tipo');
        if ($tipo==1 || $tipo==4) {
            $tipo="Proponente";
            $tipo_aprobacion="proponente/";
        }elseif ($tipo==2) {
            $tipo="Evaluador";
            $tipo_aprobacion="evaluador/";
        }elseif ($tipo==3) {
            $tipo="Administrador";
            $tipo_aprobacion="administrador/";
        }
        ?>
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?=base_url();?><?=$tipo_aprobacion;?>">Mejoramiento Magnetron</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="#">Perfil: <?=$tipo; ?></a>
                        </li>
                        <?php if ($this->session->userdata('tipo')==1 || $this->session->userdata('tipo')==4) { ?>
                        <!-- <li>
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Solicitudes <b class="caret"></b></a>
                              <ul class="dropdown-menu">
                                <li><a href="<?=base_url();?>proponente/pre_solicitud/1">Nueva Sugerencia</a></li>
                                <li class="divider"></li>
                                <li><a href="<?=base_url();?>proponente/pre_solicitud/2">Nuevo Grupo Kaizen</a></li>
                                <li class="divider"></li>
                                <li><a href="<?=base_url();?>proponente/pre_solicitud/3">Otro</a></li>
                              </ul>
                            </li>
                        </li> -->
                        <?php }elseif ($this->session->userdata('tipo')==2) {
                            ?> <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reportes <b class="caret"></b></a>
                              <ul class="dropdown-menu">
                                <li><a href="<?=base_url();?>evaluador/reporte_estado">Estado Mejoramientos</a></li>
                                <li class="divider"></li>
                                <li><a href="<?=base_url();?>evaluador/reporte_estado1">Otro</a></li>
                              </ul>
                            </li> <?php
                        }elseif ($this->session->userdata('tipo')==3) {
                            ?> <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Usuarios <b class="caret"></b></a>
                              <ul class="dropdown-menu">
                                <li><a href="<?=base_url();?>administrador/nuevo_user">Nuevo</a></li>
                              </ul>
                            </li> <?php
                        } ?>
                        <li>
                            <a href="<?=base_url();?>main/logout">Salir</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>
    <?php }else{ ?>
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?= base_url();?>">Mejoramiento Magnetron</a>
                </div>
            </div>
            <!-- /.container -->
        </nav>
    <?php } ?>
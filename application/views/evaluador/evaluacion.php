<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* End of file pre_solicitud.php */
/* Location: ./application/views/proponente/pre_solicitud.php */
?>

<div class="container">
    <div class="row">        
        <?php if ($aprobar==1) { ?>
        	<div class="col-lg-12">
	        	<form action="<?= base_url();?>evaluador/evaluacion" method="POST">
	        		<fieldset>

	        		<legend>Evaluacion de Mejoramiento Número <b><?= $mejoramiento;?></b></legend>

		            <div class="form-group col-md-12">
	            		<label class="col-md-4 control-label" for="concepcion">Concepción</label>
					  	<div class="col-md-3">
						    <select id="concepcion" name="concepcion" class="form-control" required>
						      <option value="">Seleccione uno..</option>
						      <option value="1">1</option>
						      <option value="2">2</option>
						      <option value="3">3</option>
						      <option value="4">4</option>
						      <option value="5">5</option>
						      <option value="6">6</option>
						      <option value="7">7</option>
						      <option value="8">8</option>
						      <option value="9">9</option>
						      <option value="10">10</option>
						      <option value="11">11</option>
						      <option value="12">12</option>
						      <option value="13">13</option>
						      <option value="14">14</option>
						      <option value="15">15</option>
						    </select>
					  	</div>
					</div>

					<div class="form-group col-md-12">
					  <label class="col-md-4 control-label" for="metodo">Método</label>
					  <div class="col-md-3">
					    <select id="metodo" name="metodo" class="form-control" required>
					      <option value="">Seleccione uno..</option>
					      <option value="1">1</option>
					      <option value="2">2</option>
					      <option value="3">3</option>
					      <option value="4">4</option>
					      <option value="5">5</option>
					      <option value="6">6</option>
					      <option value="7">7</option>
					      <option value="8">8</option>
					      <option value="9">9</option>
					      <option value="10">10</option>
					      <option value="11">11</option>
					      <option value="12">12</option>
					      <option value="13">13</option>
					      <option value="14">14</option>
					      <option value="15">15</option>
					    </select>
				  </div>
				</div>

				<div class="form-group col-md-12">
				  <label class="col-md-4 control-label" for="estandarizacion">Estandarización</label>
				  <div class="col-md-3">
				    <select id="estandarizacion" name="estandarizacion" class="form-control" required>
					      <option value="">Seleccione uno..</option>
					      <option value="1">1</option>
					      <option value="2">2</option>
					      <option value="3">3</option>
					      <option value="4">4</option>
					      <option value="5">5</option>
					      <option value="6">6</option>
					      <option value="7">7</option>
					      <option value="8">8</option>
					      <option value="9">9</option>
					      <option value="10">10</option>
					      <option value="11">11</option>
					      <option value="12">12</option>
					      <option value="13">13</option>
					      <option value="14">14</option>
					      <option value="15">15</option>
					    </select>
				  </div>
				</div>

				<div class="form-group col-md-12">
				  <label class="col-md-4 control-label" for="esfuerzo">Esfuerzo</label>
				  <div class="col-md-3">
				    <select id="esfuerzo" name="esfuerzo" class="form-control" required>
					      <option value="">Seleccione uno..</option>
					      <option value="1">1</option>
					      <option value="2">2</option>
					      <option value="3">3</option>
					      <option value="4">4</option>
					      <option value="5">5</option>
					      <option value="6">6</option>
					      <option value="7">7</option>
					      <option value="8">8</option>
					      <option value="9">9</option>
					      <option value="10">10</option>
					      <option value="11">11</option>
					      <option value="12">12</option>
					      <option value="13">13</option>
					      <option value="14">14</option>
					      <option value="15">15</option>
					    </select>
				  </div>
				</div>

				<div class="form-group col-md-12">
				  <label class="col-md-4 control-label" for="efecto">Efecto</label>
				  <div class="col-md-3">
				    <select id="efecto" name="efecto" class="form-control" required>
					      <option value="">Seleccione uno..</option>
					      <option value="1">1</option>
					      <option value="2">2</option>
					      <option value="3">3</option>
					      <option value="4">4</option>
					      <option value="5">5</option>
					      <option value="6">6</option>
					      <option value="7">7</option>
					      <option value="8">8</option>
					      <option value="9">9</option>
					      <option value="10">10</option>
					      <option value="11">11</option>
					      <option value="12">12</option>
					      <option value="13">13</option>
					      <option value="14">14</option>
					      <option value="15">15</option>
					    </select>
					  </div>
					</div>

		            <!-- Appended Input-->
					<div class="form-group">
						<input type="hidden" name="id_mejoramiento" value="<?= $mejoramiento;?>">

						<label class="col-md-4 control-label" for="comentario">Comentarios</label>
						<div class="col-md-8">
						    <textarea class="form-control" id="comentario" name="comentario" placeholder="Observaciones adicionales referente a la evaluación realizada."></textarea>
						  <p class="help-block">*Max. 500 caracteres.</p>
						</div>
					</div>

					<!-- Button -->
					<div class="form-group">
					  <div class="col-md-4 col-md-offset-4">
					    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Guardar</button>
					  </div>
					</div>

					</fieldset>
				</form>
	        </div><?php
        } ?>
        
	</div>
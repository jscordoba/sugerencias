<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* End of file pre_solicitud.php */
/* Location: ./application/views/proponente/pre_solicitud.php */
?>

<div class="container">
    <div class="row">        
        <?php if ($aprobar==1) { ?>
        <legend>Aprobar Mejoramiento ID <b><?= $mejoramiento;?></b></legend>
        	<div class="col-lg-12">
	        	<form action="<?= base_url();?>evaluador/aprobar/0/2" method="POST">
		            <!-- Appended Input-->
					<div class="form-group">
						<input type="hidden" name="id_mejoramiento" value="<?= $mejoramiento;?>">
					  <label class="col-md-4 control-label" for="titulo_mejoramiento">Comentarios</label>
					  <div class="col-md-8">
					      <textarea class="form-control" id="comentario_aprobacion" name="comentario_aprobacion" placeholder="default text"></textarea>
					    <p class="help-block">*Max. 500 caracteres.</p>
					  </div>
					</div>

					<!-- Button -->
					<div class="form-group">
					  <div class="col-md-4 col-md-offset-4">
					    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Guardar</button>
					  </div>
					</div>
				</form>
	        </div><?php
        }elseif ($aprobar==2) { ?>
        <legend>Negar Mejoramiento ID <b><?= $mejoramiento; ?></b></legend>
        	<div class="col-lg-12">
	        	<form action="<?= base_url();?>evaluador/negar/<?= $mejoramiento; ?>/2" method="POST">
		            <!-- Appended Input-->
					<div class="form-group">
					  <label class="col-md-4 control-label" for="comentario_negar">Comentarios</label>
					  <div class="col-md-8">
					      <textarea class="form-control" id="comentario_negar" name="comentario_negar" placeholder="default text"></textarea>
					    <p class="help-block">*Max. 500 caracteres.</p>
					  </div>
					</div>

					<!-- Button -->
					<div class="form-group">
					  <div class="col-md-4 col-md-offset-4">
					    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Guardar</button>
					  </div>
					</div>
				</form>
	        </div><?php
        } ?>
        
	</div>
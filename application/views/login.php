<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* End of file registro.php */
/* Location: ./application/views/registro.php */

?>

<?php if ($this->session->userdata('login')){ 
		if ($this->session->userdata('tipo')==1) {//proponente
			redirect('proponente/','refresh');
		}elseif ($this->session->userdata('tipo')==2) {//evaluador
			redirect('evaluador/','refresh');
		}elseif ($this->session->userdata('tipo')==3) {//administrador	
			redirect('administrador/','refresh');
		}elseif ($this->session->userdata('tipo')==4) {//administrador	
			redirect('proponente/','refresh');
		}
	//redirect(base_url().'','refresh');
}else{?>

<html>
<head>
	<title>Iniciar - Mejoramiento</title>
</head>
<body>

 <!-- Page Content -->
<div class="container">
	<div class="row">
		<!-- <div class="col-md-4">
			<img src="<?= base_url() .  'image/Positivo.jpg' ?>" alt="Megueito Plus">
			<?php echo base_url() .  'image/Positivo.jpg'; ?>
		</div> -->
		<div class="col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">
                
		    <div class="well">
		    	<form action="<?=base_url();?>main/login" method="POST">
			        <h4>Acceso</h4>
			        	<label class="col-md-4 control-label" for="log_user">Usuario</label>
			        	<input id="log_user" name="log_user" class="form-control" placeholder="Usuario" type="text">

			        	<label class="col-md-4 control-label" for="log_clave">Clave</label>
			        	<input id="log_clave" name="log_clave" class="form-control" placeholder="Clave" type="password">
			        <!-- /.input-group -->
			        <br>
			        <div class="text-center">
			        	<button class="btn btn-primary btn-success">Iniciar <span class="glyphicon glyphicon-share"></span></button>	
			        </div>
		        </form>
		    </div>
	    
		</div>
	</div>
<?php } ?>

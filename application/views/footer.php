<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* End of file footer.php */
/* Location: ./application/views/footer.php */
?>
        <hr>


        <!-- Footer -->
        <footer>
            <div class="row text-center">
                <div class="col-lg-12">
                    <p><br><b>Copyright &copy; Industrias Electromecanicas Magnetron S.A.S - TIC's 2016</b></p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

	 <!-- jQuery -->
    <script src="<?=base_url();?>/styles/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=base_url();?>/styles/js/bootstrap.min.js"></script>

    <script src="<?=base_url();?>/styles/js/functions.js"></script>

</body>

</html>
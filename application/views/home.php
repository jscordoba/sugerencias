
<?php 
if ($this->session->userdata('login')){ 
    if ($this->session->userdata('tipo')==1) {//USUARIO TIPO PROPONENTE
    ?>
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-12">

                <?php 
                $jefe="";

                if ($this->session->userdata('jefe_area')) {
                    $jefe=", Jefe de <b>".$this->session->userdata('jefe_area')."</b>";
                }
                ?>
                <!-- Title -->
                <h1>Solicitudes de Mejoramiento</h1>
                <!-- Author -->
                <p class="lead">de <a href="#"><?=$this->session->userdata('nombre'); ?></a><?= $jefe; ?></p>
                <hr>
                <!-- Date/Time -->
                <p><span class="glyphicon glyphicon-time"></span> <?=strftime("%A %d de %B de %Y , ").date("H:i:s A");?></p>
                <hr>

                <div class="col-md-4 col-lg-4">
                    <a href="<?=base_url();?>proponente/pre_solicitud/1">
                        <button class="btn btn-primary btn-lg form-control" style="height: 9.5em;">
                            <b>Nueva Sugerencia </b><span class="glyphicon glyphicon-thumbs-up"></span>
                        </button>
                    </a>
                </div>
               
                
                <?php 
                if ($this->session->userdata('jefe')) { ?>
                <div class="col-md-4 col-lg-4">
                    <div class="well">
                        <h4>Pendiente Aprobar</h4>
                        <table class="table-hover" width="100%">
                            <tr><td><label for="">Presentación:</label></td><td><a href="<?=base_url();?>proponente/ver_mejora/31"><?= $num_presentacion; ?></a></td></tr>
                            <tr><td><label for="">Implementación:</label></td><td><a href="<?=base_url();?>proponente/ver_mejora/32"><?= $num_implementacion; ?></a></td></tr>
                        </table>
                        <!-- /.input-group -->
                    </div>
                </div>
                <?php }
                 ?>
                <div class="col-md-4 col-lg-4">
                    <div class="well">
                        <h4>Sugerencias</h4>
                        <table class="table-hover" width="100%">
                            <tr><td><label for="">Registrados:</label></td><td><a href="<?=base_url();?>proponente/ver_mejora/11"><?= $num_registrado_sg; ?></a></td></tr>
                            <tr><td><label for="">Activos:</label></td><td><a href="<?=base_url();?>proponente/ver_mejora/13"><?= $num_activo_sg; ?></a></td></tr>
                            <tr><td><label for="">Finalizados:</label></td><td><a href="<?=base_url();?>proponente/ver_mejora/14"><?= $num_finalizado_sg; ?></a></td></tr>
                            <tr><td><label for="">Negados:</label></td><td><a href="<?=base_url();?>proponente/ver_mejora/15"><?= $num_negado_sg; ?></a></td></tr>
                        </table>
                        <!-- /.input-group -->
                    </div>
                </div>
                <!-- <div class="col-md-4 col-lg-4">
                    <div class="well">
                        <h4>Grupos Kaizen</h4>
                        <table class="table-hover" width="100%">
                            <tr><td><label for="">Registrados:</label></td><td><a href="<?=base_url();?>proponente/ver_mejora/21"><?= $num_registrado_gk; ?></a></td></tr>
                            <tr><td><label for="">Activos:</label></td><td><a href="<?=base_url();?>proponente/ver_mejora/23"><?= $num_activo_gk; ?></a></td></tr>
                            <tr><td><label for="">Finalizados:</label></td><td><a href="<?=base_url();?>proponente/ver_mejora/24"><?= $num_finalizado_gk; ?></a></td></tr>
                            <tr><td></td></tr>
                        </table>  
                    </div>
                </div> -->

                <div class="col-md-4 col-lg-4">
                    <div class="well">
                        <form class="form-horizontal" method="POST" action="<?=base_url();?>proponente/search" enctype="multipart/form-data">
                            <h4>Consultas</h4>
                            <div class="input-group">
                                <input type="text" class="form-control" name="busca_mejoramiento_id" placeholder="ID Sugerencia /Nombre de usuario">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <span class="glyphicon glyphicon-search"></span>
                                </button>
                                </span>
                            </div>
                            <!-- /.input-group -->
                        </form>
                    </div>
                </div>
            </div>
            

            <!-- Blog Sidebar Widgets Column -->

        </div>

            <div class="col-lg-4 small">
                <h4 class="text-center">Clasificación</h4>
                <p><b>Registrados:</b> La mejora a sido ingresada pero no a sido enviada para revisión del Jefe Directo y el Jefe del Área.</p>
                <p><b>Activos:</b> La mejora se encuentra registrada y se le puede realizar seguimiento de sus diferentes estados.</p>
                <p><b>Finalizados:</b> La mejora se dió por finalizada y permite ver los comentarios con los cuales fue cerrada.</p>
            </div>
<?php }elseif ($this->session->userdata('tipo')==2) {//USUARIO TIPO EVALUADOR?>
    <div class="container">

    <div class="row">

        <!-- Blog Post Content Column -->
        <div class="col-lg-12">

            <!-- Title -->
            <h1>Solicitudes de Mejoramiento</h1>
            <!-- Author -->
            <p class="lead">de <a href="#"><?=$this->session->userdata('nombre'); ?></a></p>
            <hr>
            <!-- Date/Time -->
            <p><span class="glyphicon glyphicon-time"></span> <?=strftime("%A %d de %B de %Y , ").date("H:i:s A");?></p>
            <hr>
           
            <div class="col-lg-4">
                
                <div class="well">
                    <h4>Solicitudes Pendientes</h4>
                    <table class="table-hover" width="100%">
                        <tr><td><label for="">Sugerencias:</label></td><td><a href="<?=base_url();?>evaluador/ver_mejora/11"><?= $num_pendiente_sg; ?></a></td></tr>
                        <tr><td><label for="">Grupos Kaizen:</label></td><td><a href="<?=base_url();?>evaluador/ver_mejora/12"><?= $num_pendiente_gk; ?></a></td></tr>
                        <tr><td><label for="">Otros:</label></td><td><a href="<?=base_url();?>evaluador/ver_mejora/13"><?= $num_pendiente_ot; ?></a></td></tr>
                    </table>
                    <!-- /.input-group -->
                </div>
                
            </div>
            <div class="col-lg-4">
                    <div class="well">
                        <h4>Solicitudes en Transito</h4>
                        <table class="table-hover" width="100%">
                            <tr><td><label for="">Sugerencias:</label></td><td><a href="<?=base_url();?>evaluador/ver_mejora/21"><?= $transito_sg; ?></a></td></tr>
                            <tr><td><label for="">Grupos Kaizen:</label></td><td><a href="<?=base_url();?>evaluador/ver_mejora/22"><?= $transito_gk; ?></a></td></tr>
                            <tr><td><label for="">Otros:</label></td><td><a href="<?=base_url();?>evaluador/ver_mejora/23"><?= $transito_ot; ?></a></td></tr>
                            <tr><td><label for="">Finalizadas:</label></td><td><a href="<?=base_url();?>evaluador/ver_mejora/24"><?= $transito_fn; ?></a></td></tr>
                        </table>  
                    </div>
                <!-- /.input-group -->
            </div>
            <div class="col-lg-4">
                <div class="well">
                    <form class="form-horizontal" method="POST" action="<?=base_url();?>evaluador/search" enctype="multipart/form-data">
                        <h4>Consultas</h4>
                        <div class="input-group">
                            <input type="text" class="form-control" name="busca_mejoramiento_id" placeholder="ID Sugerencia /Nombre de usuario">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <span class="glyphicon glyphicon-search"></span>
                            </button>
                            </span>
                        </div>
                        <!-- /.input-group -->
                    </form>
                </div>
            </div>
        </div>

        <!-- Blog Sidebar Widgets Column -->
    </div>

    <div class="col-lg-4 small">
        <h4 class="text-center">Clasificación</h4>
        <p><b>Solicitudes Pendientes:</b> Mejoras registradas por otros usuarios y que han sido asignadas a usted como Evaluador.</p>
        <p><b>Solicitudes en Tránsito:</b> Mejoras que usted como evaluador ha aprobado y se encuentra en etapa de ejecución.</p>
    </div>
<?php }elseif ($this->session->userdata('tipo')==3) {//USUARIO TIPO ADMINISTRADOR?>
    <div class="container">

    <div class="row">

        <!-- Blog Post Content Column -->
        <div class="col-lg-12">

            <!-- Title -->
            <h1>Solicitudes de Mejoramiento</h1>
            <!-- Author -->
            <p class="lead">de <a href="#"><?=$this->session->userdata('nombre'); ?></a></p>
            <hr>
            <!-- Date/Time -->
            <p><span class="glyphicon glyphicon-time"></span> <?=strftime("%A %d de %B de %Y , ").date("H:i:s A");?></p>
            <hr>
           
            <div class="col-lg-4">
                
                <div class="well">
                    <h4>Administrar Usuarios</h4>
                    <table class="table-hover" width="100%">
                        <tr><td><label for="">Proponentes:</label></td><td><a href="<?=base_url();?>administrador/ver_usuarios/1"><?= $num_proponentes;?></a></td></tr>
                        <tr><td><label for="">Jefes de Sección:</label></td><td><a href="<?=base_url();?>administrador/ver_usuarios/3"><?= $num_jefes;?></a></td></tr>
                        <tr><td><label for="">Planta:</label></td><td><a href="<?=base_url();?>administrador/ver_usuarios/4"><?= $num_planta;?></a></td></tr>
                        <tr><td><label for="">Evaluadores:</label></td><td><a href="<?=base_url();?>administrador/ver_usuarios/2"><?= $num_evaluadores;?></a></td></tr>
                    </table>
                    <!-- /.input-group -->
                </div>
                
            </div>
            <div class="col-lg-4">
                    <div class="well">
                        <h4>Administrar Solicitudes</h4>
                        <table class="table-hover" width="100%">
                            <tr><td><label for="">Registradas:</label></td><td><a href="<?=base_url();?>administrador/ver_mejora/6"><?= $num_registrado;?></a></td></tr>
                            <tr><td><label for="">Activas:</label></td><td><a href="<?=base_url();?>administrador/ver_mejora/2"><?= $num_activo;?></a></td></tr>
                            <tr><td><label for="">Finalizadas:</label></td><td><a href="<?=base_url();?>administrador/ver_mejora/7"><?= $num_finalizado;?></a></td></tr>
                        </table>  
                    </div>
                <!-- /.input-group -->
            </div>
            <div class="col-lg-4">
                <div class="well">
                    <form class="form-horizontal" method="POST" action="<?=base_url();?>administrador/search_home" enctype="multipart/form-data">
                        <h4>Consultas</h4>
                        <div class="input-group">
                            <input type="text" class="form-control" name="busca_mejoramiento_id" placeholder="ID Sugerencia /Nombre de usuario">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <span class="glyphicon glyphicon-search"></span>
                            </button>
                            </span>
                        </div>
                        <!-- /.input-group -->
                    </form>
                </div>
            </div>
        </div>

        <!-- Blog Sidebar Widgets Column -->

    </div>
<?php }elseif ($this->session->userdata('tipo')==4) {//USUARIO TIPO PLANTA  ?>
 <div class="container">

    <div class="row">
        <h1>Solicitudes de Mejoramiento</h1>
        <!-- Author -->
        <p class="lead">de <a href="#"><?=$this->session->userdata('nombre'); ?> <?=$this->session->userdata('apellido'); ?></a></p>
        <hr>
        <!-- Date/Time -->
        <p><span class="glyphicon glyphicon-time"></span> <?=strftime("%A %d de %B de %Y , ").date("H:i:s A");?></p>
        <hr>

        <!-- Blog Post Content Column -->
        <div class="col-lg-12">
            <div class="col-md-4 col-lg-4">
                <a href="<?=base_url();?>proponente/pre_solicitud/1">
                    <button class="btn btn-primary btn-lg form-control" style="height: 6.5em;">
                        <b>Nueva Sugerencia </b><span class="glyphicon glyphicon-thumbs-up"></span>
                    </button>
                </a>
            </div>

            <a href="<?=base_url();?>proponente/ver_mejora/41">
                <div class="col-md-4 col-lg-4">
                    <div class="well text-center">
                        <h4>Sugerencias</h4>
                        <li>Ver listado y actualizaciones.</li>
                        <li>Enviar a Aprobación.</li>
                        <!-- /.input-group -->
                    </div>
            
                </div>
            </a>

            <!-- <a href="<?=base_url();?>proponente/ver_mejora/42">
                <div class="col-md-4 col-lg-4">
                    <div class="well text-center">
                        <h4>Grupo Kaizen</h4>
                        <li>Ver listado y actualizaciones.</li>
                        <li>Enviar a Aprobación.</li>
                    </div>
                </div>
            </a>   -->   

            <div class="col-md-4 col-lg-4">
                <div class="well">
                    <form class="form-horizontal" method="POST" action="<?=base_url();?>proponente/search" enctype="multipart/form-data">
                        <h4>Consultas</h4>
                        <div class="input-group">
                            <input type="text" class="form-control" name="busca_mejoramiento_id" placeholder="ID Sugerencia /Nombre de usuario">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <span class="glyphicon glyphicon-search"></span>
                            </button>
                            </span>
                        </div>
                        <!-- /.input-group -->
                    </form>
                </div>
            </div>       
        </div>

    </div> <?php
    }?>

    <div class="col-lg-4 small">
        <h4 class="text-center">Estados de Aprobación</h4>
        <p><b>PDT:</b> Pendiente enviar para Aprobar Presentación.</p>
        <p><b>PJ:</b> Pendiente Aprobar Presentación por el Jefe.</p>
        <p><b>PL:</b> Pendiente Aprobar Presentación por el Lider.</p>
        <p><b>F2:</b> En Desarrollo de Fase 2 o de Implementación.</p>
        <p><b>IJ:</b> Pendiente Aprobar Implementación por el Jefe.</p>
        <p><b>IL:</b> Pendiente Aprobar y Evaluar Implementación por el Lider.</p>
        <p><b>FN:</b> Proceso Finalizado correctamente.</p>
        <p><b>NG:</b> Propuesta Negada o Rechazada.</p>
    </div>
    
<?php 
}else{
    redirect('main/','refresh');
} ?>
    <!-- /.row -->
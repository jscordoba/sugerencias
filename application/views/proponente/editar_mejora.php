<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* End of file pre_solicitud.php */
/* Location: ./application/views/proponente/pre_solicitud.php */
?>

<?php 
if ($this->session->userdata('login')){ 
    if ($this->session->userdata('tipo')==1) {//USUARIO TIPO PROPONENTE
    ?>
 <!-- Page Content -->
<div class="container">

    <div class="row">
      <div class="col-lg-8">
      <form class="form-horizontal" method="POST" action="<?=base_url();?>proponente/editar/up">
        <fieldset>

        <!-- Form Name -->
        <legend>Edición de Sugerencia <?=$detalles->id_pre_mejoramiento; ?></legend>

        <input type="hidden" name="id_pre_mejoramiento" value="<?=$detalles->id_pre_mejoramiento; ?>">

        <!-- Select Basic -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="tipo_mejora">Tipo de Mejoramiento</label>
          <div class="col-md-8">
            <select id="tipo_mejora" name="tipo_mejora" class="form-control">
              <option value="<?= $detalles->tipo_mejoramiento_id; ?>"><?= $detalles->tipo_mejoramiento_descripcion; ?></option>
              <option value="1">Preventivo</option>
              <option value="2">Mejora</option>
            </select>
          </div>
        </div>

        <!-- Appended Input-->
        <div class="form-group">
          <label class="col-md-4 control-label" for="titulo_mejoramiento">Titulo del Mejoramiento</label>
          <div class="col-md-8">
              <input id="titulo_mejoramiento" name="titulo_mejoramiento" value="<?= $detalles->pre_mejoramiento_titulo; ?>" class="form-control" placeholder="placeholder" type="text">
            <p class="help-block">*Ayuda</p>
          </div>
        </div>

        <!-- Appended Input-->
        <!--<div class="form-group">
          <label class="col-md-4 control-label" for="participante">Participantes</label>
          <div class="col-md-8">
            <div class="input-group">
              <input id="participantex" name="participantex" value="$detalles->usuario_nombre;  $detalles->usuario_apellido; " class="form-control" placeholder="placeholder" type="text">
              <span class="input-group-addon"><a href=""><span class="glyphicon glyphicon-plus-sign glyphicon-plus-sign"></span></a></span>
            </div>
            <p class="help-block">*Ayuda</p>
          </div>
        </div>-->

        <!-- Text input-->
        <!--<div class="form-group">
          <label class="col-md-4 control-label" for="fecha_registro">Fecha de Registro</label>  
          <div class="col-md-4">
          <input id="fecha_registro" name="fecha_registro" type="date" placeholder="placeholder" class="form-control input-md" value="ate("Y-m-d")">
          <span class="help-block">*Ayuda</span>  
          </div>
        </div>
      -->

        <!-- Select Basic -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="seccion_id">Sección a Aplicar</label>
          <div class="col-md-8">
            <select id="seccion_id" name="seccion_id" class="form-control">
              <option value="<?= $detalles->seccion_usuario_id; ?>"><?= $detalles->seccion_usuario_descripcion; ?></option>
              <option value="1">NUCLEOS</option>
              <option value="2">TICS</option>
              <option value="3">CALIDAD</option>
            </select>
          </div>
        </div>

        <!-- Textarea -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="antes_mejoramiento">Antes del Mejoramiento</label>
          <div class="col-md-8">                     
            <textarea class="form-control" rows="5" id="antes_mejoramiento" name="antes_mejoramiento"><?= $detalles->sugerencia_antes_mejora; ?></textarea>
          </div>
        </div>

        <!-- Textarea -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="problema">Problemas</label>
          <div class="col-md-8">                     
            <textarea class="form-control" rows="5" id="problema" name="problema"><?= $detalles->sugerencia_problema; ?></textarea>
          </div>
        </div>

        <!-- Textarea -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="efecto_esperado">Efecto esperado de la sugerencia</label>
          <div class="col-md-8">                     
            <textarea class="form-control" rows="5" value="<?= $detalles->sugerencia_esperado; ?>" id="efecto_esperado" name="efecto_esperado"><?= $detalles->sugerencia_esperado; ?></textarea>
          </div>
        </div>

        <!-- Multiple Radios -->
        <!--<div class="form-group">
          <label class="col-md-4 control-label" for="ahorro">¿Genera Ahorro?</label>
          <div class="col-md-8">
            <div class="radio">
            <label for="radios-0">
              <input type="radio" name="ahorro" id="ahorro-0" value="1">
              SI
            </label>
          </div>

            <div class="radio">
            <label for="radios-1">
              <input type="radio" name="ahorro" id="ahorro-1" value="0" checked="checked">
              NO
            </label>
          </div>
          <div class="col-md-12 help-block">
            * (SI) Diligenciar formato para demostración de ahorros. <br>
            * (NO) Continuar.
          </div>
          <div class="col-md-12 help-block">
            <a href="">Formato de gerneración de Ahorro.</a> 
            <input type="file" name="f_ahorro" id="f_ahorro" class="btn btn-sm btn-primary">
          </div>
          </div>
        </div>-->

        <!-- Textarea -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="propuesta">Descripcion o bosquejo de la sugerencia propuesta</label>
          <div class="col-md-8">                     
            <a href="<?=base_url();?>proponente/download_file/<?=$detalles->sugerencia_propuesta; ?>/EV"><?= $detalles->sugerencia_propuesta; ?></a>
          </div>
        </div>

        <!-- Button -->
        <div class="form-group">
          <div class="col-md-4 col-md-offset-4">
            <button id="singlebutton" name="singlebutton" class="btn btn-primary">Guardar</button>
          </div>
        </div>

        </fieldset>
      </form>

    </div>

    <div class="col-lg-4">
        <div class="well">
                <h4>Buscar</h4>
                <div class="input-group">
                    <input type="text" class="form-control">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <span class="glyphicon glyphicon-search"></span>
                    </button>
                    </span>
                </div>
                <!-- /.input-group -->
            </div>
    </div>
    </div>

<?php }
}else{
    redirect('main/login','refresh');
} ?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* End of file pre_solicitud.php */
/* Location: ./application/views/proponente/pre_solicitud.php */
?>


<?php 
if ($this->session->userdata('login')){ 
    if ($this->session->userdata('tipo')==1 || $this->session->userdata('tipo')==4) {//USUARIO TIPO PROPONENTE
    ?>

    <?php
    echo "<script type=\"text/javascript\">
		function quitar_doc(id){
			//alert(\"antes Quitar Doc \"+total_docs);
			$(\"#\"+id).parent().parent().parent().remove();
			total_docs-=1;

			//alert(\"despues Quitar Doc\"+total_docs);
			$('#total_docs').val(total_docs);
		}
		</script>";

	$script="<script type=\"text/javascript\">
			var doc_cont=1;
			var total_docs=1;

			function nuevo_doc(){
				//alert(\"antes Nuevo Doc \"+total_docs);
				doc_cont+=1;
				total_docs+=1;

				//var participanteadd='participante\"+(doc_cont)+\"';

				if (total_docs>2) {
					alert('Solo se permite un máximo de 2 participantes.');
					total_docs-=1;
				}else{
					$('#mas_participantes').append(\"<div class='form-group'><label class='control-label col-xs-4 col-sm-12 col-md-4' for='participante\"+(doc_cont)+\"'>Participante \"+(doc_cont)+\"</label><div class='col-md-8'><div class='input-group'><select id='participante\"+(doc_cont)+\"' name='participante2' class='form-control doc_add' required>";
						//$('#mas_participantes').append(\"<div class='form-group'><label class='control-label col-xs-4 col-sm-12 col-md-4' for='participante\"+(doc_cont)+\"'>Participante \"+(doc_cont)+\"</label><div class='col-md-8'><div class='input-group'><select id='participante\"+(doc_cont)+\"' name='participante\"+(doc_cont)+\"' class='form-control doc_add' required>";

					if ($this->session->userdata('tipo')==1){ 
						$script.="<option value='".$this->session->userdata('id')."'>".$this->session->userdata('nombre')." ".$this->session->userdata('apellido')."</option>";

			    		foreach ($usuarios->result() as $user) {
					  		$script.="<option value='".$user->idusuario."'>".$user->usuario_nombre." ".$user->usuario_apellido."</option>";
					  	} 
					}elseif ($this->session->userdata('tipo')==4) {
			    		$script.="<option value=''>Seleccione uno..</option>";
			    		foreach ($usuarios->result() as $user) {
					  		$script.="<option value='".$user->idusuario."'>".$user->usuario_nombre." ".$user->usuario_apellido."</option>";
					  	} 
			    	} 
			    	
					
					$script.="</select><span class='input-group-addon btn btn-danger' onclick=quitar_doc('participante\"+(doc_cont)+\"')><span class='glyphicon glyphicon-minus-sign'></span></span></div></div></div>\");";
						
					
					$script.="$('#total_docs').val(total_docs);";
					$script.="$('#total_document').val(doc_cont);";

				$script.="	
				}
			}

		</script>";
		
		echo $script;
?>
 <!-- Page Content -->
<div class="container">

    <div class="row">
    	<div class="col-lg-8">
			<form class="form-horizontal" method="POST" action="<?=base_url();?>proponente/reg_sugerencia" enctype="multipart/form-data">
				<fieldset>

				<!-- Form Name -->
				<legend>Registro de Sugerencia</legend>

				<!-- Select Basic -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="tipo_mejora">Tipo de Mejoramiento</label>
				  <div class="col-md-8">
				    <select id="tipo_mejora" name="tipo_mejora" class="form-control" required>
				      <option value="">Seleccione uno..</option>
				      <option value="1">Preventivo</option>
				      <option value="2">Mejora</option>
				      <option value="3">Correctivo</option>
				    </select>
				  </div>
				</div>

				<!-- Appended Input-->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="titulo_mejoramiento">Titulo del Mejoramiento</label>
				  <div class="col-md-8">
				      <input id="titulo_mejoramiento" name="titulo_mejoramiento" class="form-control" placeholder="Titulo o Nombre propio de la Sugerencia" type="text" required>
				    <p class="help-block">*Nombre que desea darle a su Sugerencia</p>
				  </div>
				</div>

				<!-- SECCION DEL USUARIO -->
				<?php if ($this->session->userdata('tipo')==1){ ?>
					<!-- Appended Input-->
					<div class="form-group">
					  <label class="col-md-4 control-label" for="participante">Participantes</label>
					  <div class="col-md-8">
					    <div class="input-group">
					      		<input type="text" class="form-control" value="<?= $this->session->userdata('nombre'); ?> <?= $this->session->userdata('apellido'); ?>" disabled>
					      		<input type="hidden" name="participante" value="<?= $this->session->userdata('id'); ?>">
					      <span class="input-group-addon btn btn-primary" onclick="nuevo_doc()"><span class="glyphicon glyphicon-plus-sign"></span></span>
					    </div>

					    <?php if ($this->session->userdata('tipo')==1){ 
					    	?> <p class="help-block">*Proponente(s)</p> <?php
					    }else{
					    	?> <p class="help-block">*Ubiquese en la Lista Desplegable</p> <?php
					    }?>
					  </div>
					</div>
				<?php
				}elseif ($this->session->userdata('tipo')==4) { ?>
					<div class="form-group">
					  <label class="col-md-4 control-label" for="participante">Participantes</label>
					  <div class="col-md-8">
					    <div class="input-group">
					      	<select id="participante" name="participante" class="form-control" required>
						      <option value="">Seleccione uno..</option>
						      <?php foreach ($usuarios->result() as $usuario) { ?>
							  		<option value="<?= $usuario->idusuario; ?>"><?=$usuario->usuario_nombre; ?> <?= $usuario->usuario_apellido; ?></option><?php
							  	} ?>
						    </select> 
						    <div id="funcionarios_seccion1"></div>
					      <span class="input-group-addon btn btn-primary" onclick="nuevo_doc()"><span class="glyphicon glyphicon-plus-sign"></span></span>
					    </div>
					  </div>
					</div>
				<?php 
				}	?>

				

				<div id="mas_participantes">
					<!-- Appended Input-->
					
					<!-- EN ESTE LUGAR SE AGREGAN LOS CAMPOS PARA N DOCUMENTOS -->
				</div>

				<!-- Text input-->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="fecha_registro">Fecha de Registro</label>  
				  <div class="col-md-4">
				  <input id="fecha_registro" name="fecha_registro" type="date" placeholder="placeholder" class="form-control input-md" value="<?=date("Y-m-d")?>" required>
				  <span class="help-block">*Fecha actual</span>  
				  </div>
				</div>

				<!-- SECCION A LA QUE APLICA LA SUGERENCIA -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="seccion_id">Sección a Aplicar</label>
				  <div class="col-md-8">
				  	<div class="">
				  		<select id="seccion_id" name="seccion_id" class="form-control" required>
					      <option value="">Seleccione uno..</option>
					  		<?php foreach ($secciones_aplica->result() as $seccion) {
						  		?><option value="<?= $seccion->idseccion_usuario; ?>"><?=$seccion->seccion_usuario_descripcion; ?></option><?php
						  	} ?>
					    </select>
				  	</div>
				    <p class="help-block">*Sección donde desea aplicar su sugerencia</p>
				  </div>
				</div>

				<!-- Textarea -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="antes_mejoramiento">Antes del Mejoramiento</label>
				  <div class="col-md-8">                     
				    <textarea class="form-control" id="antes_mejoramiento" name="antes_mejoramiento" placeholder="Descripción del estado anterior a la mejora propuesta." required></textarea>
				  </div>
				</div>

				<!-- Textarea -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="problema">Problemas</label>
				  <div class="col-md-8">                     
				    <textarea class="form-control" id="problema" name="problema" placeholder="Descripción del principal inconveniente que encontró." required></textarea>
				  </div>
				</div>

				<!-- Textarea -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="efecto_esperado">Efecto esperado de la sugerencia</label>
				  <div class="col-md-8">                     
				    <textarea class="form-control" id="efecto_esperado" name="efecto_esperado" placeholder="Descripción del efecto que espera tome la sugerencia." required></textarea>
				  </div>
				</div>

				<!-- Multiple Radios -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="ahorro">¿Genera Ahorro?</label>
				  <div class="col-md-8">
				  	<div class="radio">
				    <label for="radios-0">
				      <input type="radio" name="ahorro" id="ahorro-0" value="1">
				      SI
				    </label>
					</div>

				  	<div class="radio">
				    <label for="radios-1">
				      <input type="radio" name="ahorro" id="ahorro-1" value="0" checked="checked">
				      NO
				    </label>
					</div>
					<div class="col-md-12 help-block">
						* (SI) Diligenciar formato para demostración de ahorros. <br>
						* (NO) Continuar. 
						<br><a href="<?=base_url();?>proponente/download_file/F-PQ6-03.xls/FA">Formato de Ahorro de Procesos.</a> 
						<br><a href="<?=base_url();?>proponente/download_file/F-PQ6-05.xls/FA">Formato de Ahorro de Materiales.</a> 
					</div>
				  </div>
				</div>


				<!-- Multiple Radios -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="pre_evidencias">Subir Bosquejo</label>
					<div class="col-md-4 help-block">
						<input type="file" name="pre_evidencias" id="pre_evidencias" class="btn btn-sm btn-primary" required>
					</div>
					<div class="col-lg-8 col-lg-offset-4">
						<p class="help-block">*Documento (Word/Excel) Máximo 5MB, de la descripción o bosquejo. Incluya en este documento, imagenes y/o Graficos (Si aplica).</p>
					</div>
				</div>

				<!-- Button -->
				<div class="form-group">
				  <div class="col-md-4 col-md-offset-4">
				    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Guardar</button>
				  </div>
				</div>

				</fieldset>
			</form>

		</div>

		
    </div>

<?php }
}else{
    redirect('main/login','refresh');
} ?>
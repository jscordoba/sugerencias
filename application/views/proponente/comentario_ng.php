<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* End of file pre_solicitud.php */
/* Location: ./application/views/proponente/pre_solicitud.php */
?>

<div class="container">
    <div class="row">        
        <legend>Comentarios de Mejoramiento ID <b><?= $novedad->pre_mejoramiento_id; ?></b></legend>
    	<div class="col-lg-12">
        	<!-- Textarea -->
			<div class="form-group">
				<label class="col-md-4 control-label" for="novedad">Observacionees</label>
				<div class="col-md-8">                     
				  <textarea class="form-control" id="novedad" rows="5" disabled name="novedad"><?= $novedad->novedad_descripcion; ?></textarea>
				</div>
			</div>
			<div class="col-lg-12"><a href="<?= base_url(); ?>proponente/ver_mejora/41" class="btn btn-primary">Volver</a></div> 
        </div>
	</div>
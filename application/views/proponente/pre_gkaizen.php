<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* End of file pre_solicitud.php */
/* Location: ./application/views/proponente/pre_solicitud.php */
?>

<?php 
if ($this->session->userdata('login')){ 
    if ($this->session->userdata('tipo')==1) {//USUARIO TIPO PROPONENTE
    ?>
 <!-- Page Content -->
<div class="container">

    <div class="row">
    	<div class="col-lg-8">
			<form class="form-horizontal">
				<fieldset>

				<!-- Form Name -->
				<legend>Registro de Grupo Kaizen</legend>

				<!-- Select Basic -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="selectbasic">Tipo de Mejoramiento</label>
				  <div class="col-md-8">
				    <select id="selectbasic" name="selectbasic" class="form-control">
				      <option value="">Seleccione uno..</option>
				      <option value="1">Preventivo</option>
				      <option value="2">Mejora</option>
				    </select>
				  </div>
				</div>

				<!-- Appended Input-->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="appendedtext">Participantes</label>
				  <div class="col-md-8">
				    <div class="input-group">
				      <input id="appendedtext" name="appendedtext" class="form-control" placeholder="placeholder" type="text">
				      <span class="input-group-addon"><a href=""><span class="glyphicon glyphicon-plus-sign glyphicon-plus-sign"></span></a></span>
				    </div>
				    <p class="help-block">*Ayuda</p>
				  </div>
				</div>
				<!-- Text input-->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="textinput">Fecha de Registro</label>  
				  <div class="col-md-4">
				  <input id="textinput" name="textinput" type="text" placeholder="placeholder" class="form-control input-md">
				  <span class="help-block">*Ayuda</span>  
				  </div>
				</div>

				<!-- Select Basic -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="selectbasic">Sección</label>
				  <div class="col-md-8">
				    <select id="selectbasic" name="selectbasic" class="form-control">
				      <option value="">Seleccione uno..</option>
				      <option value="1">Option one</option>
				      <option value="2">Option two</option>
				    </select>
				  </div>
				</div>

				<!-- Textarea -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="textarea">Antes del Mejoramiento</label>
				  <div class="col-md-8">                     
				    <textarea class="form-control" id="textarea" name="textarea">default text</textarea>
				  </div>
				</div>

				<!-- Textarea -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="textarea">Problemas</label>
				  <div class="col-md-8">                     
				    <textarea class="form-control" id="textarea" name="textarea">default text</textarea>
				  </div>
				</div>

				<!-- Textarea -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="textarea">Efecto esperado de la sugerencia</label>
				  <div class="col-md-8">                     
				    <textarea class="form-control" id="textarea" name="textarea">default text</textarea>
				  </div>
				</div>

				<!-- Multiple Radios -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="radios">¿Genera Ahorro?</label>
				  <div class="col-md-8">
				  	<div class="radio">
				    <label for="radios-0">
				      <input type="radio" name="radios" id="radios-0" value="1" checked="checked">
				      SI
				    </label>
					</div>

				  	<div class="radio">
				    <label for="radios-1">
				      <input type="radio" name="radios" id="radios-1" value="2">
				      NO
				    </label>
					</div>
					<div class="col-md-12 help-block">
						* (SI) Diligenciar formato para demostración de ahorros. <br>
						* (NO) Continuar.
					</div>
				  </div>
				</div>

				<!-- Button -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="singlebutton">VoBo. Comité de cambios</label>
				  <div class="col-md-4">
				    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Enviar</button>
				  </div>
				</div>

				<!-- Textarea -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="textarea">Descripcion o bosquejo de la sugerencia propuesta</label>
				  <div class="col-md-8">                     
				    <textarea class="form-control" id="textarea" name="textarea">default text</textarea>
				  </div>
				</div>

				<!-- Button -->
				<div class="form-group">
				  <div class="col-md-4 col-md-offset-4">
				    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Guardar</button>
				  </div>
				</div>

				</fieldset>
			</form>

		</div>

		<div class="col-lg-4">
    		<div class="well">
                <h4>Blog Search</h4>
                <div class="input-group">
                    <input type="text" class="form-control">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <span class="glyphicon glyphicon-search"></span>
                    </button>
                    </span>
                </div>
                <!-- /.input-group -->
            </div>
		</div>
    </div>
<?php }
}else{
    redirect('main/login','refresh');
} ?>
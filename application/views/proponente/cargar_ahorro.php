<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* End of file pre_solicitud.php */
/* Location: ./application/views/proponente/pre_solicitud.php */
?>

<?php 
if ($this->session->userdata('login')){ 
    if ($this->session->userdata('tipo')==1 || $this->session->userdata('tipo')==4) {//USUARIO TIPO PROPONENTE
    ?>
 <!-- Page Content -->
<div class="container">

    <div class="row">
    	<div class="col-lg-8">
			<form class="form-horizontal" method="POST" action="<?=base_url();?>proponente/cargar_ahorro/<?= $id_mejoramiento; ?>/1" enctype="multipart/form-data">
				<fieldset>

				<!-- Form Name -->
				<legend>Cargar Ahorro para Sugerencia ID <?= $id_mejoramiento; ?></legend>
				
				<input type="hidden" name="id_mejoramiento" value="<?= $id_mejoramiento; ?>">
				<!-- Multiple Radios -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="fahorro">Subir Comprobación de Ahorros</label>
					<div class="col-md-4 help-block">
						<input type="file" name="fahorro" id="fahorro" class="btn btn-sm btn-primary" required>
					</div>
					<div class="col-lg-8 col-lg-offset-4">
						<p class="help-block">*Documento (Word/Excel) Máximo 5MB, de la descripción o bosquejo. Incluya en este documento, imagenes y/o Graficos (Si aplica).</p>
					</div>
				</div>

				<!-- Button -->
				<div class="form-group">
				  <div class="col-md-4 col-md-offset-4">
				    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Guardar</button>
				  </div>
				</div>

				</fieldset>
			</form>

		</div>

		
    </div>

<?php }
}else{
    redirect('main/login','refresh');
} ?>
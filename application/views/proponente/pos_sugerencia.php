<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* End of file pre_solicitud.php */
/* Location: ./application/views/proponente/pre_solicitud.php */
?>

<?php 
if ($this->session->userdata('login')){ 
    if ($this->session->userdata('tipo')==1 || $this->session->userdata('tipo')==4) {//USUARIO TIPO PROPONENTE
    ?>
 <!-- Page Content -->
<div class="container">

    <div class="row">
    	<div class="col-lg-8">
			<form class="form-horizontal" method="POST" action="<?=base_url();?>proponente/reg_pos_mejora" enctype="multipart/form-data">
				<fieldset>

				<!-- Form Name -->
				<legend> Fase 2 de <b><?=$sugerencia->pre_mejoramiento_titulo; ?></b></legend>

				<input type="hidden" name="id_pre_mejoramiento" value="<?=$sugerencia->id_pre_mejoramiento; ?>">
				<input type="hidden" name="seccion_usuario_id" value="<?=$sugerencia->seccion_usuario_id; ?>">

				<?php //$sugerencia->seccion_usuario_id; ?>

				<!-- Textarea -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="antes_mejoramiento">Despúes del Mejoramiento</label>
				  <div class="col-md-8">                     
				    <textarea class="form-control" id="despues_mejoramiento" name="despues_mejoramiento" placeholder="default text" required></textarea>
				  </div>
				  <div class="col-lg-8 col-lg-offset-4"><p class="help-block">*Max 500 Caracteres.</p></div>
				</div>

				<!-- Textarea -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="problema">Estandarización</label>
				  <div class="col-md-8">                     
				    <textarea class="form-control" id="estandarizacion" name="estandarizacion" placeholder="Descripción del proceso de estandarización" required></textarea>
				  </div>
				  <div class="col-lg-8 col-lg-offset-4"><p class="help-block">*Max 500 Caracteres.</p></div>
				</div>

				<!-- Multiple Radios -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="post_evidencias">Subir Evidencias</label>
					<div class="col-md-4 help-block">
						<input type="file" name="post_evidencias" id="post_evidencias" class="btn btn-sm btn-primary" required>
					</div>
					<div class="col-lg-8 col-lg-offset-4"><p class="help-block">*Documento (Word/Excel) Máximo 5MB, del resultado de la sugerencia.</p></div>
				</div>

				<!-- Button -->
				<div class="form-group">
				  <div class="col-md-4 col-md-offset-4">
				    <button id="singlebutton" name="singlebutton" class="btn btn-success">Guardar</button>
				  </div>
				</div>

				</fieldset>
			</form>

		</div>

		<div class="col-lg-4">
    		<div class="well">
                <h4>Buscar</h4>
                <div class="input-group">
                    <input type="text" class="form-control">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <span class="glyphicon glyphicon-search"></span>
                    </button>
                    </span>
                </div>
                <!-- /.input-group -->
            </div>
		</div>
    </div>

<?php }
}else{
    redirect('main/login','refresh');
} ?>
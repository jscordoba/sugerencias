<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* End of file pre_solicitud.php */
/* Location: ./application/views/proponente/pre_solicitud.php */
?>

<div class="container">
    <div class="row">        
        <legend>Negar Mejoramiento ID <b><?= $sugerencia->id_pre_mejoramiento; ?></b></legend>
    	<div class="col-lg-12">
        	<form action="<?= base_url();?>proponente/negar/<?=$sugerencia->id_pre_mejoramiento; ?>/2" method="POST">
	            <input type="hidden" name="id_pre_mejoramiento" value="<?= $sugerencia->id_pre_mejoramiento; ?>">
	            <!-- Appended Input-->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="comentario_negar">Comentarios</label>
				  <div class="col-md-8">
				      <textarea class="form-control" id="comentario_negar" name="comentario_negar" placeholder="Comentarios "></textarea>
				    <p class="help-block">*Descripción del porque no aplica.</p>
				  </div>
				</div>

				<!-- Button -->
				<div class="form-group">
				  <div class="col-md-4 col-md-offset-4">
				    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Guardar</button>
				  </div>
				</div>
			</form>
        </div>
	</div>
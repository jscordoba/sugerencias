<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* End of file pre_solicitud.php */
/* Location: ./application/views/proponente/pre_solicitud.php */
?>


<?php 
if ($this->session->userdata('login')){ 
    if ($this->session->userdata('tipo')==1) {//USUARIO TIPO PROPONENTE------------------------------------------
    ?>

 <!-- Page Content -->
<div class="container">
    <div class="row">
    <?php if ($tipo_id==11){//REGISTRADAS ####################################?>
        <legend><?=$tipo?></legend>
        
        <div class="col-lg-12">
            <table class="table table-hover">
                <tr><th>ID</th><th>Titulo</th><th>Tipo</th><th>Registro</th><th>Clase</th><th>Puntaje</th></tr>
                <?php 
                if ($registrados) {
                    foreach ($registrados->result() as $registrado) { ?>
                        <tr>
                            <td><?=$registrado->id_pre_mejoramiento;?></td>
                            <td><?=$registrado->pre_mejoramiento_titulo;?></td>
                            <td><?=$registrado->tipo_mejoramiento_descripcion;?></td>
                            <td><?=$registrado->pre_mejoramiento_date;?></td>
                            <td><?=$registrado->clase_mejoramiento_descripcion; ?></td>
                            <td><a href="<?=base_url();?>proponente/sol_aprobacion/<?=$registrado->id_pre_mejoramiento;?>/<?=$registrado->seccion_usuario_id;?>" class="btn btn-sm btn-success">Solicitar Aprobación</a></td>
                            <td><a href="<?=base_url();?>proponente/editar/<?=$registrado->id_pre_mejoramiento;?>" class="btn btn-sm btn-primary">Editar</a></td>
                            <!--<td><a href="<?=base_url();?>proponente/delete/<?=$registrado->id_pre_mejoramiento;?>" class="btn btn-sm btn-danger">Eliminar</a></td>-->
                        </tr>


                    <?php }
                }
                ?>
            </table>
        </div>
    </div>

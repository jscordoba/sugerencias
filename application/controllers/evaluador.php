<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evaluador extends CI_Controller {

		public function __construct()
	{
		parent::__construct();
		//Do your magic here
		setlocale(LC_ALL,"es_ES");
		date_default_timezone_set('Etc/GMT+5');
		$this->load->model('m_evaluador');
		$this->load->view('header');
	}

	public function index()
	{
		if ($this->session->userdata('tipo')==1) {//USUARIO TIPO PROPONENTE
			redirect('main/login','refresh');
		}elseif ($this->session->userdata('tipo')==2) {//USUARIO TIPO EVALUADOR
			$tipo=$this->session->userdata('tipo');
			$id_usuario=$this->session->userdata('id');
			
			//--------------------SECCION 1 ---------------------//
			$data['num_pendiente_sg']=$this->m_evaluador->m_numrows($id_usuario, 2,1);
			//echo "RESULTADO: ".$result;

			$data['num_pendiente_gk']=$this->m_evaluador->m_numrows($id_usuario, 2,2);
			//echo "RESULTADO: ".$result->num_rows;

			$data['num_pendiente_ot']=$this->m_evaluador->m_numrows($id_usuario, 2,3);
			//echo "RESULTADO: ".$result->num_rows;


			//--------------------SECCION 2 --------------------------//

			$data['transito_sg']=$this->m_evaluador->m_numrows($id_usuario, 3,1);
			//echo "RESULTADO: ".$result;

			$data['transito_gk']=$this->m_evaluador->m_numrows($id_usuario, 3,2);
			//echo "RESULTADO: ".$result->num_rows;

			$data['transito_ot']=$this->m_evaluador->m_numrows($id_usuario, 3,3);
			//echo "RESULTADO: ".$result->num_rows;

			$data['transito_fn']=$this->m_evaluador->m_numrows($id_usuario, 4,1);
			//echo "RESULTADO: ".$result;

			//----------------------------------------------------------//

			$this->load->view('home',$data);
			$this->load->view('footer');

		}elseif ($this->session->userdata('tipo')==3) {//USUARIO TIPO ADMINISTRADOR
			redirect('main/login','refresh');
		}
	}

	public function ver_mejora($tipo_ver){

		$id_usuario=$this->session->userdata('id');

		switch ($tipo_ver) {
			case 11: 
				$data['tipo']="Sugerencias Pendientes";
				$data['tipo_id']=$tipo_ver;
				$data['pendientes']=$this->m_evaluador->pendientes($id_usuario,1,2);//2 APROBADOS POR JEFE PENDIENTES POR LIDER
				$data['indicador']=$tipo_ver;

				break;
			case 12: 
				$data['tipo']="Grupos Kaizen Pendientes";
				$data['tipo_id']=$tipo_ver;
				$data['pendientes']=$this->m_evaluador->pendientes($id_usuario,2,2);//2 APROBADOS POR JEFE PENDIENTES POR LIDER
				break;
			case 13: 
				$data['tipo']="Otros Pendientes";
				$data['tipo_id']=$tipo_ver;
				$data['pendientes']=$this->m_evaluador->pendientes($id_usuario,3,2);//2 APROBADOS POR JEFE PENDIENTES POR LIDER
				break;
			case 21: 
				$data['tipo']="Sugerencias en Transito";
				$data['tipo_id']=$tipo_ver;
				$data['pendientes']=$this->m_evaluador->pendientes($id_usuario,1,3);//3 APROBADOS POR EL LIDER Y SE ENCUENTRAN EN TRANSITO O ACTIVOS
				$data['indicador']=$tipo_ver;
				break;
			case 22: 
				$data['tipo']="Grupos Kaizen en Transito";
				$data['tipo_id']=$tipo_ver;
				$data['pendientes']=$this->m_evaluador->pendientes($id_usuario,2,3);//3 APROBADOS POR EL LIDER Y SE ENCUENTRAN EN TRANSITO O ACTIVOS
				break;
			case 23: 
				$data['tipo']="Otros en Transito";
				$data['tipo_id']=$tipo_ver;
				$data['pendientes']=$this->m_evaluador->pendientes($id_usuario,3,3);//3 APROBADOS POR EL LIDER Y SE ENCUENTRAN EN TRANSITO O ACTIVOS
				break;
			case 24: 
				$data['tipo']="Sugerencias Finalizadas y/o Negadas";
				$data['tipo_id']=$tipo_ver;
				$data['pendientes']=$this->m_evaluador->pendientes($id_usuario,1,4);//LISTA DE SUGERENCIAS FINALIZADOS Y NEGADOS
				$data['indicador']=$tipo_ver;
				$data['evaluacion']=$this->m_evaluador->evaluacion($id_usuario,1,7);
				break;
			default:
				$data="";
				break;
		}

		$this->load->view('ver_mejora',$data);
		$this->load->view('footer');
	}

	public function aprobar($id_mejoramiento, $tipo){
		
		if ($tipo==1) {// (!=2) CARGAR ESCENARIO DE FORMULARIO APROBACION PARA SEGUIR A ESTADO F2
			$data['aprobar']=1;
			$data['mejoramiento']=$id_mejoramiento;

			$this->load->view('evaluador/aprobar',$data);
			//echo "Vista APROBAR solicitud. $id_mejoramiento";
		}elseif($tipo==2){//RECIBIR OBSERVACION Y REGISTRAR LA APROBACION PARA CONTINUAR A ESTADO F2
			$id_mejoramiento=$this->input->post('id_mejoramiento');
			$comentario_aprobacion=$this->input->post('comentario_aprobacion');
			$num_mejora=$id_mejoramiento; // MISMO NUMERO DE ID

			//echo $id_mejoramiento.$comentario_aprobacion.$num_mejora;
			$result=$this->m_evaluador->m_aprobar($id_mejoramiento, $comentario_aprobacion, $num_mejora);

			if ($result) {
				//echo "Guardado, estado cambiado a 3, comentario almacenado.";
				redirect(base_url(),'refresh');
			}
		}elseif ($tipo=="imp") {//CARGAR ESCENARIO DE EVALUACION FINAL
			$data['aprobar']=1;
			$data['mejoramiento']=$id_mejoramiento;

			$this->load->view('evaluador/evaluacion',$data);
			$this->load->view('footer');
		}
		
	}

	public function negar($id_mejoramiento,$tipo){
		$fecha=date("Y-m-d");
		$id_usuario=$this->session->userdata('id');

		if ($tipo!=2) {
			$data['aprobar']=2;
			$data['mejoramiento']=$id_mejoramiento;
			$this->load->view('evaluador/aprobar',$data);
			//echo "Vista APROBAR solicitud. $id_mejoramiento";
		}else{
			$comentario_negar=$this->input->get_post('comentario_negar');

			try {
				$query1="UPDATE pre_mejoramiento 
						SET tipo_aprobacion_id=9
						WHERE id_pre_mejoramiento=$id_mejoramiento";//CAMBIA A APROBACION DE EA (ESPERA DE AHORRO)

				$result1=$this->db->query($query1);

				$query2="UPDATE aprobacion 
						SET tipo_aprobacion_id=9, 
						aprobacion_date='$fecha', 
						usuario_evaluador_id=$id_usuario
						WHERE pre_mejoramiento_id=$id_mejoramiento";
				$result2=$this->db->query($query2);//ESTADO PASA DE PL A EA (ESPERA DE AHORRO), FECHA DE CAMBIO

				$query3="INSERT INTO novedad 
						VALUES ('',$id_mejoramiento,'$comentario_negar','$fecha',$id_usuario,9)";
				$result3=$this->db->query($query3);//ESTADO PASA DE PL A EA (ESPERA DE AHORRO), FECHA DE CAMBIO

				if ($result1) {
					if ($result2) {
						//echo "Mejoramiento Actualizado.";
						if ($result3) {
							//echo "Comentario Ingresado.";
							redirect(base_url(),'refresh');
						}else{
							echo "Error al insertar comentario.";
						}
					}else{
						echo "Error al actualizar aprobacion.";
					}
					
				}else{
					echo "Error al actualizar Mejoramiento.";
				}
			} catch (Exception $e) {
				echo "Error al realizar actualizacion del mejoramiento.";
			}

			//echo "Guardado, estado cambiado a 3, comentario almacenado.";

		}
		//echo "Vista para NO APROBAR solicitud. $id_mejoramiento agregar campo para agregar COMENTARIOS  Y NOVEDADES.";
	}

	public function detalle($id_mejoramiento,$indicador){

		$this->load->model('m_proponente');

		$data['detalles']=$this->m_evaluador->ver_detalle($id_mejoramiento);
		$data['participantes']=$this->m_proponente->ver_participantes($id_mejoramiento);
		$data['indicador']=$indicador;

		//print_r($data['detalles']);

		if ($data['detalles']) {
			//echo "Hola";
			$this->load->view('evaluador/detalle_mejora',$data);
			$this->load->view('footer');
		}else{
			echo "Sin Datos";
			//redirect(base_url(),'refresh');
		}
		
	}

	public function evaluacion(){
		//echo "evaluacion";

		$id_mejoramiento=$this->input->post('id_mejoramiento');
		$concepcion=$this->input->post('concepcion');
		$metodo=$this->input->post('metodo');
		$estandarizacion=$this->input->post('estandarizacion');
		$esfuerzo=$this->input->post('esfuerzo');
		$efecto=$this->input->post('efecto');
		$comentario=$this->input->post('comentario');

		$query=$this->m_evaluador->evaluar($concepcion,$metodo,$estandarizacion,$esfuerzo,$efecto,$comentario,$id_mejoramiento);

		if ($query) {
			echo "Se ha finalizado el mejoramiento correctamente.";
			redirect(base_url(),'refresh');
		}else{
			echo "Error al finalizar el mejoramiento.";
		}
	}

	public function search(){
		$id_usuario=$this->session->userdata('id');
		$busca_mejoramiento_id=$this->input->get_post('busca_mejoramiento_id');

		if ($busca_mejoramiento_id=="") {
			redirect(base_url(),'refresh');
		}else{
			if ($this->session->userdata('tipo')==1) {
				$data['tipo_id']=13;
			}elseif ($this->session->userdata('tipo')==2) {
				$data['tipo_id']=21;
			}elseif ($this->session->userdata('tipo')==4) {
				$data['tipo_id']=41;
			}
			$data['tipo']="Resultado de Busqueda";

			// $query="SELECT * FROM v_mejoramiento_participante 
			// 		WHERE id_pre_mejoramiento='$busca_mejoramiento_id'
			// 		OR usuario_nombre LIKE('%$busca_mejoramiento_id%')
			// 		OR usuario_apellido LIKE('%$busca_mejoramiento_id%')";

			$query="SELECT * 
					FROM aprobacion 
					JOIN pre_mejoramiento ON aprobacion.pre_mejoramiento_id=pre_mejoramiento.id_pre_mejoramiento 
					JOIN tipo_mejoramiento ON tipo_mejoramiento.idtipo_mejoramiento=pre_mejoramiento.tipo_mejoramiento_id 
					JOIN usuario ON usuario.idusuario=aprobacion.usuario_id JOIN seccion_usuario ON seccion_usuario.idseccion_usuario=pre_mejoramiento.seccion_usuario_id 
					JOIN tipo_aprobacion ON pre_mejoramiento.tipo_aprobacion_id=tipo_aprobacion.idtipo_aprobacion 
					LEFT JOIN evaluacion ON evaluacion.pre_mejoramiento_id=pre_mejoramiento.id_pre_mejoramiento
					WHERE id_pre_mejoramiento='$busca_mejoramiento_id'
			 		OR usuario_nombre LIKE('%$busca_mejoramiento_id%')
			 		OR usuario_apellido LIKE('%$busca_mejoramiento_id%')";


			//echo "$query";

			$data['pendientes']=$this->m_evaluador->m_query($query);
			$data['evaluacion']=$this->m_evaluador->evaluacion($id_usuario,1,7);
			$data['indicador']=$data['tipo_id'];

			//echo $query;

			if ($data['pendientes']) {
				$this->load->view('ver_mejora',$data);
				$this->load->view('footer');
			}else{
				redirect(base_url(),'refresh');
			}

		}
		
	}

}

/* End of file evaluador.php */
/* Location: ./application/controllers/evaluador.php */
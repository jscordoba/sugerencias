<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_proponente');
	}


	public function obt_funcionarios(){
		$seccion_id=$this->input->get_post('seccion_id');

		//print_r($seccion_id);

		try {
			$res_usuarios=$this->m_proponente->obt_funcionarios($seccion_id);

			//print_r($res_usuarios);

			foreach ($res_usuarios->result() as $usuarios) { ?>
				<option value="<?= $usuarios->idusuario; ?>"><?= $usuarios->usuario_nombre; ?> <?=$usuarios->usuario_apellido; ?></option>
				<?php
			}
		} catch (Exception $e) {
			echo "Error al buscar usuarios...";
		}
		
	}
	

}

/* End of file buscar.php */
/* Location: ./application/controllers/buscar.php */
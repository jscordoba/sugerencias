<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrador extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		setlocale(LC_ALL,"es_ES");
		date_default_timezone_set('Etc/GMT+5');
		$this->load->model('m_administrador');
		$this->load->view('header');
	}

	public function index()
	{
		if ($this->session->userdata('tipo')==1) {//USUARIO TIPO PROPONENTE
			redirect('main/login','refresh');
		}elseif ($this->session->userdata('tipo')==2) {//USUARIO TIPO EVALUADOR
			redirect('main/login','refresh');
		}elseif ($this->session->userdata('tipo')==3) {//USUARIO TIPO ADMINISTRADOR
			
			$data['num_proponentes']=$this->m_administrador->m_numrows(1,1,0);//6 eESTADO PDT
			//echo "RESULTADO: ".$result;

			$data['num_jefes']=$this->m_administrador->m_numrows(1,3,0);//6 eESTADO PDT
			//echo "RESULTADO: ".$result;

			$data['num_planta']=$this->m_administrador->m_numrows(4,1,0);//6 eESTADO PDT
			//echo "RESULTADO: ".$result;

			$data['num_evaluadores']=$this->m_administrador->m_numrows(2,1,0);//1-2-3-4-5
			//echo "RESULTADO: ".$result->num_rows;


			$data['num_registrado']=$this->m_administrador->m_numrows(0,2,6);//6 eESTADO PDT
			//echo "RESULTADO: ".$result;

			$data['num_activo']=$this->m_administrador->m_numrows(0,2,2);//1-2-3-4-5
			//echo "RESULTADO: ".$result->num_rows;

			$data['num_finalizado']=$this->m_administrador->m_numrows(0,2,7);//7
			//echo "RESULTADO: ".$result->num_rows;

			$this->load->view('home',$data);
			$this->load->view('footer');
		}
	}

	public function ver_usuarios($id){
		$data['usuarios']=$this->m_administrador->usuarios($id);
		$data['seccion']=$this->m_administrador->ver_seccion();
		
		if ($id==3) {
			$id=5;//para realizar busqueda de usuario jefes
		}

		$data['tipo_user']=$id;

		$this->load->view('administrador/ver_user',$data);
		$this->load->view('footer');
		//echo "Vista de usuarios".$id;
	}

	public function ver_mejora($id){
		$data['mejoras']=$this->m_administrador->mejoras($id);
		$data['id']=$id;

		$this->load->view('administrador/ver_mejora',$data);
		$this->load->view('footer');
		//echo "Vista de Mejoras".$id;
	}

	public function detalle($id_mejoramiento,$indicador){

		$this->load->model('m_proponente');

		$data['detalles']=$this->m_administrador->ver_detalle($id_mejoramiento);
		$data['participantes']=$this->m_proponente->ver_participantes($id_mejoramiento);
		$data['indicador']=$indicador;

		if ($data['detalles']) {

			$this->load->view('administrador/detalle_mejora',$data);
			$this->load->view('footer');
		}else{
			echo "Sin Datos";
			//redirect(base_url(),'refresh');
		}
	}

	public function nuevo_user(){
		$data['secciones']=$this->m_administrador->ver_seccion();

		$this->load->view('administrador/admin_user',$data);
		$this->load->view('footer');
	}

	public function search(){
		$c_buscar=$this->input->get_post('c_buscar');
		$tipo_usuario=$this->input->get_post('tipo_usuario');

		$data['busqueda']=$this->m_administrador->search($c_buscar,$tipo_usuario);
		$data['tipo_user']=$tipo_usuario;

		$this->load->view('administrador/search_user', $data);
		$this->load->view('footer');
	}

	public function reg_user(){
		$id_usuario = $this->input->get_post('id_usuario');
		$nombre_usuario = $this->input->get_post('nombre_usuario');
		$apellido_usuario = $this->input->get_post('apellido_usuario');
		$clave_usuario = $this->input->get_post('clave_usuario');
		$seccion_usuario = $this->input->get_post('seccion_usuario');
		$tipo_usuario = $this->input->get_post('tipo_usuario');
		$email_usuario = $this->input->get_post('email_usuario');
		$planta_usuario = $this->input->get_post('planta_usuario');


		try {
			$this->m_administrador->reg_user($id_usuario,$nombre_usuario,$apellido_usuario,$clave_usuario,$seccion_usuario,$tipo_usuario,$email_usuario,$planta_usuario);

			redirect(base_url().'administrador/ver_usuarios','refresh');

		} catch (Exception $e) {
			echo "Error al ingresar nuevo usuario.";
		}
	}

	public function us_del($id_usuario){
		//echo "$id_usuario";
?>
		<script type="text/javascript">
			var x=confirm("¿Esta seguro de eliminar este usuario?");
			if (x==true) {
				<?php
				try {
					$this->m_administrador->del_us($id_usuario);
				} catch (Exception $e) {
					return "Error al Eliminar usuario.";
				}
				?>
			}			
		</script>
<?php
		redirect(base_url().'administrador/ver_usuarios/1','refresh');
	}


	public function us_edi($id_usuario){

		$data['user_edit']=$this->m_administrador->user_edi($id_usuario);
		$data['secciones']=$this->m_administrador->ver_seccion();

		$this->load->view('administrador/edit_user',$data);
		$this->load->view('footer');
	}

	public function edit_user(){
		$id_user = $this->input->get_post('id_user');
		$nombre_usuario = $this->input->get_post('nombre_usuario');
		$apellido_usuario = $this->input->get_post('apellido_usuario');
		$clave_usuario = $this->input->get_post('clave_usuario');
		$seccion_usuario = $this->input->get_post('seccion_usuario');
		$tipo_usuario = $this->input->get_post('tipo_usuario');
		$email_usuario = $this->input->get_post('email_usuario');
		$planta_usuario = $this->input->get_post('planta_usuario');


		try {
			$this->m_administrador->upd_user($id_user,$nombre_usuario,$apellido_usuario,$clave_usuario,$seccion_usuario,$tipo_usuario,$email_usuario,$planta_usuario);

			redirect(base_url().'administrador/','refresh');

		} catch (Exception $e) {
			echo "Error al ingresar nuevo usuario.";
		}
	}


	public function search_home(){
		$this->load->model('m_evaluador');
		$this->load->model('m_proponente');

		$busca_mejoramiento_id=$this->input->get_post('busca_mejoramiento_id');
		$id_usuario=$this->session->userdata('id');

		if ($busca_mejoramiento_id=="") {
			redirect(base_url(),'refresh');
		}else{
			$query="SELECT * FROM v_mejoramiento_participante 
					WHERE id_pre_mejoramiento='$busca_mejoramiento_id'
					OR usuario_nombre LIKE('%$busca_mejoramiento_id%')
					OR usuario_apellido LIKE('%$busca_mejoramiento_id%')
					GROUP BY id_pre_mejoramiento";

			$data['mejoras']=$this->m_evaluador->m_query($query);
			$data['evaluacion']=$this->m_proponente->registrados($id_usuario,1,7);
			$data['id']=5;

			//echo $query;

			if ($data['mejoras']) {
				$this->load->view('administrador/ver_mejora',$data);
				$this->load->view('footer');
			}else{
				redirect(base_url(),'refresh');
			}

		}
		
	}

}

/* End of file administrador.php */
/* Location: ./application/controllers/administrador.php */
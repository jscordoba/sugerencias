<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		setlocale(LC_ALL,"es_ES");
		date_default_timezone_set('Etc/GMT+5');
		//$this->load->model('m_proponente');
		$this->load->model('m_main');
		$this->load->view('header');
	}

	public function index()
	{
		//$this->load->view('home');
		$this->load->view('login');
		$this->load->view('footer');
	}

	public function login(){//LOGUEO AL APLICATIVO

		$log_user=$this->input->get_post('log_user');
		$log_clave=$this->input->get_post('log_clave');

		//echo "Usuario: ".$log_user."<br>Clave: ".$log_clave;

		$log=$this->m_main->m_login($log_user,$log_clave);

		if ($log!=null) {//si se encuentra usuario y clave coinciden en la BD
			//echo "nombre: ".$log->idusuario;
			
			$sesion_login = array(//se crea el array asociativo de sesion
									'login' => true,
									'tipo' => $log->tipo_usuario_id,
									'usuario' =>$log->usuario_usuario,
									'nombre' =>$log->usuario_nombre,
									'apellido' =>$log->usuario_apellido,
									'jefe' =>$log->idjefe_area,
									'jefe_area' =>$log->seccion_usuario_descripcion,
									'id' => $log->idusuario );

			$this->session->set_userdata($sesion_login);
			$this->session->userdata('id');

			//echo " - sesion: ".$this->session->userdata('login');;
			if ($this->session->userdata('tipo')==1 || $this->session->userdata('tipo')==4) {//proponente
				redirect('proponente/','refresh');
			}elseif ($this->session->userdata('tipo')==2) {//evaluador
				redirect('evaluador/','refresh');
			}elseif ($this->session->userdata('tipo')==3) {//administrador	
				redirect('administrador/','refresh');
			}
		}else{
			$resu="Usuario y/o Clave Incorrecto.";
			$this->load->view('login');
			$this->load->view('footer');
		}
	}

	public function logout(){
		$this->session->sess_destroy();

		redirect('main','refresh');
	}

	public function registro(){//REGISTRO DE UN USUARIO NUEVO
		$this->load->view('registro');
		$this->load->view('footer');
	}



	

}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
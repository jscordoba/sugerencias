<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proponente extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		setlocale(LC_ALL,"es_ES");
		date_default_timezone_set('Etc/GMT+5');
		$this->load->model('m_proponente');
		$this->load->view('header');
	}


	public function index()
	{
		if ($this->session->userdata('tipo')==1 || $this->session->userdata('tipo')==4) {//USUARIO TIPO PROPONENTE
			$tipo=$this->session->userdata('tipo');
			$id_usuario=$this->session->userdata('id');
			
			$data['num_registrado_sg']=$this->m_proponente->m_numrows($id_usuario,1,6);//6 eESTADO PDT
			//echo "RESULTADO: ".$result;

			$data['num_activo_sg']=$this->m_proponente->m_numrows($id_usuario,1,2);//1-2-3-4-5 ACTIVOS
			//echo "RESULTADO: ".$result->num_rows;

			$data['num_finalizado_sg']=$this->m_proponente->m_numrows($id_usuario,1,7);//7 FINALIZADO
			//echo "RESULTADO: ".$result->num_rows;

			$data['num_negado_sg']=$this->m_proponente->m_numrows($id_usuario,1,9);//9 NEGADOS
			//echo "RESULTADO: ".$result->num_rows;

			$data['num_registrado_gk']=$this->m_proponente->m_numrows($id_usuario,2,6);//6 eESTADO PDT
			//echo "RESULTADO: ".$result;

			$data['num_activo_gk']=$this->m_proponente->m_numrows($id_usuario,2,2);//1-2-3-4-5 ACTIVOS
			//echo "RESULTADO: ".$result->num_rows;

			$data['num_finalizado_gk']=$this->m_proponente->m_numrows($id_usuario,2,7);//7 FINALIZADO
			//echo "RESULTADO: ".$result->num_rows;

			 if ($this->session->userdata('jefe')) {
				$data['num_presentacion']=$this->m_proponente->m_numrows($id_usuario,0,1);//1 ESTADO PJ
				//echo "RESULTADO: ".$result->num_rows;

				$data['num_implementacion']=$this->m_proponente->m_numrows($id_usuario,0,4);//4 ESTADO IJ
				//echo "RESULTADO: ".$result->num_rows;
			}


			$this->load->view('home',$data);
			$this->load->view('footer');
		}elseif ($this->session->userdata('tipo')==2) {//USUARIO TIPO EVALUADOR
			redirect('main/login','refresh');
		}elseif ($this->session->userdata('tipo')==3) {//USUARIO TIPO ADMINISTRADOR
			redirect('main/login','refresh');
		}
	}

	public function ver_mejora($tipo_ver){

		$id_usuario=$this->session->userdata('id');

		switch ($tipo_ver) {
			case 11: 
				$data['tipo']="Sugerencias Registradas";
				$data['tipo_id']=$tipo_ver;
				$data['registrados']=$this->m_proponente->registrados($id_usuario,1,6);
				$data['indicador']=$tipo_ver;
				break;
			case 13: 
				$data['tipo']="Sugerencias Activas";
				$data['tipo_id']=$tipo_ver;
				$data['registrados']=$this->m_proponente->registrados($id_usuario,1,2);
				$data['indicador']=$tipo_ver;
				break;
			case 14: 
				$data['tipo']="Sugerencias Finalizadas";
				$data['tipo_id']=$tipo_ver;
				$data['registrados']=$this->m_proponente->registrados($id_usuario,1,7);
				$data['evaluacion']=$this->m_proponente->evaluacion($id_usuario,1,7);
				$data['indicador']=$tipo_ver;
				break;
			case 15: 
				$data['tipo']="Sugerencias Negadas";
				$data['tipo_id']=$tipo_ver;
				$data['registrados']=$this->m_proponente->registrados($id_usuario,1,9);
				$data['indicador']=$tipo_ver;
				break;
			case 21: 
				$data['tipo']="G.K Registrados";
				$data['tipo_id']=$tipo_ver;
				$data['registrados']=$this->m_proponente->registrados($id_usuario,2,6);
				break;
			case 23: 
				$data['tipo']="G.K Activos";
				$data['tipo_id']=$tipo_ver;
				$data['registrados']=$this->m_proponente->registrados($id_usuario,2,2);
				break;
			case 24: 
				$data['tipo']="G.K Finalizados";
				$data['tipo_id']=$tipo_ver;
				$data['registrados']=$this->m_proponente->registrados($id_usuario,2,7);
				break;
			case 31: 
				$data['tipo']="Aprobación de Solicitudes - Presentación";
				$data['tipo_id']=$tipo_ver;
				$data['registrados']=$this->m_proponente->registrados($id_usuario,0,1);//1 SOLICITUD DE PROPONENTE PARA APROBACION DE PRESENTACION POR EL JEFE
				$data['indicador']=$tipo_ver;
				break;
			case 32: 
				$data['tipo']="Aprobación de Solicitudes - Implementación";
				$data['tipo_id']=$tipo_ver;
				$data['registrados']=$this->m_proponente->registrados($id_usuario,0,4);//4 SOLICITUD DE PROPONENTE PARA APROBACION DE FASE 2 E IMPLEMENTACION POR EL JEFE
				$data['indicador']=$tipo_ver;
				break;

			case 41: 
				$data['tipo']="Sugerencias Registradas";
				$data['tipo_id']=$tipo_ver;
				$data['registrados']=$this->m_proponente->registrados($id_usuario,1,3);//3 TRAER INFORMACION DE TODAS LAS SUG. SOLO DE PLANTA 
				$data['evaluacion']=$this->m_proponente->registrados($id_usuario,1,7);
				$data['indicador']=$tipo_ver;
				break;
			case 42: 
				$data['tipo']="Grupos Kaizen Registrados";
				$data['tipo_id']=$tipo_ver;
				$data['registrados']=$this->m_proponente->registrados($id_usuario,0,4);//4 SOLICITUD DE PROPONENTE PARA APROBACION DE FASE 2 E IMPLEMENTACION POR EL JEFE
				break;

			default:
				$data="";
				break;
		}

		$this->load->view('ver_mejora',$data);
		$this->load->view('footer');
	}

	public function pre_solicitud($tipo){
		$data['secciones_usuarios']=$this->m_proponente->m_query("SELECT * FROM seccion_usuario ORDER BY seccion_usuario_descripcion ASC");
		$data['secciones_aplica']=$this->m_proponente->m_query("SELECT * FROM seccion_usuario ORDER BY seccion_usuario_descripcion ASC");
		$data['usuarios']=$this->m_proponente->m_query("SELECT * FROM usuario 
														WHERE tipo_usuario_id NOT IN(1,2,3) 
														AND idusuario NOT IN(1,2,3)
														ORDER BY usuario_nombre ASC");

		if ($tipo==1) {
			$data['tipo']=$tipo;
			$this->load->view('proponente/pre_sugerencia', $data);
			$this->load->view('footer');
		}elseif ($tipo==2) {
			$data['tipo']=$tipo;
			$this->load->view('proponente/pre_gkaizen', $data);
			$this->load->view('footer');
		}elseif ($tipo==3) {
			$data['tipo']=$tipo;
			$this->load->view('proponente/pre_otro', $data);
			$this->load->view('footer');
		}
	}

	public function reg_sugerencia(){
		$fecha=date("Y-m-d");
		$tipo_mejora=$this->input->get_post('tipo_mejora');
		$titulo_mejoramiento=$this->input->get_post('titulo_mejoramiento');		
		$fecha_registro=$this->input->get_post('fecha_registro');//fecha actual
		$seccion_id=$this->input->get_post('seccion_id');
		$antes_mejoramiento=$this->input->get_post('antes_mejoramiento');
		$problema=$this->input->get_post('problema');
		$efecto_esperado=$this->input->get_post('efecto_esperado');
		$ahorro=$this->input->get_post('ahorro');
		$f_ahorro=$this->input->get_post('f_ahorro');
		$clase_mejoramiento=1;//SUGERENCIA 1 - GK 2 - PLAN ESTRATEGICO 3
		$estado_mejoramiento=3;//APROBADO 1 - NO APROBADO 2 - PDTE APROBACION 3 - FINALIZADO 4
		
		$regis_id_usuario=$this->input->get_post('participante');
		$regis_id_usuario2=$this->input->get_post('participante2');
		//$regis_id_usuario=$this->session->userdata('id');

		if ($this->session->userdata('tipo')==4) {
			$estado=1;
		}else{
			$estado=6;
		}
		
		try {
			$query="INSERT INTO pre_mejoramiento VALUES('','$titulo_mejoramiento',$tipo_mejora,'$fecha_registro',$clase_mejoramiento,$seccion_id,NULL,$estado)";
			$result=$this->m_proponente->m_query($query);

			$last_id=$this->db->insert_id();
			//echo "<br>".$last_id;

			$form_evidencia='pre_evidencias';
			$propuesta=$this->upload_file($fecha,$last_id,$form_evidencia);

			//echo "CARGA DE ARCHIVO: $propuesta.<br>";

			if ($result) {
				//echo "mejoramiento registrado";

				$bandera=1;

				try {
					$query1="INSERT INTO sugerencia 
							VALUES('',$last_id,'$antes_mejoramiento','$problema','$efecto_esperado',$ahorro,'$f_ahorro','$propuesta')";

					$result1=$this->m_proponente->m_query($query1);

					if ($result1) {
						//echo "sugerencia registrada.";
						?>
						<script type="text/javascript">
							alert("Sugerencia Registrada con Éxito!. \nNo. <?= $last_id ?>");
						</script>
						<?php
						$bandera=2;
					}else{
						echo "Error al insertar el query1";
					}
				} catch (Exception $e) {
					echo "Error al insertar en la tabla Sugerencia.";
				}

				try {
					$query2="INSERT INTO participante 
							VALUES('',$last_id,'$regis_id_usuario')";

					$result2=$this->m_proponente->m_query($query2);

					if ($regis_id_usuario2!="" || $regis_id_usuario2!=NULL) {
						$query3="INSERT INTO participante 
							VALUES('',$last_id,'$regis_id_usuario2')";

						try {
							$result3=$this->m_proponente->m_query($query3);
						} catch (Exception $e) {
							echo "Error al insertar el Segundo Participante.";
						}
					}
					

					if ($result2) {
						//echo "participante registrado";
						$bandera=3;
					}else{
						echo "Error al insertar el query1";
					}
				} catch (Exception $e) {
					echo "Error al insertar en la tabla participante.";
				}
				
				try {
					$query3="INSERT INTO aprobacion 
							VALUES('',$last_id,'$regis_id_usuario',6,'$fecha_registro',NULL)";

					$result3=$this->m_proponente->m_query($query3);

					if ($this->session->userdata('tipo')==4) {
						$Presentacion=$this->sol_aprobacion($last_id,$seccion_id);//SE ENVIA PARA APROBACION DIRECTA DE PRESENTACION USUARIO PLANTA

						if ($Presentacion) {
							echo "Aprobacion solicitada al Jefe.";
						}else{
							echo "Error al solicitar aprobacion directa de presentación.";
						}
					}else{
						if ($result3) {
							//echo "aprobacion registrada";
							$bandera=4;
						}else{
							echo "Error al insertar el query1";
						}
					}
					
				} catch (Exception $e) {
					echo "Error al insertar en la tabla aprobacion.";
				}

			}else{
				echo "Error al insertar el query1";
			}
		} catch (Exception $e) {
			echo "Error al insertar en la tabla pre_mejoramiento.";
		}
		
		if ($bandera=4) {
			redirect(base_url().'proponente/','refresh');
		}
		
		/*echo "<br>Query: ".$query;
		echo "<br>Query1: ".$query1;*/
		

	}

	public function reg_gk(){
		
	}

	public function reg_otro(){
		
	}

	public function search(){
		$busca_mejoramiento_id=$this->input->get_post('busca_mejoramiento_id');
		// $data['indicador']=$tipo_ver;
		$id_usuario=$this->session->userdata('id');
		

		if ($busca_mejoramiento_id=="") {
			redirect(base_url(),'refresh');
		}else{
			if ($this->session->userdata('tipo')==1) {
				$data['tipo_id']=13;
				$user=" usuario_id=$id_usuario";
			}elseif ($this->session->userdata('tipo')==2) {
				$data['tipo_id']=21;
				$user="1 ";
			}elseif ($this->session->userdata('tipo')==4) {
				$data['tipo_id']=41;
				$user="1 ";
			}elseif ($this->session->userdata('tipo')==3) {
				$data['tipo_id']=31;
				$user="1 ";
			}
			$data['tipo']="Resultado de Busqueda";

			$query="SELECT * FROM v_mejoramiento_participante 
					WHERE $user
					AND (id_pre_mejoramiento='$busca_mejoramiento_id'
						OR usuario_nombre LIKE('%$busca_mejoramiento_id%')
						OR usuario_apellido LIKE('%$busca_mejoramiento_id%') )
					GROUP BY id_pre_mejoramiento";

			$data['registrados']=$this->m_proponente->m_query($query);
			$data['evaluacion']=$this->m_proponente->registrados($id_usuario,1,7);
			$data['indicador']=$data['tipo_id'];

			//echo $query;

			if ($data['registrados']) {
				$this->load->view('ver_mejora',$data);
				$this->load->view('footer');
			}else{
				redirect(base_url(),'refresh');
			}

		}
		
	}

	public function sol_aprobacion($pre_mejoramiento_id,$seccion_usuario_id){
		/* ########### SOLICITUD DE APROBACION DESDE PROPONENTE HACIA PJ ########### */ 
		$fecha=date("Y-m-d");

		try {
			$query="SELECT jefe_area.usuario_id,usuario.email 
					FROM jefe_area 
					JOIN usuario on usuario.idusuario=jefe_area.usuario_id
					WHERE jefe_area.seccion_usuario_id=$seccion_usuario_id
					LIMIT 1";

			$result=$this->m_proponente->m_query($query);
			$usuario_evaluador_id=$result->row();//id usuario jefe del area al que pertenece y jefe de la seccion que afecta el mejoramiento
		} catch (Exception $e) {
			echo "Error al ubicar el jefe correspondiente de la sección.";
		}

		//echo $usuario_evaluador_id->usuario_id;

		if (isset($usuario_evaluador_id)) {
			try {
				$query1="UPDATE pre_mejoramiento 
						SET tipo_aprobacion_id=1
						WHERE id_pre_mejoramiento=$pre_mejoramiento_id";//CAMBIA A APROBACION DE PRESENTACION JEFE

				$result1=$this->m_proponente->m_query($query1);

				$query2="UPDATE aprobacion 
						SET tipo_aprobacion_id=1, 
						aprobacion_date='$fecha', 
						usuario_evaluador_id=$usuario_evaluador_id->usuario_id
						WHERE pre_mejoramiento_id=$pre_mejoramiento_id";
				$result2=$this->m_proponente->m_query($query2);//SE ESTADO DE APROBACION A PJ, FECHA DE CAMBIO Y SE ASIGNA JEFE DE SECCION CORRESPONDIENTE

				if ($result1) {
					if ($result2) {
						
						//echo "Mejoramiento Actualizado.";
						$email=$usuario_evaluador_id->email;

						$subject="Nueva solicitud de Mejora.";
						$mail=$this->enviar_email($pre_mejoramiento_id,$email,$subject);
						
						if ($mail) {
							redirect(base_url().'proponente/','refresh');
						}else{
							echo "Error al enviar el Correo de Notificación.";
						}
					}else{
						echo "Error al actualizar aprobacion.";
					}
					
				}else{
					echo "Error al actualizar Mejoramiento.";
				}
			} catch (Exception $e) {
				echo "Error al realizar actualizacion del mejoramiento.";
			}
		}
	}

	public function detalle($id_mejoramiento,$indicador){

		$this->load->model('m_evaluador');

		$data['detalles']=$this->m_evaluador->ver_detalle($id_mejoramiento);
		$data['participantes']=$this->m_proponente->ver_participantes($id_mejoramiento);
		$data['indicador']=$indicador;

		if ($data['detalles']) {

			$this->load->view('proponente/detalle_mejora',$data);
			$this->load->view('footer');
		}else{
			echo "Sin Datos";
			//redirect(base_url(),'refresh');
		}
	}


	public function cargar_ahorro($id_pre_mejoramiento,$tipo){
		$fecha=date("Y-m-d");

		if ($tipo==0) {
			$data['id_mejoramiento']=$id_pre_mejoramiento;
			$this->load->view('proponente/cargar_ahorro',$data);
			$this->load->view('footer');
		}elseif ($tipo==1) {
			$fahorro=$this->input->get_post('fahorro');
			$id_mejoramiento=$fahorro=$this->input->get_post('id_mejoramiento');

			$form_evidencia='f_ahorro';
		
			$ahorro=$this->upload_file($fecha,$id_pre_mejoramiento,$form_evidencia);

			if ($ahorro) {
				$this->m_proponente->m_query("UPDATE sugerencia SET sugerencia_fahorro='$ahorro'
											WHERE pre_mejoremiento_id=$id_mejoramiento");
				redirect(base_url(),'refresh');
			}else{
				echo "Error de carga";
			}
		}
			
	}

	public function editar($id_pre_mejoramiento){ // EDITAR DETALLES DE LA FASE 1
		$fecha=date("Y-m-d");

		if ($id_pre_mejoramiento=="up") {
			$id_pre_mejoramiento=$this->input->post('id_pre_mejoramiento');
			$tipo_mejora=$this->input->post('tipo_mejora');
			$titulo_mejoramiento=$this->input->post('titulo_mejoramiento');
			//$participantex=$this->input->post('participantex');
			//$fecha_registro=$this->input->post('fecha_registro');
			$seccion_id=$this->input->post('seccion_id');
			$antes_mejoramiento=$this->input->post('antes_mejoramiento');
			$problema=$this->input->post('problema');
			$efecto_esperado=$this->input->post('efecto_esperado');

			$query1=$this->m_proponente->m_query("UPDATE pre_mejoramiento SET 
											pre_mejoramiento_titulo='$titulo_mejoramiento',
											tipo_mejoramiento_id=$tipo_mejora,
											pre_mejoramiento_date='$fecha',
											seccion_usuario_id=$seccion_id
											WHERE id_pre_mejoramiento=$id_pre_mejoramiento");

			$query2=$this->m_proponente->m_query("UPDATE sugerencia SET 
											sugerencia_antes_mejora='$antes_mejoramiento',
											sugerencia_problema='$problema',
											sugerencia_esperado='$efecto_esperado'
											WHERE pre_mejoremiento_id=$id_pre_mejoramiento");

			if ($query1) {
				echo "Se actualizó mejoramiento correctamente.";
				if ($query2) {
					echo "Se actualizó sugerencia correctamente.";

					redirect(base_url().'proponente/ver_mejora/11','refresh');
				}else{
					echo "Error al actualizar sugerencia.";
				}
			}else{
				echo "Error al actualizar mejoramiento.";
			}

		}else{
			$data['detalles']=$this->m_proponente->ver_detalle($id_pre_mejoramiento,1);

			if ($data['detalles']) {
				$this->load->view('proponente/editar_mejora',$data);
				$this->load->view('footer');
			}else{
				echo "Sin Datos";
				//redirect(base_url(),'refresh');
			}
			//echo "Editar Sugerencia. ".$id_mejoramiento;
		}
	}

	public function negar($id_pre_mejoramiento, $tipo){
		$id_usuario=$this->session->userdata('id');
		$fecha=date("Y-m-d");

		if ($tipo==1) {
			$this->db->where('id_pre_mejoramiento', $id_pre_mejoramiento);
			$data['sugerencia']=$this->db->get('pre_mejoramiento')->row();
			
			$this->load->view('proponente/negar',$data);
		}elseif ($tipo==2) {
			$comentario_negar=$this->input->get_post('comentario_negar');
			$id_pre_mejoramiento=$this->input->get_post('id_pre_mejoramiento');

			try {
				$query1="UPDATE pre_mejoramiento 
						SET tipo_aprobacion_id=9
						WHERE id_pre_mejoramiento=$id_pre_mejoramiento";//CAMBIA A APROBACION DE EA (ESPERA DE AHORRO)

				$result1=$this->db->query($query1);

				$query2="UPDATE aprobacion 
						SET tipo_aprobacion_id=9, 
						aprobacion_date='$fecha', 
						usuario_evaluador_id=$id_usuario
						WHERE pre_mejoramiento_id=$id_pre_mejoramiento";
				$result2=$this->db->query($query2);//ESTADO PASA DE PL A EA (ESPERA DE AHORRO), FECHA DE CAMBIO

				$query3="INSERT INTO novedad 
						VALUES ('',$id_pre_mejoramiento,'$comentario_negar','$fecha',$id_usuario,9)";
				$result3=$this->db->query($query3);//ESTADO PASA DE PL A EA (ESPERA DE AHORRO), FECHA DE CAMBIO

				if ($result1) {
					if ($result2) {
						//echo "Mejoramiento Actualizado.";
						if ($result3) {
							//echo "Comentario Ingresado.";
							redirect(base_url(),'refresh');
						}else{
							echo "Error al insertar comentario.";
						}
					}else{
						echo "Error al actualizar aprobacion.";
					}
					
				}else{
					echo "Error al actualizar Mejoramiento.";
				}
			} catch (Exception $e) {
				echo "Error al realizar actualizacion del mejoramiento.";
			}
		}
	}

	public function comentario_ng($id_mejoramiento){

		$result=$this->m_proponente->m_query("SELECT * FROM novedad 
											WHERE pre_mejoramiento_id=$id_mejoramiento
											AND tipo_aprobacion_id=9");


		if ($result) {
			$data['novedad']=$result->row();
			$this->load->view('proponente/comentario_ng', $data);
		}
	}

	public function delete($id_pre_mejoramiento){
		echo "Eliminar la sugerencia. ".$id_pre_mejoramiento;

		try {
			$delete=$this->m_proponente->eliminar($id_pre_mejoramiento);
			redirect(base_url().'proponente/ver_mejora/11','refresh');
		} catch (Exception $e) {
			redirect(base_url().'proponente/ver_mejora/11','refresh');
			echo "Sin eliminar";
		}
		

		if ($delete) {
			echo "eliminado";
			//redirect(base_url().'proponente/ver_mejora/11','refresh');
		}else{
			echo "Sin eliminar";
		}
	}

	public function aprobar($id_pre_mejoramiento,$aprobacion){
		/* ########### APROBACION DESDE PROPONENTE JEFE HACIA PL (LIDER MEJORAMIENTO) ########### */ 
		$fecha=date("Y-m-d");

		$query="SELECT idusuario,email
				FROM v_usuarios_jefe 
				WHERE tipo_usuario_id=2
				LIMIT 1";

		$result=$this->m_proponente->m_query($query);
		$usuario_evaluador=$result->row();//id usuario EVALUADOR O LIDER DE MEJORAMIENTO

		//echo $usuario_evaluador_id->idusuario;

		if ($usuario_evaluador) {
			if ($aprobacion=="pre") {//REGISTRAR APROBACION Y ESCALAR A EVALUADOR PARA PRESENTACION
				$tipo_aprobacion_id=2;
			}elseif ($aprobacion=="imp") {//REGISTRAR APROBACION Y ESCALAR A EVALUADOR PARA IMPLEMENTACION
				$tipo_aprobacion_id=5;
			}

			try {
				$query1="UPDATE pre_mejoramiento 
						SET tipo_aprobacion_id=$tipo_aprobacion_id
						WHERE id_pre_mejoramiento=$id_pre_mejoramiento";//CAMBIA A APROBACION DE PRESENTACION LIDER
				$result1=$this->m_proponente->m_query($query1);

				$query2="UPDATE aprobacion 
						SET tipo_aprobacion_id=$tipo_aprobacion_id, 
						aprobacion_date='$fecha', 
						usuario_evaluador_id=$usuario_evaluador->idusuario
						WHERE pre_mejoramiento_id=$id_pre_mejoramiento";
				$result2=$this->m_proponente->m_query($query2);//ESTADO PASA DE PJ A PL, FECHA DE CAMBIO Y SE ASIGNA AL LIDER DE MEJORAMIENTO

				if ($result1) {
					if ($result2) {
						//echo "Mejoramiento Actualizado.";
						$email=$usuario_evaluador->email;

						$subject="Novedad de solicitud de Mejora.";
						$mail=$this->enviar_email($id_pre_mejoramiento,$email,$subject);
						
						if ($mail) {
							//echo $mail;
							redirect(base_url(),'refresh');
						}else{
							echo "Error al enviar el Correo de Notificación.";
						}

					}else{
						echo "Error al actualizar aprobacion.";
					}
					
				}else{
					echo "Error al actualizar Mejoramiento.";
				}
			} catch (Exception $e) {
				echo "Error al realizar actualizacion del mejoramiento.";
			}
		}	
	}

	public function pos_sugerencia($id_pre_mejoramiento){
		$this->db->where('id_pre_mejoramiento', $id_pre_mejoramiento);
		$data['sugerencia']=$this->db->get('pre_mejoramiento')->row();
		
		$this->load->view('proponente/pos_sugerencia',$data);
	}

	public function reg_pos_mejora(){
		$fecha=date("Y-m-d");
		$id_pre_mejoramiento=$this->input->post('id_pre_mejoramiento');
		$seccion_usuario_id=$this->input->post('seccion_usuario_id');
		$despues_mejoramiento=$this->input->post('despues_mejoramiento');
		$estandarizacion=$this->input->post('estandarizacion');
		$form_evidencia='post_evidencias';
		
		$evidencia=$this->upload_file($fecha,$id_pre_mejoramiento,$form_evidencia);

		echo $evidencia;
		echo "$seccion_usuario_id";

		/* ########### SOLICITUD DE APROBACION DESDE PROPONENTE HACIA JEFE ########### */ 
		try {
			$query="SELECT idusuario,email
					FROM `jefe_area` 
					JOIN usuario ON usuario.idusuario=jefe_area.usuario_id 
					WHERE jefe_area.seccion_usuario_id=$seccion_usuario_id
					LIMIT 1";

			echo "$query";

			$result=$this->m_proponente->m_query($query);
			$usuario_evaluador=$result->row();//id usuario jefe del area al que pertenece y jefe de la seccion que afecta el mejoramiento

		} catch (Exception $e) {
			echo "Error al ubicar el jefe correspondiente de la sección.";
		}

		$evaluador=$usuario_evaluador->idusuario;
		echo $evaluador;

		if (isset($usuario_evaluador)) {
			//echo "recibir pos mejora.".$despues_mejoramiento.$estandarizacion.$evidencias." insertar en estandarizacion y luego con el ID se inserta en post_mejoramiento";

			$result=$this->m_proponente->reg_f2($id_pre_mejoramiento,$despues_mejoramiento,$evidencia,$estandarizacion,$fecha,$evaluador);
		}
		//########################################################################

		if ($result) {
			$email=$usuario_evaluador->email;

			$subject="Novedad de solicitud de Mejora.";
			$mail=$this->enviar_email($id_pre_mejoramiento,$email,$subject);

			redirect(base_url(),'refresh');
		}else{
			//redirect(base_url(),'refresh');
			echo "Error al registrar la post mejora";
		}
	}


	public function upload_file($fecha,$id_pre_mejoramiento,$form_evidencia){
		//echo $form_evidencia.'_'.$fecha.'_ID'.$id_pre_mejoramiento;


		if ($form_evidencia=="pre_evidencias") {//CARGAR ARCHIVO DE PROPUESTA PRE_MEJORAMIENTO
			$config['upload_path'] = 'uploads/evidencias/';
			$config['allowed_types'] = 'xls|xlsx|doc|docx';
			$config['file_name']= $form_evidencia.'_'.$fecha.'_ID'.$id_pre_mejoramiento;
			$config['max_size']  = '5120';
			$config['max_width']  = '2048';
			$config['max_height']  = '2048';
			
			$this->load->library('upload', $config);


			if ( ! $this->upload->do_upload('pre_evidencias')){
				$error = array('error' => $this->upload->display_errors());
				echo $this->upload->display_errors();
			}
			else{
				$upload_data=$this->upload->data();//SE GUARDA EL ARCHIVO EN EL PATH ESPECIFICADO
				//echo "success pre_evidencias";
				return $upload_data['file_name'];//SE ASIGNA EL NOMBRE DE ARCHIVO PARA AGREGAR A LA DB
				//print_r($upload_data);
				//echo $upload_data['file_name'].$upload_data['full_path'];
			}
		}elseif ($form_evidencia=="post_evidencias") {//CARGAR ARCHIVO DE EVIDENCIAS POST MEJORAMIENTO
			$config['upload_path'] = 'uploads/evidencias/';
			$config['allowed_types'] = 'xls|xlsx|doc|docx';
			$config['file_name']= $form_evidencia.'_'.$fecha.'_ID'.$id_pre_mejoramiento;
			$config['max_size']  = '5120';
			$config['max_width']  = '2048';
			$config['max_height']  = '2048';
			
			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('post_evidencias')){
				$error = array('error' => $this->upload->display_errors());
				echo $this->upload->display_errors();
			}
			else{
				$upload_data=$this->upload->data();//SE GUARDA EL ARCHIVO EN EL PATH ESPECIFICADO
				//echo "success post_evidencias";
				return $upload_data['file_name'];//SE ASIGNA EL NOMBRE DE ARCHIVO PARA AGREGAR A LA DB
				//print_r($upload_data);
				//echo $upload_data['file_name'].$upload_data['full_path'];
			}
		}elseif ($form_evidencia=="f_ahorro") {//CARGAR ARCHIVO DE EVIDENCIAS POST MEJORAMIENTO
			$config['upload_path'] = 'uploads/f_ahorro/';
			$config['allowed_types'] = 'xls|xlsx|doc|docx';
			$config['file_name']= $form_evidencia.'_'.$fecha.'_ID'.$id_pre_mejoramiento;
			$config['max_size']  = '5120';
			$config['max_width']  = '2048';
			$config['max_height']  = '2048';
			
			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('fahorro')){
				$error = array('error' => $this->upload->display_errors());
				echo $this->upload->display_errors();
			}
			else{
				$upload_data=$this->upload->data();//SE GUARDA EL ARCHIVO EN EL PATH ESPECIFICADO
				//echo "success post_evidencias";
				return $upload_data['file_name'];//SE ASIGNA EL NOMBRE DE ARCHIVO PARA AGREGAR A LA DB
				//print_r($upload_data);
				//echo $upload_data['file_name'].$upload_data['full_path'];
			}
		}
		
		
	}

	public function download_file($file_name,$type_file){
		$this->load->helper('download');

		if ($type_file=="FA") {
			$path="uploads/formatos/";
		}elseif ($type_file=="EV") {
			$path="uploads/evidencias/";
		}elseif ($type_file=="F_A") {
			$path="uploads/f_ahorro/";
		}
		$path=$path.$file_name;
		//echo $path;

		$data=file_get_contents($path);
		$name=$file_name;
		force_download($name,$data);
	}

	public function enviar_email($pre_mejoramiento_id,$email,$subject){
		
		$query3="SELECT *
				FROM v_mejoramiento_participante
				WHERE pre_mejoramiento_id=$pre_mejoramiento_id
				LIMIT 1";

		$result3=$this->m_proponente->m_query($query3);
		$mejoramiento_usuario=$result3->row();

		$usuario=$mejoramiento_usuario->usuario_nombre." ".$mejoramiento_usuario->usuario_apellido;
		$pre_mejoramiento_titulo=$mejoramiento_usuario->pre_mejoramiento_titulo;

		$configGmail = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'tics@magnetron.com.co',
			'smtp_pass' => 'orozco260288',
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'newline' => "\r\n"
		); 

		$this->email->initialize($configGmail);//CARGAR CONFIGURACION DEL SERVIDOR DE SALIDA
		
		$this->email->from('tics@magnetron.com.co', 'Mejoramiento Online');
		$this->email->to($email);
		$this->email->cc('');//CON COPIA
		$this->email->bcc('');//CON COPIA OCULTA
		
		$this->email->subject($subject);

		if ($mejoramiento_usuario->idtipo_aprobacion==1 || $mejoramiento_usuario->idtipo_aprobacion==2) {//MENSAJE PARA APROBACION DE PRESENTACION POR EL JEFE
			$titulo='<h3>Solicitud de Aprobación para Presentar Sugerencia</h3>';
			
		}elseif ($mejoramiento_usuario->idtipo_aprobacion==4 || $mejoramiento_usuario->idtipo_aprobacion==5) {//MENSAJE PARA APROBACION DE IMPLEMENTACION POR EL JEFE
			$titulo='<h3>Solicitud de Aprobación para Implementar Sugerencia</h3>';
		}

		$message=$titulo;
		$message.='<b>Proponente:</b> '.$usuario;
		$message.='<br><b>Número Sugerencia:</b> '.$pre_mejoramiento_id;
		$message.='<br><b>Titulo Sugerencia:</b> '.$pre_mejoramiento_titulo;
		$message.='<br><br>Para revisar el mejoramiento ingrese al sistema desde <a href="portal.magnetron.com/sugerencias">aquí.</a>';
		$message.='<br>Si tiene problemas con su acceso al programa o presenta inconsistencias, por favor informar a tics@magnetron.com.co';
		$message.='<br><br>Muchas gracias por utilizar nuestro servicio,<br>TICs Magnetron - Apoya la Mejora Continua.';

		$this->email->message($message);
		
		if ($this->email->send()) {
			return 1;
		}else{
			echo $this->email->print_debugger();
			return NULL;
		}
	}
}

/* End of file proponente.php */
/* Location: ./application/controllers/proponente.php */